﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Newtonsoft.Json.Linq {
    public static class JObjectExtensions {
        
        public static JToken GetValueForKey(this JObject jObj, string key){
            try {
                JToken jtoken = jObj[key];

                if (jtoken == null) {
                    jtoken = new JValue(String.Empty);
                }

                return jtoken;
            } catch (Exception ex) {
                throw ex;
            }
        }
    }
}
