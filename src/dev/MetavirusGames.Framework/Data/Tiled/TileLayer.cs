﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Data.Tiled {

    [Serializable]
    public sealed class TileLayer : Layer {
        public int[,] Data { get; set; }


    }
}
