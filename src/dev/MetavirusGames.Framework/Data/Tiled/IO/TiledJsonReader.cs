﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Data.Tiled.IO {
    public sealed class TiledJsonReader {

        private string _filename;

        public TiledJsonReader(string filename){

            this._filename = filename;

        }

        public Map Read() {
            Map result = new Map();

            string jsonFile = File.ReadAllText(_filename);

            JObject mapJsonObject = JObject.Parse(jsonFile);

            result.Width = (int)mapJsonObject.GetValueForKey("width");

            result.Height = (int)mapJsonObject.GetValueForKey("height");

            result.TileWidth = (int)mapJsonObject.GetValueForKey("tilewidth");

            result.TileHeight = (int)mapJsonObject.GetValueForKey("tileheight");

            result.Version = (string)mapJsonObject.GetValueForKey("version");

            result.Orientation = (string)mapJsonObject.GetValueForKey("orientation");

            JObject mapProperties = (JObject)mapJsonObject["properties"];

            foreach (var kvp in mapProperties) {
                result.Properties.Add(kvp.Key, kvp.Value.ToString());
            }

            JArray layerObject = (JArray)mapJsonObject.GetValueForKey("layers");

            JArray tilesetsObject = (JArray)mapJsonObject.GetValueForKey("tilesets");

            foreach (JObject tilesetJson in tilesetsObject) {
                Tileset tileset = new Tileset();

                tileset.FirstGID = int.Parse(tilesetJson["firstgid"].ToString());

                tileset.Name = tilesetJson["name"].ToString();

                tileset.TileWidth = int.Parse(tilesetJson["tilewidth"].ToString());

                tileset.TileHeight = int.Parse(tilesetJson["tileheight"].ToString());

                tileset.Margin = int.Parse(tilesetJson["margin"].ToString());
                
                tileset.Image = tilesetJson["image"].ToString();

                tileset.ImageWidth = int.Parse(tilesetJson["imagewidth"].ToString());

                tileset.ImageHeight = int.Parse(tilesetJson["imageheight"].ToString());

                tileset.Spacing = int.Parse(tilesetJson["spacing"].ToString());

                JObject tilesetProperties = (JObject)tilesetJson["properties"];

                foreach (var kvp in tilesetProperties) {
                    tileset.Properties.Add(kvp.Key, kvp.Value.ToString());
                }

                if (tilesetJson["tileproperties"] != null) {
                    JObject tileProperties = (JObject)tilesetJson["tileproperties"];

                    foreach (var kvp in tileProperties) {
                        tileset.TileProperties.Add(int.Parse(kvp.Key) + (tileset.FirstGID - 1), kvp.Value.ToString());
                    }
                }

                result.Tilesets.Add(tileset);

            }

            foreach (JObject jsonLayer in layerObject) {
                string layerType = (string)jsonLayer["type"];
                Layer layer = null;
                int layerWidth = (int)jsonLayer["width"];
                int layerHeight = (int)jsonLayer["height"];

                if (layerType == "tilelayer") {
                    layer = new TileLayer();
                    object[] data = (jsonLayer["data"] as JArray).ToArray();

                    (layer as TileLayer).Data = new int[layerWidth, layerHeight];

                    int i = 0;
                    for (int y = 0; y < layerHeight; y++) {
                        for (int x = 0; x < layerWidth; x++) {
                            string value = data[i].ToString();
                            (layer as TileLayer).Data[x, y] = int.Parse(value);
                            i++;
                        }
                    }

                } else if (layerType == "objectgroup"){
                    layer = new ObjectLayer();

                    JArray jsonMapObject = (JArray)jsonLayer["objects"];

                    foreach (JObject mapObject in jsonMapObject) {

                        string objectName = mapObject["name"].ToString();

                        TiledObject tiledObject = new TiledObject();

                        tiledObject.Name = mapObject["name"].ToString();
                        tiledObject.X = int.Parse(mapObject["x"].ToString());
                        tiledObject.Y = int.Parse(mapObject["y"].ToString());
                        tiledObject.Visible = bool.Parse(mapObject["visible"].ToString());
                        tiledObject.Type = mapObject["type"].ToString();
                        tiledObject.Width = int.Parse(mapObject["width"].ToString());
                        tiledObject.Height = int.Parse(mapObject["height"].ToString());
                        tiledObject.GID = int.Parse(mapObject["gid"].ToString());
                        tiledObject.OriginalGID = tiledObject.GID;

                        JObject propertiesArray = (JObject)mapObject["properties"];

                        foreach (var kvp in propertiesArray) {
                            tiledObject.Properties.Add(kvp.Key, kvp.Value.ToString());
                        }

                        (layer as ObjectLayer).Objects.Add(tiledObject);
                    }
                }

                layer.Width = layerWidth;

                layer.Height = layerHeight;

                layer.Name = jsonLayer["name"].ToString();

                layer.X = (int)jsonLayer["x"];

                layer.Y = (int)jsonLayer["y"];

                layer.Opacity = float.Parse(jsonLayer["opacity"].ToString());

                layer.Visible = bool.Parse(jsonLayer["visible"].ToString());


                result.Layers.Add(layer);

            }

            return result;
        }

     
    }
}
