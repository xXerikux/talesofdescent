﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Data.Tiled {

    [Serializable]
    public sealed class TiledObject {

        public string Name { get; set; }

        public string Type { get; set; }

        public int X { get; set; }

        public int Y { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public int Rotation { get; set; }

        public int OriginalGID { get; set; }

        public int GID { get; set; }

        public bool Visible { get; set; }

        public Dictionary<string, string> Properties { get; set; }

        public Rectangle Bounds {
            get {
                return new Rectangle(X, Y - 16, 16, 16);
            }
        }

        public TiledObject() {
            Properties = new Dictionary<string, string>();
        }
    }
}
