﻿using MetavirusGames.Framework.Runtime;
using MetavirusGames.Framework.Services;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Data.Tiled {

    [Serializable]
    public sealed class Tileset {

        public int FirstGID { get; set; }

        public string Image { get; set; }

        [NonSerialized()]
        public Texture2D Texture;

        public Rectangle[] Data { get; set; }

        public string Name { get; set; }

        public int TileWidth { get; set; }

        public int TileHeight { get; set; }

        public int ImageWidth { get; set; }

        public int ImageHeight { get; set; }

        public int Spacing { get; set; }

        public int Margin { get; set; }

        public Dictionary<string, string> Properties { get; set; }

        public Dictionary<int, string> TileProperties { get; set; }

        public Tileset() {

            Properties = new Dictionary<string, string>();

            TileProperties = new Dictionary<int, string>();

        }

        public void LoadContent() {
            IXNAService gameService = ServiceLocator.Instance.Get<IXNAService>();

            if (gameService != null) {
                string filename = Path.GetFileNameWithoutExtension(Image);

                Texture = gameService.ContentManager.Load<Texture2D>(String.Format(@"Textures/{0}", filename));

                Data = new Rectangle[(ImageWidth / TileWidth * ImageHeight / TileHeight) + 1];

                int i = 1;

                Data[0] = new Rectangle();

                for (int y = 0; y < (ImageHeight / TileHeight); y++) {
                    for (int x = 0; x < (ImageWidth / TileWidth); x++) {
                        Data[i] = new Rectangle(x * TileWidth, y * TileHeight, TileWidth, TileHeight);
                        i++;
                    }
                }

            } else {
                throw new Exception("A Valid Game Service Could Not Be Found!");
            }
        }
    }
}
