﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Data.Tiled {
    public sealed class MapReference {

        private Map _map;

        public Dictionary<int, Tileset> _tilesetLookup;

        public MapReference(Map map) {

            this._map = map;

            this._tilesetLookup = new Dictionary<int, Tileset>();

            BuildTilesetLookup();

        }

        public void BuildTilesetLookup() {

            foreach (Layer layer in this._map.Layers) {

                if (layer is TileLayer) {
                    TileLayer tileLayer = (layer as TileLayer);

                    foreach (int gid in tileLayer.Data) {

                        _tilesetLookup[gid] = this._map.GetTilesetForGid(gid);

                    }

                } else if (layer is ObjectLayer) {
                    ObjectLayer objectLayer = (layer as ObjectLayer);

                    foreach (TiledObject gameObject in objectLayer.Objects) {

                        _tilesetLookup[gameObject.OriginalGID] = this._map.GetTilesetForGid(gameObject.OriginalGID);

                    }

                }

            }

        }

        public Tileset GetTilesetForGID(int gid) {

            if (this._tilesetLookup.ContainsKey(gid)) {
                return this._tilesetLookup[gid];
            } else {
                return null;
            }

        }
    }
}
