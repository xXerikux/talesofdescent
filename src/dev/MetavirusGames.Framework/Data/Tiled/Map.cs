﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Data.Tiled {

    [Serializable]
    public sealed class Map {

        public string Version { get; set; }

        public string Orientation { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public int TileWidth { get; set; }

        public int TileHeight { get; set; }

        public string BackgroundColor { get; set; }

        public IList<Layer> Layers { get; set; }

        public IList<Tileset> Tilesets { get; set; }

        public Dictionary<string, string> Properties { get; set; }

        public Map() {

            Layers = new List<Layer>();

            Tilesets = new List<Tileset>();

            Properties = new Dictionary<string, string>();

        }

        public void LoadContent() {

            foreach (Tileset tileset in Tilesets) {
                tileset.LoadContent();
            }

        }

        public Tileset GetTilesetForGid(int gid) {

            IEnumerable<Tileset> tilesetCandidates = Tilesets.Where(t => gid >= t.FirstGID).OrderByDescending(t => t.FirstGID);

            return tilesetCandidates.FirstOrDefault();
        }

        public Layer SelectLayer<T>(string name) where T : Layer {
            return (T)Layers.FirstOrDefault(l => l.Name == name);
        }

        public IQueryable<TiledObject> SelectObjects(Func<TiledObject, bool> predicate) {
            ObjectLayer objectLayer = (ObjectLayer)SelectLayer<ObjectLayer>("Entities");

            if (objectLayer != null) {

                return objectLayer.Objects.Where(predicate).AsQueryable();

            } else {

                throw new Exception("No valid object layer could be found for this map. Valid object layers should be named 'Entities'!");

            }

        }

        public int GetGidForTileType(string type){

            foreach (Tileset tileset in Tilesets) {

                foreach (var property in tileset.TileProperties) {

                    JObject tileProperty = JObject.Parse(property.Value);

                    string propertyValue = tileProperty["Type"].ToString();

                    if (propertyValue == type) {

                        return property.Key;
                    }

                }

            }
            
            return 0;
        }
    }
}
