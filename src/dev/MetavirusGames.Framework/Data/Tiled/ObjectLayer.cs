﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Data.Tiled {

    [Serializable]
    public sealed class ObjectLayer : Layer {

        public IList<TiledObject> Objects { get; set; }

        public ObjectLayer() {

            Objects = new List<TiledObject>();

        }
    }
}
