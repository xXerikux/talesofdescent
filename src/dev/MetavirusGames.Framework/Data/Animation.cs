﻿using MetavirusGames.Framework.Graphics.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Data {
    public sealed class Animation : IRenderable, ICloneable {

        public string Name { get; set; }

        public Rectangle[] Frames { get; private set; }

        public Rectangle CurrentFrame { get; private set; }

        public Texture2D Texture { get; private set; }

        public int TotalFrames {
            get {
                return Frames.Length;
            }
        }

        public Vector2 Position { get; private set; }

        public Color Color { get; private set; }

        private int _frameWidth = 0;

        private int _frameHeight = 0;

        private float _interval = 0f;

        private float _tick = 0f;

        private int _frameIndex = 0;

        private bool _loop = false;

        public bool Enabled { get; private set; }

        private bool _firstFrameOnly = false;

        private Vector2 _offset = new Vector2();

        private Action _animationEndAction;

        public Animation(string name, Texture2D texture, int frameWidth, int frameHeight, float interval, Vector2 position, Color color, Action animationEndAction = null, bool firstFrameOnly = false, bool loop = true, int xOffset = 0, int yOffset = 0) {

            this.Name = name;

            this.Texture = texture;

            this._frameWidth = frameWidth;

            this._frameHeight = frameHeight;

            Frames = new Rectangle[Texture.Width / this._frameWidth * Texture.Height / this._frameHeight];

            for (int i = 0; i < TotalFrames; i++) {
                Frames[i] = new Rectangle(i * this._frameWidth, 0, this._frameWidth, this._frameHeight);
            }

            this._interval = interval;

            this.Position = position;

            this.Color = color;

            this._loop = loop;

            this.Enabled = true;

            this._firstFrameOnly = firstFrameOnly;

            this._offset.X = xOffset;

            this._offset.Y = yOffset;

            this._animationEndAction = animationEndAction;
        }

        public void Update(GameTime gameTime) {

            if (this.Enabled) {
                if (_tick >= _interval) {
                    _tick = 0f;

                    if (!this._firstFrameOnly) {
                        _frameIndex++;
                    }

                    if (_frameIndex >= TotalFrames) {
                        _frameIndex = 0;
                        if (!this._loop) {
                            this.Enabled = false;
                            if (this._animationEndAction != null) {
                                this._animationEndAction();
                            }
                        }
                    }

                } else {
                    _tick += (float)gameTime.ElapsedGameTime.TotalSeconds;
                }

                CurrentFrame = Frames[_frameIndex];
            }
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch) {

            spriteBatch.Draw(this.Texture, this.Position + this._offset, this.CurrentFrame, this.Color);

        }

        public void SetPosition(Vector2 position) {

            this.Position = position;

        }

        public void SetColor(Color color) {

            this.Color = color;

        }

        public void Restart() {

            this.Enabled = true;

        }

        public object Clone() {
            return new Animation(this.Name, this.Texture, this._frameWidth, this._frameHeight, this._interval, this.Position, this.Color, this._animationEndAction, this._firstFrameOnly, this._loop, (int)this._offset.X, (int)this._offset.Y);
        }
    }
}
