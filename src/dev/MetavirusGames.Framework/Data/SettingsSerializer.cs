﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Data {
    public sealed class SettingsSerializer {

        public void Serialize(string filename, Settings settings) {
            string json = LitJson.JsonMapper.ToJson(settings);

            File.WriteAllText(filename, json, Encoding.UTF8);
        }

        public Settings Deserialize(string filename) {
            string json = String.Empty;

            if (File.Exists(filename)){
             json = File.ReadAllText(filename);
            }else{
                Error.FileNotFound("Could not find the settings file: {0}", filename);
            }

            if (json != String.Empty) {
                Settings settings = LitJson.JsonMapper.ToObject<Settings>(json);
                return settings;
            } else {
                Error.FileLoad("File contains invalid JSON");
            }

            return null;
        }
    }
}
