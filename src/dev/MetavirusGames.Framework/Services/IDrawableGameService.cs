﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Services {
    public interface IDrawableGameService : IGameService {

        void Draw(GameTime gameTime, SpriteBatch spriteBatch);

    }
}
