﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Entities.Interfaces {
    public interface IGameEntity {

        string Name { get; }

        Vector2 Position { get; }

        void Move(Vector2 amount);
    }
}
