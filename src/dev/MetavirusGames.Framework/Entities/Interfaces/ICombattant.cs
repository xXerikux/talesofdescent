﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Entities.Interfaces {
    public interface ICombattant {

        int AttackPower { get; }

        void Attack(ILivingEntity target);
    }
}
