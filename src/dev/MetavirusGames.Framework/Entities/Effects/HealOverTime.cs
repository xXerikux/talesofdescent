﻿using MetavirusGames.Framework.Audio;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Entities.Effects {
    public sealed class HealOverTime : Effect {

        public static SoundEffect HealSound;

        public HealOverTime(int howLong, Character character, SoundEffect healSound)
            : base(0.5f, howLong, () => {
                character.Heal(1);
                HealOverTime.HealSound.Play(Music.Volume, 0f, 0f);
            }) {

            HealSound = healSound;

            FlashColor = Color.White;

        }
    }
}
