﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.AI {
    public interface IAIBehavior {

        void Update(GameTime gameTime);

    }
}
