﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.AI {
    public class AStarSearch {
        private List<PathfindingNode> openList = new List<PathfindingNode>();
        private List<PathfindingNode> closedList = new List<PathfindingNode>();
        private List<PathfindingTile> tiles = new List<PathfindingTile>();

        public bool Diagonal = true;

        private PathfindingNode start;
        private PathfindingNode end;
        private PathfindingNode current;
        private Vector2 mapSize;

        public AStarSearch(List<PathfindingTile> t, Vector2 mapSize) {
            this.mapSize = mapSize;
            tiles = t;
        }
        public AStarSearch(List<PathfindingTile> t, Vector2 mapSize, bool diagonal) {
            this.mapSize = mapSize;
            tiles = t;
            Diagonal = diagonal;
        }
        public List<PathfindingTile> SearchPath() {
            int i;
            for (i = 0; i < tiles.Count; i++) {
                if (tiles[i].Type == 1)
                    start = new PathfindingNode(tiles[i].ID, null);
                if (tiles[i].Type == 2)
                    end = new PathfindingNode(tiles[i].ID, null);
            }

            openList.Add(start);
            current = start;

            while (true) {
                if (openList.Count == 0)
                    break;

                current = FindSmallestF();

                if (current.CellIndex == end.CellIndex)
                    break;

                openList.Remove(current);
                closedList.Add(current);

                //if (Diagonal) {
                //    //Diagonal
                //    AddAdjacentCellToOpenList(current, 1, -1, 14);
                //    AddAdjacentCellToOpenList(current, -1, -1, 14);
                //    AddAdjacentCellToOpenList(current, -1, 1, 14);
                //    AddAdjacentCellToOpenList(current, 1, 1, 14);
                //}
                //Straight
                AddAdjacentCellToOpenList(current, 0, -1, 10);
                AddAdjacentCellToOpenList(current, -1, 0, 10);
                AddAdjacentCellToOpenList(current, 1, 0, 10);
                AddAdjacentCellToOpenList(current, 0, 1, 10);

            }
            while (current != null) {
                bool endOnClosed = false;
                for (int v = 0; v < openList.Count; v++)
                    if (openList[v].CellIndex == end.CellIndex)
                        endOnClosed = true;
                if (endOnClosed && tiles[current.CellIndex].Type != 2 && tiles[current.CellIndex].Type != 1)
                    tiles[current.CellIndex].Type = 4;
                current = current.Parent;
            }

            return tiles;
        }


        private PathfindingNode FindSmallestF() {
            var smallestF = int.MaxValue;
            PathfindingNode selectedNode = null;

            foreach (var node in openList) {
                if (node.F < smallestF) {
                    selectedNode = node;
                    smallestF = node.F;
                }
            }

            return selectedNode;
        }


        private void AddAdjacentCellToOpenList(PathfindingNode parentNode, int columnOffset, int rowOffset, int gCost) {
            var adjacentCellIndex = GetAdjacentCellIndex(parentNode.CellIndex, columnOffset, rowOffset);

            // ignore unwalkable nodes (or nodes outside the grid)
            if (adjacentCellIndex == -1)
                return;

            // ignore nodes on the closed list
            if (closedList.Any(n => n.CellIndex == adjacentCellIndex))
                return;

            var adjacentNode = openList.SingleOrDefault(n => n.CellIndex == adjacentCellIndex);
            if (adjacentNode != null) {
                if (parentNode.G + gCost < adjacentNode.G) {
                    adjacentNode.Parent = parentNode;
                    adjacentNode.G = parentNode.G + gCost;
                    adjacentNode.F = adjacentNode.G + adjacentNode.H;
                }

                return;
            }

            var node = new PathfindingNode(adjacentCellIndex, parentNode) { G = gCost, H = GetDistance(adjacentCellIndex, end.CellIndex) };
            node.F = node.G + node.H;
            openList.Add(node);
        }

        public int GetAdjacentCellIndex(int cellIndex, int columnOffset, int rowOffset) {
            var x = cellIndex % (int)mapSize.X;
            var y = cellIndex / (int)mapSize.X;

            if ((x + columnOffset < 0 || x + columnOffset > mapSize.X - 1) ||
                (y + rowOffset < 0 || y + rowOffset > mapSize.Y - 1))
                return -1;

            if (tiles[((y + rowOffset) * (int)mapSize.X) + (x + columnOffset)].Type == 3)
                return -1;

            return cellIndex + columnOffset + (rowOffset * (int)mapSize.X);
        }

        public int GetDistance(int startTileID, int endTileID) {
            var startX = (int)tiles[startTileID].TilePos.X / (int)mapSize.X;
            var startY = (int)tiles[startTileID].TilePos.Y / (int)mapSize.Y;

            var endX = (int)tiles[endTileID].TilePos.X / (int)mapSize.X;
            var endY = (int)tiles[endTileID].TilePos.Y / (int)mapSize.Y;

            return Math.Abs(startX - endX) + Math.Abs(startY - endY);
        }
    }

}
