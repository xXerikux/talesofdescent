﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Graphics {
    public class Light {
        public Texture2D Texture;
        public Vector2 Position, Origin;
        public float Rotation;
        public float Scale;
        public Color Tint;

        public Light(Texture2D texture, float posX, float posY, float originX, float originY, float rotation, float scale, Color tint) {
            this.Texture = texture;
            this.Position = new Vector2(posX, posY);
            this.Origin = new Vector2(originX, originY);
            this.Rotation = rotation;
            this.Scale = scale;
            this.Tint = tint;
        }
    }
}
