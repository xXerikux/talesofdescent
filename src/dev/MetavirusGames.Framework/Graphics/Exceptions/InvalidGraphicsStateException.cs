﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Graphics.Exceptions {
    public sealed class InvalidGraphicsStateException : Exception {

        public InvalidGraphicsStateException()
            : base() {

        }

        public InvalidGraphicsStateException(string message, params string[] args) : base(String.Format(message, args)) {

        }
    }
}
