﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Graphics.Interfaces {
    public interface IGraphicsEngine {

        ICamera Camera { get; }

        void BeginDraw();

        void BeginDrawUI();

        void Draw(GameTime gameTime, IRenderable renderableObject);

        void DrawUI(GameTime gameTime, IRenderable renderableObject);

        void EndDraw();

        void EndDrawUI();

        void AddLight(Light light);

        void ClearLights();
    }
}
