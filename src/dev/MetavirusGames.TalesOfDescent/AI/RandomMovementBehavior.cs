﻿using MetavirusGames.Framework.AI;
using MetavirusGames.Framework.Entities;
using MetavirusGames.Framework.Runtime;
using MetavirusGames.TalesOfDescent.Entities;
using MetavirusGames.TalesOfDescent.Screens;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.AI {
    public sealed class RandomMovementBehavior : IAIBehavior {

        private Character _character;

        private Timer _movementTimer;

        private static Random _random = new Random(new Random().Next(0, 100000));

        public RandomMovementBehavior(Character character, float interval = 1f, int idleChanceThreshold = 20) {

            this._character = character;

            

            _movementTimer = new Timer(() => {

                Player player = (Player)GameplayScreen.Characters.FirstOrDefault(c => c is Player);

                float distance = Vector2.Distance(player.Position, _character.Position);

                if (distance < 75) {
                    TurnTowardsPlayer(player);
                } else {

                    int idleChance = _random.Next(0, 100);

                    if (idleChance < idleChanceThreshold) {
                        Axis axis;

                        int axisChance = _random.Next(0, 100);

                        axis = axisChance <= 50 ? Axis.Horizontal : Axis.Vertical;

                        int moveChance = _random.Next(-100, 100);

                        int moveVector = moveChance <= 0 ? -1 : 1;

                        if (axis == Axis.Vertical) {
                            this._character.Move(new Vector2(0, moveVector));
                            if (moveVector < 0) {
                                this._character.SetHeading(EntityHeading.Up);

                            } else {
                                this._character.SetHeading(EntityHeading.Down);
                            }
                            this._character.SetState(EntityState.Walking);
                        } else {
                            this._character.Move(new Vector2(moveVector, 0));

                            if (moveVector < 0) {
                                this._character.SetHeading(EntityHeading.Left);
                            } else {
                                this._character.SetHeading(EntityHeading.Right);
                            }
                            this._character.SetState(EntityState.Walking);
                        }
                    }
                }
            }, interval, repeats: true);
            
        }

        public void Update(GameTime gameTime) {

            _movementTimer.Update(gameTime);

        }

        private void TurnTowardsPlayer(Player player) {

            float xDistance = _character.Bounds.Center.X - player.Bounds.Center.X;
            float yDistance = _character.Bounds.Center.Y - player.Bounds.Center.Y;

            if (Math.Abs(xDistance) > Math.Abs(yDistance)) {
                if (player.Bounds.Center.X < _character.Bounds.Center.X) {
                    _character.SetHeading(EntityHeading.Left);
                    //this.SetPosition(this.Position + new Vector2(5, 0));
                } if (player.Bounds.Center.X >= _character.Bounds.Center.X) {
                    _character.SetHeading(EntityHeading.Right);
                    //this.SetPosition(this.Position + new Vector2(-5, 0));
                }
            } else {
                if (player.Bounds.Center.Y < _character.Bounds.Center.Y) {
                    _character.SetHeading(EntityHeading.Up);
                    //this.SetPosition(this.Position + new Vector2(0, 5));
                } if (player.Bounds.Center.Y >= _character.Bounds.Center.Y) {
                    _character.SetHeading(EntityHeading.Down);
                    //this.SetPosition(this.Position + new Vector2(0, -5));
                }
            }

            _character.SetState(EntityState.Idle);

        }
    }
}
