﻿using MetavirusGames.Framework.Runtime;
using MetavirusGames.Framework.Services;
using MetavirusGames.TalesOfDescent.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Components {
    public sealed class ArtifactService : IDrawableGameService {

        public IList<Artifact> Artifacts;

        public Texture2D Texture { get; set; }

        public ArtifactService() { }

        public void Initialize() {

            Artifacts = new List<Artifact>();

            IXNAService xnaService = ServiceLocator.Instance.Get<IXNAService>();

            Texture = xnaService.ContentManager.Load<Texture2D>(@"Textures/Artifacts");

        }

        public void Update(GameTime gameTime) {
            throw new NotImplementedException();
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch) {

            foreach (Artifact artifact in Artifacts) {

                artifact.Draw(gameTime, spriteBatch);

            }

        }

        public void AddArtifact(Artifact artifact) {

            Artifacts.Add(artifact);

        }
        
    }
}
