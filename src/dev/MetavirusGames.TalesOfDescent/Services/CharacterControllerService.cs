﻿using MetavirusGames.Framework.Audio;
using MetavirusGames.Framework.Entities;
using MetavirusGames.Framework.Entities.Effects;
using MetavirusGames.Framework.Entities.Interfaces;
using MetavirusGames.Framework.Runtime;
using MetavirusGames.Framework.Services;
using MetavirusGames.TalesOfDescent.Entities;
using MetavirusGames.TalesOfDescent.Screens;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Services {
    public class CharacterControllerService {

        private Character _character;

        private KeyboardState _previousKeyboard;

        private GamePadState _previousGamePad;

        private IXNAService _gameService;

        private SoundEffect _bowSound;

        public ProjectileService ProjectileService {
            get {
                return ServiceLocator.Instance.Get<ProjectileService>();
            }
        }

        public InventoryService InventoryService {
            get {
                return ServiceLocator.Instance.Get<InventoryService>();
            }
        }

        public CharacterControllerService(Character entity, IXNAService gameService) {

            this._character = entity;

            this._gameService = gameService;

            _bowSound = gameService.ContentManager.Load<SoundEffect>(@"SFX/bow");

        }

        public void Update() {
            KeyboardState keyboard = Keyboard.GetState();
            GamePadState gamePad = GamePad.GetState(PlayerIndex.One); //TODO: search for active index, not just index one

            Vector2 moveAmount = new Vector2();

            this._character.ResetMovement();

            Vector2 thumbstickState = new Vector2(gamePad.ThumbSticks.Left.X, gamePad.ThumbSticks.Left.Y);

            //if (thumbstickState != Vector2.Zero) {
            //    return;
            //}

            if (keyboard.IsKeyDown(Keys.Up) || gamePad.DPad.Up == ButtonState.Pressed || thumbstickState.Y > 0) {
                moveAmount = new Vector2(0, -1);
                this._character.SetHeading(EntityHeading.Up);

                if (this._character.State != EntityState.Attacking)
                    this._character.SetState(EntityState.Walking);
            } if (keyboard.IsKeyDown(Keys.Down) || gamePad.DPad.Down == ButtonState.Pressed || thumbstickState.Y < 0) {
                moveAmount = new Vector2(0, 1);
                this._character.SetHeading(EntityHeading.Down);
                if (this._character.State != EntityState.Attacking)
                    this._character.SetState(EntityState.Walking);
            } if (keyboard.IsKeyDown(Keys.Left) || gamePad.DPad.Left == ButtonState.Pressed || thumbstickState.X < 0) {
                moveAmount = new Vector2(-1, moveAmount.Y);
                this._character.SetHeading(EntityHeading.Left);
                if (this._character.State != EntityState.Attacking)
                    this._character.SetState(EntityState.Walking);
            } if (keyboard.IsKeyDown(Keys.Right) || gamePad.DPad.Right == ButtonState.Pressed || thumbstickState.X > 0) {
                moveAmount = new Vector2(1, moveAmount.Y);
                this._character.SetHeading(EntityHeading.Right);
                if (this._character.State != EntityState.Attacking)
                    this._character.SetState(EntityState.Walking);
            }

            if ((keyboard.IsKeyDown(Keys.X) && !this._previousKeyboard.IsKeyDown(Keys.X)) || (gamePad.Buttons.X == ButtonState.Pressed && _previousGamePad.Buttons.X != ButtonState.Pressed)) {

                if (this._character.Stamina > 0) {

                    this._character.UseStamina();

                    this._character.SetState(EntityState.Attacking);

                    if (this._character.AttackSound != null) {
                        this._character.AttackSound.Play(Music.Volume, 0f, 0f);
                    }
                }
            }

            if ((keyboard.IsKeyDown(Keys.Z) && !this._previousKeyboard.IsKeyDown(Keys.Z)) || (gamePad.Buttons.Y == ButtonState.Pressed && _previousGamePad.Buttons.Y != ButtonState.Pressed)) {


                if (InventoryService.CurrentItem == Items.HealthPotionId) {
                    if (InventoryService.GetCurrentCount(InventoryService.CurrentItem) > 0) {
                        InventoryService.RemoveItem(Items.HealthPotionId, 1);
                        this._character.SetEffect(new HealOverTime(5, this._character, this._gameService.ContentManager.Load<SoundEffect>(@"SFX/heal")));
                    }


                } else if (InventoryService.CurrentItem == Items.SpellbookId || InventoryService.CurrentItem == Items.ArrowId
                    || InventoryService.CurrentItem == Items.IceArrowId || InventoryService.CurrentItem == Items.FireArrowId) {
                        int currentItemID = InventoryService.CurrentItem;

                    Vector2 velocity = new Vector2();

                    if (_character.Heading == EntityHeading.Up) {
                        velocity = new Vector2(0, -250);
                    }
                    if (_character.Heading == EntityHeading.Down) {
                        velocity = new Vector2(0, 250);
                    }
                    if (_character.Heading == EntityHeading.Left) {
                        velocity = new Vector2(-250, 0);
                    }
                    if (_character.Heading == EntityHeading.Right) {
                        velocity = new Vector2(250, 0);
                    }

                    if (currentItemID == Items.SpellbookId) {
                        ProjectileService.AddProjectile(new MagicBall(_character.Position + new Vector2(8, 8), velocity));
                    } else if (currentItemID == Items.ArrowId) {
                        ProjectileService.AddProjectile(new Arrow(_character.Position + new Vector2(8, 8), velocity));
                        _bowSound.Play(Music.Volume, 0f, 0f);
                    } else if (currentItemID == Items.FireArrowId) {
                        ProjectileService.AddProjectile(new FireArrow(_character.Position + new Vector2(8, 8), velocity));
                        _bowSound.Play(Music.Volume, 0f, 0f);
                    } else if (currentItemID == Items.IceArrowId) {
                        ProjectileService.AddProjectile(new IceArrow(_character.Position + new Vector2(8, 8), velocity));
                        _bowSound.Play(Music.Volume, 0f, 0f);
                    } 

                }
            }

            this._character.Move(moveAmount);

            if (this._character.Velocity == Vector2.Zero) {
                if (this._character.State != EntityState.Attacking) {
                    this._character.SetState(EntityState.Idle);
                }
            }

            this._previousKeyboard = keyboard;
            this._previousGamePad = gamePad;
        }
    }
}
