﻿using MetavirusGames.Framework.Runtime;
using MetavirusGames.Framework.Services;
using MetavirusGames.TalesOfDescent.Components;
using MetavirusGames.TalesOfDescent.Entities;
using MetavirusGames.TalesOfDescent.Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Services {
    public sealed class ProjectileService : IDrawableGameService {

        public IList<Projectile> Projectiles { get; private set; }

        public ArtifactService ArtifactService {
            get {
                return ServiceLocator.Instance.Get<ArtifactService>();
            }
        }

        public MapService MapService {
            get {
                return ServiceLocator.Instance.Get<MapService>();
            }
        }

        public void Initialize() {
            Projectiles = new List<Projectile>();
        }

        public void Update(Microsoft.Xna.Framework.GameTime gameTime) {

            foreach (Projectile projectile in Projectiles) {

                projectile.Update(gameTime);

                MetavirusGames.Framework.Data.Tiled.TileLayer collisonLayer =
                    (MetavirusGames.Framework.Data.Tiled.TileLayer)MapService.CurrentMap.SelectLayer<MetavirusGames.Framework.Data.Tiled.TileLayer>("Collisions");

                if (collisonLayer != null) {

                    Vector2 tilePosition = new Vector2(projectile.Position.X / 16, projectile.Position.Y / 16);
                    int value = collisonLayer.Data[(int)tilePosition.X, (int)tilePosition.Y];

                    if (value > 0) {
                        projectile.IsAlive = false;
                    }
                }
            }

            for (int i = 0; i < Projectiles.Count; i++) {

                if (Projectiles[i].IsAlive == false) {
                    if (Projectiles[i] is Arrow) {
                        ArtifactService.AddArtifact(new Artifact(ArtifactService.Texture, Projectiles[i].Position, new Rectangle(16, 0, 16, 16), Projectiles[i].Rotation));
                    }
                    if (Projectiles[i] is FireArrow) {
                        ArtifactService.AddArtifact(new Artifact(ArtifactService.Texture, Projectiles[i].Position, new Rectangle(48, 0, 16, 16), Projectiles[i].Rotation));
                    }
                    if (Projectiles[i] is IceArrow) {
                        ArtifactService.AddArtifact(new Artifact(ArtifactService.Texture, Projectiles[i].Position, new Rectangle(32, 0, 16, 16), Projectiles[i].Rotation));
                    }
                    Projectiles.RemoveAt(i);
                    i--;
                }
            }

        }

        public void Draw(Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch) {
            foreach (Projectile projectile in Projectiles) {

                projectile.Draw(gameTime, spriteBatch);

            }
        }

        public void AddProjectile(Projectile projectile) {

            this.Projectiles.Add(projectile);

        }
    }
}
