﻿using MetavirusGames.TalesOfDescent.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Services {
    internal sealed class ScoreoidService {

        public static string API_KEY = @"3df8a28375c108afeb8b1315e0cb66579e751594";

        public static string GAME_KEY = @"1bb00dfa03";

        public static string API_URI = @"https://api.scoreoid.com/v1";

        public void SetGameData(string key, string value, Action successAction, Action<string> failureAction, Action informationAction) {

            try {
                
                WebRequestHelper.PostGameData(API_URI, key, value);

            } catch (Exception ex) {
                failureAction(ex.ToString());
            }
        }

        public string GetGameData(string key, Action successAction, Action<string> failureAction, Action informationAction) {

            try {

                string response = WebRequestHelper.GetGameData(API_URI, key);

                return response;

            } catch (Exception ex) {
                failureAction(ex.ToString());
                return String.Empty;
            }

        }
    }
}
