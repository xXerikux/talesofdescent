﻿using MetavirusGames.Framework.Data.Tiled;
using MetavirusGames.Framework.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Services {
    public sealed class MapService : IGameService {

        public Map CurrentMap { get; private set; }

        public void Initialize() {
            MetavirusGames.Framework.Data.Tiled.IO.TiledJsonReader reader = new Framework.Data.Tiled.IO.TiledJsonReader(@"Content/Maps/Beta.json");

            CurrentMap = reader.Read();

            CurrentMap.LoadContent();

        }

        public void Update(Microsoft.Xna.Framework.GameTime gameTime) {
            
        }
    }
}
