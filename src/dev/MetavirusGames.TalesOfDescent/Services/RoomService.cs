﻿using MetavirusGames.Framework;
using MetavirusGames.Framework.Services;
using MetavirusGames.TalesOfDescent.Dungeon;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Services {
    public class RoomService {

        private string _roomPath = @"Content/Rooms/";

        private string _searchPattern = "*.png";

        private IAssetService _textureAssetService;

        private List<int> _randomBlocks = new List<int>() { 37, 38, 39 };

        private Random _random = new Random(new Random(100000).Next());

        public List<Room> ExitRoomCandidates { get; private set; }

        public RoomService(IAssetService assetService) {
            this._textureAssetService = assetService;

            ExitRoomCandidates = new List<Room>();

        }

        public IList<Room> GetAll() {

            if (Directory.Exists(_roomPath)) {

                IList<Room> rooms = new List<Room>();

                DirectoryInfo directoryInfo = new DirectoryInfo(_roomPath);

                IList<FileInfo> files = directoryInfo.GetFiles(this._searchPattern).ToList<FileInfo>();

                foreach (string filename in files.Select(f => f.Name)) {

                    if (!filename.Contains("SpawnRoom")) {
                        Texture2D texture = _textureAssetService.GameService.ContentManager.Load<Texture2D>(this._roomPath + filename);

                        Room room = CreateRoomFromImage(texture);

                        rooms.Add(room);

                        if (int.Parse(Path.GetFileNameWithoutExtension(filename)) < 16) {
                            ExitRoomCandidates.Add(room);
                        }
                    }
                }

                return rooms;
            } else {
                Error.DirectoryNotFound("Could not find the path: {0}", _roomPath);
            }

            return null;
        }

        public Room GetSpawnRoom() {

            Texture2D texture = _textureAssetService.GameService.ContentManager.Load<Texture2D>(this._roomPath + "SpawnRoom.png");

            Room room = CreateRoomFromImage(texture);

            return room;
        }

        /// <summary>
        /// Creates a 2d array of data from a texture where
        /// black represents a 0 value and white represents 1
        /// </summary>
        /// <param name="texture"></param>
        /// <returns></returns>
        private Room CreateRoomFromImage(Texture2D texture) {


            Color[] color = new Color[texture.Width * texture.Height];

            texture.GetData<Color>(color);

            int[,] data = new int[Room.MaxWidth, Room.MaxHeight];

            int i = 0;

            List<Rectangle> warps = new List<Rectangle>();
            bool defeatAllRoom = false;

            for (int y = 0; y < Room.MaxHeight; y++) {
                for (int x = 0; x < Room.MaxWidth; x++) {
                    int value = TranslateDataIdFromColor(color[i]);
                    data[x, y] = value;

                    if (value == 3) {
                        warps.Add(new Rectangle(x * 16, y * 16, 16, 16));
                    } else if (value == 6) {
                        defeatAllRoom = true;
                        data[x, y] = 1;
                    }

                    i++;
                }
            }

            Room result = new Room(data);

            result.MustDefaultAllToLeave = defeatAllRoom;

            foreach (Rectangle warp in warps) {
                result.AddWarp(warp);
            }

            return result;
        }

        private List<int> _floorTiles = new List<int>() { 4, 5 }; //Mossy cobble, gets translated to 12,13

        private int TranslateDataIdFromColor(Color color) {

            if (color == Color.White) {
                if (this._random.Next(0, 100) > 5) {
                    return 1;// return 7;
                } else {
                    int reval = _floorTiles[this._random.Next(0, 2)];
                    return reval;
                }
            }

            if (color == new Color(179, 179, 179)) {
                return 0;
            }

            if (color == new Color(49, 162, 238)) {
                return 3;
            }

            if (color.R == 33 && color.G == 33 && color.B == 33) {
                return 2;// return _randomBlocks[_random.Next(0, _randomBlocks.Count - 1)];
            }

            if (color.R == 184 && color.G == 152 && color.B == 208) {
                return 6;
            }

            if (color.R == 78 && color.G == 74 && color.B == 66) {
                return 7;
            }

            return -1;
        }

    }
}
