﻿using MetavirusGames.Framework.Services;
using MetavirusGames.TalesOfDescent.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Services {
    public sealed class InventoryService : IGameService {

        private Inventory _playerInventory;

        public Dictionary<int, int> Items {
            get {
                return _playerInventory.Items;
            }
        }

        public int CurrentItem {
            get {
                return _playerInventory.CurrentItem;
            }
        }

        public void Initialize() {
            _playerInventory = new Inventory();
        }

        public void Update(Microsoft.Xna.Framework.GameTime gameTime) {
            throw new NotImplementedException();
        }

        public void AddItem(int id, int quantity) {
            _playerInventory.AddItem(id, quantity);
        }

        public void RemoveItem(int id, int quantity) {
            _playerInventory.RemoveItem(id, quantity);
        }

        public int GetCurrentCount(int id) {
            return _playerInventory.GetCurrentCount(id);
        }

        public void SetCurrentItem(int id) {
            _playerInventory.SetCurrentItem(id);
        }
    }
}
