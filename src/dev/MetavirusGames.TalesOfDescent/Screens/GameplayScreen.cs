#region File Description
//-----------------------------------------------------------------------------
// GameplayScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using MetavirusGames.Framework.Audio;
using MetavirusGames.Framework.Services;
using MetavirusGames.Framework.Data;
using MetavirusGames.Framework.Data.Tiled.IO;
using MetavirusGames.Framework.Entities;
using MetavirusGames.Framework.Entities.Effects;
using MetavirusGames.Framework.Graphics;
using MetavirusGames.Framework.Graphics.Interfaces;
using MetavirusGames.Framework.Runtime;
using MetavirusGames.Framework.ScreenManager;
using MetavirusGames.Framework.Services;
using MetavirusGames.Framework.UI;
using MetavirusGames.TalesOfDescent.Components;
using MetavirusGames.TalesOfDescent.Dungeon;
using MetavirusGames.TalesOfDescent.Entities;
using MetavirusGames.TalesOfDescent.Graphics;
using MetavirusGames.TalesOfDescent.Services;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using Tiled = MetavirusGames.Framework.Data.Tiled;

#endregion

namespace MetavirusGames.TalesOfDescent.Screens {
    /// <summary>
    /// This screen implements the actual game logic. It is just a
    /// placeholder to get the idea across: you'll probably want to
    /// put some more interesting gameplay in here!
    /// </summary>
    class GameplayScreen : GameScreen {
        #region Fields

        private SpriteBatch spriteBatch;

        //private Map map;

        private KeyboardState previousKeyboard;

        private GamePadState previousGamepad;

        public ArtifactService ArtifactService { get; set; }

        public ProjectileService ProjectileService { get; set; }

        public MapService MapService { get; set; }

        public InventoryService InventoryService { get; set; }

        private MetavirusGames.Framework.Data.Tiled.MapReference _mapReference;

        private SoundEffect _respawnSound;

        public MetavirusGames.Framework.Settings Settings { get; private set; }

        internal IGraphicsEngine GraphicsEngine;

        internal IAssetService AssetsService;

        internal TiledMapRenderer TiledMapRender;

        internal Player Player = null;

        internal SkeletonBoss SkeletonBoss = null;

        public static IList<Character> Characters = null;

        internal CharacterControllerService CharacterControllerService { get; private set; }

        internal PhysicsService PhysicsService { get; private set; }

        internal EntityRenderer EntityRenderer { get; private set; }

        internal StatsRenderer StatsRenderer { get; private set; }

        internal GlyphRenderer GlyphRenderer { get; private set; }

        internal IList<Animation> AnimationQueue { get; private set; }

        internal Meter PlayerHealthMeter { get; private set; }

        internal Meter PlayerStaminaMeter { get; private set; }

        internal Meter BossHealthMeter { get; private set; }

        //internal Texture2D ItemsTexture { get; private set; }

        internal PopupManager PopupManager { get; private set; }

        internal InventoryRenderer InventoryRenderer { get; private set; }

        internal SoundEffect PickupSound { get; private set; }

        //internal static Inventory PlayerInventory;

        public bool IsPaused { get; private set; }

        private bool _isReloading = false;

        ContentManager content;

        SpriteFont gameFont;

        Random random = new Random();

        private SoundEffect _lockSound;

        private Texture2D _magicBall;

        float pauseAlpha;

        private InventorySelectorComponent inventorySelector;

        #endregion

        #region Initialization

        public GameplayScreen() {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
        }

        public override void Initialize() {

            if (!_isReloading) {

                ArtifactService = new ArtifactService();

                ArtifactService.Initialize();

                ProjectileService = new Services.ProjectileService();

                ProjectileService.Initialize();

                MapService = new MapService();

                MapService.Initialize();

                if (InventoryService == null) {
                    InventoryService = new InventoryService();

                    InventoryService.Initialize();

                    ServiceLocator.Instance.Register(typeof(InventoryService), InventoryService);
                }

                ServiceLocator.Instance.Register(typeof(ArtifactService), this.ArtifactService);

                ServiceLocator.Instance.Register(typeof(ProjectileService), this.ProjectileService);

                ServiceLocator.Instance.Register(typeof(MapService), MapService);

                TiledMapRender = new TiledMapRenderer(MapService.CurrentMap);

                ICamera camera = new BasicCamera();

                this.AssetsService = new AssetService((ScreenManager.Game as IXNAService));

                PopupManager.Initialize(this.AssetsService);

                this.spriteBatch = new SpriteBatch(ScreenManager.Game.GraphicsDevice);

                this.GraphicsEngine = new GraphicsEngine(spriteBatch, camera, this.AssetsService);

                ServiceLocator.Instance.Register(typeof(ICamera), camera);

                this.GraphicsEngine.ClearLights();

                IEnumerable<MetavirusGames.Framework.Data.Tiled.TiledObject> tiledObjects = MapService.CurrentMap.SelectObjects(o => o.Type == "Light");

                Texture2D lightTexture = this.AssetsService.GameService.ContentManager.Load<Texture2D>(@"Textures/effect_lightmap");

                foreach (var tiledObject in tiledObjects) {
                    int r = int.Parse(tiledObject.Properties["r"]);
                    int g = int.Parse(tiledObject.Properties["g"]);
                    int b = int.Parse(tiledObject.Properties["b"]);
                    int a = 200;

                    Color color = new Color(r, g, b, a);

                    Light light = new Light(lightTexture, tiledObject.X, tiledObject.Y - 16, lightTexture.Width / 2, lightTexture.Height / 2, 0f, 1f, color);

                    this.GraphicsEngine.AddLight(light);
                }

                this.PhysicsService = new PhysicsService();

                //this.ItemsTexture = this.AssetsService.LoadTexture(@"Content/Textures/Items.png");

                this._respawnSound = this.ScreenManager.Game.Content.Load<SoundEffect>(@"SFX/Respawn");

                this.PickupSound = this.ScreenManager.Game.Content.Load<SoundEffect>(@"SFX/coin");

                GraphicsEngine.Camera.SetZoom(2f);

                Characters = new List<Character>();

                var spawnObject = MapService.CurrentMap.SelectObjects(o => o.Type == "Spawn").FirstOrDefault();

                Vector2 spawnPosition = new Vector2(spawnObject.X, spawnObject.Y + 16);

                Player = new Player("Will", spawnPosition);

                Player.SetPosition(Player.Position - new Vector2(0, 32));

                Characters.Add(Player);

                this.EntityRenderer = new EntityRenderer(Characters);

                this.StatsRenderer = new StatsRenderer(Player, this.ScreenManager.Game.Content.Load<SpriteFont>(@"Fonts/04b03_16"));

                this.GlyphRenderer = new GlyphRenderer(MapService.CurrentMap);

                this._magicBall = ScreenManager.Game.Content.Load<Texture2D>(@"Textures/MagicBall");

                this.CharacterControllerService = new CharacterControllerService(Player, (ScreenManager.Game as TalesOfDescentGame));

                this.PopupManager = new PopupManager();

                this.AnimationQueue = new List<Animation>();

                this.PlayerHealthMeter = new Meter(this.AssetsService.GameService.ContentManager.Load<Texture2D>(@"Textures/HealthMeter"), this.Player.Health, this.Player.MaximumHealth,
                    new Rectangle(190, 22, 197, 35), new Rectangle(190, 70, 195, 26), new Rectangle(190, 118, 195, 28), new Vector2(10, 10),
                    this.ScreenManager.Game.Content.Load<SpriteFont>(@"Fonts/04b03_16"));

                this.PlayerHealthMeter.SetText(String.Format("HP {0}/{1}", this.Player.Health, this.Player.MaximumHealth));



                this.PlayerStaminaMeter = new Meter(this.AssetsService.GameService.ContentManager.Load<Texture2D>(@"Textures/HealthMeter"), this.Player.Stamina, this.Player.MaximumStamina,
                    new Rectangle(190, 22, 197, 35), new Rectangle(190, 94, 195, 26), new Rectangle(190, 118, 195, 28), new Vector2(45, 45),
                    this.ScreenManager.Game.Content.Load<SpriteFont>(@"Fonts/04b03_16"));

                this.PlayerStaminaMeter.SetText(String.Format("STA {0}/{1}", this.Player.Stamina, this.Player.MaximumStamina));


                base.Initialize();
            }
        }

        /// <summary>
        /// Load graphics content for the game.
        /// </summary>
        public override void LoadContent() {

            if (!_isReloading) {
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");

                gameFont = content.Load<SpriteFont>("gamefont");

                Animation boneAttack = new Animation("Bone", this.AssetsService.GameService.ContentManager.Load<Texture2D>(@"Textures/Animations/BoneAnimation"), 16, 16, 0.025f, new Vector2(), Color.White);

                AnimationService.Instance.AddAnimation("BoneAttack", boneAttack);

                Animation smokeAnimation = new Animation("Smoke", this.AssetsService.GameService.ContentManager.Load<Texture2D>(@"Textures/Animations/SmokeAnimation"), 16, 16, 0.05f, Vector2.Zero, Color.White, loop: false);

                AnimationService.Instance.AddAnimation("Smoke", smokeAnimation);

                Animation bossBoneAttack = new Animation("BoneBoss", this.AssetsService.GameService.ContentManager.Load<Texture2D>(@"Textures/Animations/SkeletonBossBoneAnimation"), 64, 64, 0.025f, new Vector2(), Color.White);

                AnimationService.Instance.AddAnimation("BossBoneAttack", bossBoneAttack);

                Animation squish = new Animation("Squish", this.AssetsService.GameService.ContentManager.Load<Texture2D>(@"Textures/Animations/Flames"), 16, 16, 0.075f, new Vector2(), Color.White, loop: true);

                AnimationService.Instance.AddAnimation("Squish", squish);

                _lockSound = ScreenManager.Game.Content.Load<SoundEffect>(@"SFX/Lock");

                foreach (Character character in Characters) {
                    character.Load(this.AssetsService);
                }

                Music.Play("DST-Aronara", true);

                // once the load has finished, we use ResetElapsedTime to tell the game's
                // timing mechanism that we have just finished a very long frame, and that
                // it should not try to catch up.
                ScreenManager.Game.ResetElapsedTime();

                this.InventoryRenderer = new InventoryRenderer(this.AssetsService.GameService.ContentManager.Load<Texture2D>(@"Textures/ItemsUI"), this.ScreenManager.Game.Content.Load<SpriteFont>(@"Fonts/04b03_16"));

                inventorySelector = new InventorySelectorComponent(ScreenManager.Game);

                inventorySelector.Initialize();

                ScreenManager.Game.Components.Add(inventorySelector);

            } else {
                _isReloading = false;
            }

        }


        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void UnloadContent() {
            Music.Stop();
            content.Unload();
        }


        #endregion

        #region Update and Draw

        /// <summary>
        /// Updates the state of the game. This method checks the GameScreen.IsActive
        /// property, so the game will stop updating when the pause menu is active,
        /// or if you tab away to a different application.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen) {
            base.Update(gameTime, otherScreenHasFocus, false);

            KeyboardState keyboard = Keyboard.GetState();
            GamePadState gamePad = GamePad.GetState(PlayerIndex.One);

            this.GlyphRenderer.Update(gameTime);

            UpdateCamera();

            UpdateUIInformation();

            if (!this.IsPaused && !_isReloading) {

                this.ProjectileService.Update(gameTime);

                //Process Character Specific Logic
                foreach (Character character in Characters) {
                    character.Update(gameTime);

                    UpdateEntitySpecificLogic(gameTime, character);

                    if (character is Player) {
                        MetavirusGames.Framework.Data.Tiled.ObjectLayer objectLayer =
                            (MetavirusGames.Framework.Data.Tiled.ObjectLayer)MapService.CurrentMap.SelectLayer<MetavirusGames.Framework.Data.Tiled.ObjectLayer>("Entities");

                        if (objectLayer != null) {

                            foreach (var gameObject in objectLayer.Objects) {
                                if (character.Bounds.Intersects(gameObject.Bounds)) {

                                    if (gameObject.Type == "Stairs") {

                                        string map = gameObject.Properties["Map"];
                                        int x = int.Parse(gameObject.Properties["X"]);
                                        int y = int.Parse(gameObject.Properties["Y"]);

                                        if (map == "this") {
                                            _isReloading = true;
                                            LoadingScreen.ReLoad(ScreenManager, true, PlayerIndex.One, false,
                                            this);
                                            character.SetPosition(new Vector2(x * 16, y * 16));
                                        }

                                    } else if (gameObject.Type == "Spikes") {
                                        character.Damage(null);
                                        
                                    }

                                } else if (character.Bounds.Inflate(4).Intersects(gameObject.Bounds)) {
                                    if (gameObject.Type == "Chest") {

                                        if (keyboard.IsKeyDown(Keys.C) && !previousKeyboard.IsKeyDown(Keys.C)) {
                                            if (gameObject.GID == gameObject.OriginalGID) {
                                                gameObject.GID += 1;
                                                string item = gameObject.Properties["Items"];
                                                MetavirusGames.Framework.Data.Tiled.TiledObject chestObject = MapService.CurrentMap.SelectObjects(o => o.Type == item).FirstOrDefault();
                                                int chestObjectGid = MapService.CurrentMap.GetGidForTileType(item);
                                                this.GlyphRenderer.AddGlyph(chestObject.GID, new Vector2(gameObject.X, gameObject.Y - 24), new Vector2(gameObject.X, gameObject.Y - 32));

                                                if (item == "Health Potion") {
                                                    InventoryService.AddItem(Items.HealthPotionId, 1);
                                                    InventoryService.AddItem(Items.ArrowId, 1);
                                                    InventoryService.AddItem(Items.IceArrowId, 1);
                                                    InventoryService.AddItem(Items.BombId, 1);
                                                    InventoryService.AddItem(Items.ScrollId, 1);
                                                    InventoryService.AddItem(Items.SpellbookId, 1);
                                                    InventoryService.AddItem(Items.StopWatchId, 1);
                                                    InventoryService.AddItem(Items.DivePearlId, 1);
                                                }
                                                

                                                _lockSound.Play(Music.Volume, 0f, 0f);

                                                InventoryService.SetCurrentItem(Items.HealthPotionId);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    UpdateEntityCollisions(character);

                    this.PhysicsService.ResolvePosition(character, MapService.CurrentMap, gameTime);
                    //}

                } //End foreach:character

          
                if (keyboard.IsKeyDown(Keys.M) && !previousKeyboard.IsKeyDown(Keys.M)) {

                    Vector2 velocity = new Vector2();

                    if (Player.Heading == EntityHeading.Up) {
                        velocity = new Vector2(0, -250);
                    }
                    if (Player.Heading == EntityHeading.Down) {
                        velocity = new Vector2(0, 250);
                    }
                    if (Player.Heading == EntityHeading.Left) {
                        velocity = new Vector2(-250, 0);
                    }
                    if (Player.Heading == EntityHeading.Right) {
                        velocity = new Vector2(250, 0);
                    }

                    ProjectileService.AddProjectile(new MagicBall(Player.Position + new Vector2(8, 8), velocity));
                }

                
                    
                UpdateCombat();

                UpdateAnimationQueue(gameTime);

                UpdateDeadCharacters();

                UpdateTransition(coveredByOtherScreen);

                previousKeyboard = keyboard;

                previousGamepad = gamePad;

            }
        }

        private void UpdateCamera() {
            GraphicsEngine.Camera.SetPosition(
                -Player.Position.X + (this.ScreenManager.Game as IXNAService).Graphics.GraphicsDevice.Viewport.Width / 4 - (16 / 2),
                -Player.Position.Y + (this.ScreenManager.Game as IXNAService).Graphics.GraphicsDevice.Viewport.Height / 4 - (16 / 2));
        }

        private void UpdateUIInformation() {

            this.PlayerHealthMeter.SetText(String.Format("HP {0}/{1}", this.Player.Health, this.Player.MaximumHealth));

            this.PlayerHealthMeter.SetCurrentValue(Player.Health);

            this.PlayerHealthMeter.SetMaxValue(Player.MaximumHealth);

            this.PlayerStaminaMeter.SetText(String.Format("STA {0}/{1}", this.Player.Stamina, this.Player.MaximumStamina));

            this.PlayerStaminaMeter.SetCurrentValue(Player.Stamina);

            this.PlayerStaminaMeter.SetMaxValue(Player.MaximumStamina);

            if (this.BossHealthMeter != null) {
                this.BossHealthMeter.SetCurrentValue(SkeletonBoss.Health);

                this.BossHealthMeter.SetMaxValue(SkeletonBoss.MaximumHealth);

                this.BossHealthMeter.SetText(String.Format("HP {0}/{1}", this.SkeletonBoss.Health, this.SkeletonBoss.MaximumHealth));
            }

        }

        private void UpdateCombat() {
            if (this.Player.State == EntityState.Attacking) {
                IEnumerable<Character> attackedCharacters = Characters.Where(c => this.Player.AttackBounds.Inflate(4).Intersects(c.Bounds) && (c != this.Player));

                foreach (Character character in attackedCharacters) {
                    character.Damage(this.Player);
                }

            }
        }

        private void UpdateTransition(bool coveredByOtherScreen) {
            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
                pauseAlpha = Math.Min(pauseAlpha + 1f / 32, 1);
            else
                pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);
        }

        bool added = false;

        private void UpdateEntitySpecificLogic(GameTime gameTime, Character character) {

            //if (!added) {
            //    Animation squishAnimation = (Animation)AnimationService.Instance.GetAnimation("Squish").Clone();
            //    squishAnimation.SetPosition(Player.Position);
            //    this.AnimationQueue.Add(squishAnimation);
            //    added = true;
            //}

            if (character is Skeleton) {
                Skeleton skeleton = (character as Skeleton);

                List<BoneAttackEntity> bonesToRemove = new List<BoneAttackEntity>();

                foreach (BoneAttackEntity bone in skeleton.Bones) {
                    Rectangle boneBounds = new Rectangle((int)bone.Position.X, (int)bone.Position.Y, 16, 16);

                    float distance = Vector2.Distance(Player.Position, bone.Position);

                    if (distance < 300) {
                        if (boneBounds.Intersects(Player.Bounds)) {
                            Player.Damage(null);
                            bone.Collide(Player);
                        }

                        this.PhysicsService.ResolvePosition(bone, MapService.CurrentMap, gameTime);

                        if (!bone.IsAlive) {
                            bonesToRemove.Add(bone);

                            Animation animation = (Animation)AnimationService.Instance.GetAnimation("Smoke").Clone();
                            animation.SetPosition(bone.Position);
                            this.AnimationQueue.Add(animation);
                        }
                    } else {
                        bonesToRemove.Add(bone);
                    }
                }

                foreach (BoneAttackEntity bone in bonesToRemove) {
                    skeleton.Bones.Remove(bone);
                }
            }

            if (character is Zombie) {

                if (character.State == EntityState.Attacking) {

                    if (character.Bounds.Inflate(8).Intersects(Player.Bounds)) {
                        Player.Damage(character);

                        int poisonChance = random.Next(1, 101);

                        if (poisonChance <= 10) {
                            if (!(Player.Effect is HealOverTime)) {
                                Player.SetEffect(new PoisonEffect(3, Player));
                            }
                        }
                    }
                }
            }

            if (character is SkeletonBoss) {
                SkeletonBoss skeleton = (character as SkeletonBoss);
                List<BoneAttackEntity> bonesToRemove = new List<BoneAttackEntity>();

                foreach (BoneAttackEntity bone in skeleton.Bones) {
                    Rectangle boneBounds = new Rectangle((int)bone.Position.X + 4, (int)bone.Position.Y + 4, 64 - 8, 64 - 8);

                    if (boneBounds.Intersects(Player.Bounds)) {
                        Player.Damage(skeleton);
                        bone.Collide(null);
                    }

                    this.PhysicsService.ResolvePosition(bone, MapService.CurrentMap, gameTime);

                    if (!bone.IsAlive) {
                        bonesToRemove.Add(bone);
                    }
                }

                foreach (BoneAttackEntity bone in bonesToRemove) {
                    skeleton.Bones.Remove(bone);
                }
            }
        }

        private void UpdateEntityCollisions(Character character) {

            KeyboardState keyboard = Keyboard.GetState();
            GamePadState gamePad = GamePad.GetState(PlayerIndex.One);

            foreach (Character otherCharacter in Characters) {
                if (character != otherCharacter) {


                    if (otherCharacter is ShopKeeper) {

                        if (Player.Bounds.Inflate(10).Intersects(otherCharacter.Bounds.Inflate(10))) {
                            if ((keyboard.IsKeyDown(Keys.C) && !previousKeyboard.IsKeyDown(Keys.C)) || (gamePad.Buttons.A == ButtonState.Pressed && previousGamepad.Buttons.A != ButtonState.Pressed)) {
                                PopupManager.Show("ShopKeepMessage", new Vector2(otherCharacter.Position.X - 50, otherCharacter.Position.Y - 25));
                            }
                        } else {
                            if (PopupManager.CurrentMessageKey == "ShopKeepMessage") {
                                PopupManager.Close();
                            }
                        }
                    }

                    if (otherCharacter is Player) {
                        if (Player.Bounds.Inflate(10).Intersects(character.Bounds.Inflate(10))) {
                            character.SetVelocity(Vector2.Zero);
                        }
                    }

                    if (character.Bounds.Intersects(otherCharacter.Bounds)) {
                        otherCharacter.Collide(character);
                    }

                    Vector2 currentPosition = character.Position;

                    //TODO: Measure from centers for accurate distance regardless of sprite size
                    float xDistance = otherCharacter.Center.X - character.Center.X;
                    float yDistance = otherCharacter.Center.Y - character.Center.Y;

                    if (character.Velocity != Vector2.Zero) {
                        //Debug.Print(currentPosition.ToString());
                        if (Math.Abs(xDistance) > Math.Abs(yDistance)) {
                            if (character.Center.X < otherCharacter.Center.X) { //TODO: Redo player on entity collisions
                                //this.PhysicsService.ResolveCollision(character, otherCharacter, this.map, this.tileset, Framework.Runtime.Axis.Horizontal);
                            } if (character.Center.X >= otherCharacter.Center.X) {
                                //this.PhysicsService.ResolveCollision(character, otherCharacter, this.map, this.tileset, Framework.Runtime.Axis.Horizontal);
                            }
                        } else {
                            if (character.Center.Y < otherCharacter.Center.Y) {
                                //this.PhysicsService.ResolveCollision(character, otherCharacter, this.map, this.tileset, Framework.Runtime.Axis.Vertical);
                            } if (character.Center.Y >= otherCharacter.Center.Y) {
                                //this.PhysicsService.ResolveCollision(character, otherCharacter, this.map, this.tileset, Framework.Runtime.Axis.Vertical);
                            }
                        }
                    }

                }
            } //End foreach:character (Physics)
        }

        /// <summary>
        /// Update the world animation queue
        /// </summary>
        /// <param name="gameTime"></param>
        private void UpdateAnimationQueue(GameTime gameTime) {

            foreach (Animation animation in this.AnimationQueue) {

                animation.Update(gameTime);

            }

            for (int i = 0; i < this.AnimationQueue.Count; i++) {
                if (this.AnimationQueue[i].Enabled == false) {
                    this.AnimationQueue.RemoveAt(i);
                    i--;
                }
            }
        }

        private Vector2 PositionAdditive {
            get {
                return new Vector2(random.Next(-4, 4), random.Next(-4, 4));
            }
        }
        /// <summary>
        /// Updates Dead Characters - Either removing them from the characters list
        /// or respawning them during specific scenarios
        /// </summary>
        private void UpdateDeadCharacters() {
            //Remove dead characters
            for (int i = 0; i < Characters.Count; i++) {
                if (Characters[i].IsAlive == false) {
                    Animation animation = (Animation)AnimationService.Instance.GetAnimation("Smoke").Clone();
                    animation.SetPosition(Characters[i].Position);
                    this.AnimationQueue.Add(animation);

                    if (!(Characters[i] is Player)) {

                        if (Characters[i] is Skeleton || Characters[i] is Zombie) {
                            //this.map.AddItem(new BoneEssence(this.ItemsTexture, Characters[i].Position + PositionAdditive));
                            //this.map.AddItem(new FerocityEssence(this.ItemsTexture, Characters[i].Position + PositionAdditive));

                            int healthPotionChance = random.Next(1, 101);
                            int staminaPotionChance = random.Next(1, 101);
                            int lifeEssenceChance = random.Next(1, 101);

                            //if (healthPotionChance <= 30) {
                            //    this.map.AddItem(new HealthPotion(this.ItemsTexture, Characters[i].Position + PositionAdditive, true));
                            //}

                            //if (staminaPotionChance <= 30) {
                            //    this.map.AddItem(new StaminaPotion(this.ItemsTexture, Characters[i].Position + PositionAdditive, true));
                            //}

                            //if (lifeEssenceChance <= 40) {
                            //    this.map.AddItem(new LifeEssence(this.ItemsTexture, Characters[i].Position + PositionAdditive));
                            //}

                        } else if (Characters[i] is SkeletonBoss) {
                            //this.map.AddItem(new BoneEssence(this.ItemsTexture, Characters[i].Position + PositionAdditive, 8, 15));
                            //this.map.AddItem(new FerocityEssence(this.ItemsTexture, Characters[i].Position + PositionAdditive));

                            int healthPotionChance = random.Next(1, 101);
                            int lifeEssenceChance = random.Next(1, 101);

                            //if (healthPotionChance <= 80) {
                            //    this.map.AddItem(new HealthPotion(this.ItemsTexture, Characters[i].Position + PositionAdditive, true));
                            //}
                            //if (lifeEssenceChance <= 70) {
                            //    this.map.AddItem(new LifeEssence(this.ItemsTexture, Characters[i].Position + PositionAdditive, 5, 10));
                            //}
                        }

                        //Room room = map.GetRoomOfEntity(this.tileset, Characters[i]);

                        //if (room.Characters.Contains(Characters[i])) {
                        //    room.Characters.Remove(Characters[i]);
                        //}

                        Characters.RemoveAt(i);

                        i--;
                    } else {
                        this.Player.StuckInCurrentRoom = false;
                        this.Player.Spawn();
                        _respawnSound.Play(Music.Volume, 0f, 0f);
                    }
                }
            }
        }


        /// <summary>
        /// Lets the game respond to player input. Unlike the Update method,
        /// this will only be called when the gameplay screen is active.
        /// </summary>
        public override void HandleInput(InputState input) {
            if (input == null)
                throw new ArgumentNullException("input");

            // Look up inputs for the active player profile.
            int playerIndex = (int)ControllingPlayer.Value;

            PlayerIndex testIndex;

            if (input.IsNewKeyPress(Keys.T, PlayerIndex.One, out testIndex)) {

            }

            this.IsPaused = input.IsPauseGame(ControllingPlayer);

            KeyboardState keyboardState = input.CurrentKeyboardStates[playerIndex];
            GamePadState gamePadState = input.CurrentGamePadStates[playerIndex];

            if (!input.IsPauseGame(ControllingPlayer) && !inventorySelector.IsOpen) {
                this.CharacterControllerService.Update();
            }
            // The game pauses either if the user presses the pause button, or if
            // they unplug the active gamepad. This requires us to keep track of
            // whether a gamepad was ever plugged in, because we don't want to pause
            // on PC if they are playing with a keyboard and have no gamepad at all!
            bool gamePadDisconnected = !gamePadState.IsConnected &&
                                       input.GamePadWasConnected[playerIndex];

            if (input.IsPauseGame(ControllingPlayer) || gamePadDisconnected) {
                ScreenManager.AddScreen(new PauseMenuScreen(), PlayerIndex.One);
            }
        }


        /// <summary>
        /// Draws the gameplay screen.
        /// </summary>
        public override void Draw(GameTime gameTime) {

            this.GraphicsEngine.BeginDraw();

            //this.GraphicsEngine.Draw(gameTime, this.MapRenderer);

            this.GraphicsEngine.Draw(gameTime, TiledMapRender);

            ProjectileService.Draw(gameTime, spriteBatch);

            ArtifactService.Draw(gameTime, spriteBatch);

            this.GraphicsEngine.Draw(gameTime, this.EntityRenderer);

            foreach (Animation animation in this.AnimationQueue) {
                this.GraphicsEngine.Draw(gameTime, animation);
            }

            

            this.GraphicsEngine.Draw(gameTime, this.PopupManager);

            this.GraphicsEngine.Draw(gameTime, this.GlyphRenderer);

            this.GraphicsEngine.EndDraw();

            this.GraphicsEngine.BeginDrawUI();

            this.GraphicsEngine.DrawUI(gameTime, this.PlayerHealthMeter);

            this.GraphicsEngine.DrawUI(gameTime, this.PlayerStaminaMeter);

            //TODO: Redo this entire system
            //if (this.map.Boss != null && (this.map.Boss as ILivingEntity).IsAlive) {
            //    Vector2 playerRoomCoordinates = this.map.GetRoomPositionOfEntity(this.tileset, Player);

            //    if (playerRoomCoordinates == new Vector2(this.map.ExitRoomPosition.X, this.map.ExitRoomPosition.Y)) {
            //        this.GraphicsEngine.DrawUI(gameTime, this.BossHealthMeter);
            //    }
            //}


            this.GraphicsEngine.DrawUI(gameTime, this.InventoryRenderer);

            //this.GraphicsEngine.DrawUI(gameTime, this.StatsRenderer);

            this.GraphicsEngine.EndDrawUI();
            float talpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);

            // If the game is transitioning on or off, fade it out to black.
            if (TransitionPosition > 0 || pauseAlpha > 0) {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);

                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }


        #endregion
    }
}
