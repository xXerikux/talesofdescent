﻿using Nuclex.UserInterface.Controls.Desktop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.UI {
    partial class LoginDialog : WindowControl {

        /// <summary>Initializes a new GUI demonstration dialog</summary>
        public LoginDialog() {
            InitializeComponent();
        }

        /// <summary>Called when the user clicks on the okay button</summary>
        /// <param name="sender">Button the user has clicked on</param>
        /// <param name="arguments">Not used</param>
        private void okClicked(object sender, EventArgs arguments) {
            Close();
        }

        /// <summary>Called when the user clicks on the cancel button</summary>
        /// <param name="sender">Button the user has clicked on</param>
        /// <param name="arguments">Not used</param>
        private void cancelClicked(object sender, EventArgs arguments) {
            Close();
        }

    }
}
