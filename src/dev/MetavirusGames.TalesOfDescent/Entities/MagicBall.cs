﻿using MetavirusGames.Framework.Runtime;
using MetavirusGames.Framework.Services;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Entities {
    public sealed class MagicBall : Projectile {
        private IXNAService _gameService{
            get{
                return ServiceLocator.Instance.Get<IXNAService>();
            }
        }

        public MagicBall(Vector2 position, Vector2 velocity)
            : base(null, new Rectangle(), position, velocity) {

            if (velocity.Y != 0) {
                if (velocity.Y < 0) {
                    this.Rotation = MathHelper.ToRadians(0);
                } else {
                    this.Rotation = MathHelper.ToRadians(180);
                }
            }

            if (velocity.X != 0) {
                if (velocity.X < 0) {
                    this.Rotation = MathHelper.ToRadians(270);
                } else {
                    this.Rotation = MathHelper.ToRadians(90);
                }
            }

            this.Texture = _gameService.ContentManager.Load<Texture2D>(@"Textures/MagicBall");

            this.Source = new Rectangle(0, 0, 16, 16);

        }

    }
}
