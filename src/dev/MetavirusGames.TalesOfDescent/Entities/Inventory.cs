﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Entities {
    public class Inventory {

        public int BoneEssences;

        public int FerocityEssences;

        public int EnduranceEssences;

        public int LifeEssences;

        public int MagicEssences;

        public int SkeletonKeys;

        public int Keys;

        public int HealthPotions;

        public int StaminaPotions;

        public Dictionary<int, int> Items { get; set; }

        public int CurrentItem { get; set; }

        public Inventory() {

            Items = new Dictionary<int, int>();

        }

        public void AddItem(int id, int quantity) {

            if (Items.ContainsKey(id)) {
                Items[id] += quantity;
            } else {
                Items[id] = quantity;
            }

        }

        public void RemoveItem(int id, int quantity) {

            if (Items.ContainsKey(id)) {
                Items[id] -= quantity;

                if (Items[id] < 0) {
                    Items[id] = 0;
                }
            }

        }

        public void SetCurrentItem(int id) {

            if (Items.ContainsKey(id)) {
                CurrentItem = id;
            } else {
                throw new Exception("Can't set an item as the current selection if it is not in the inventory!");
            }
        }

        public int GetCurrentCount(int id) {

            if (Items.ContainsKey(id)) {
                return Items[id];
            }

            return 0;
        }
    }
}
