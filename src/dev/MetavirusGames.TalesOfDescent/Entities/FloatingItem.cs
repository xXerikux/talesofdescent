﻿using MetavirusGames.Framework.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Entities {
    public abstract class FloatingItem : Item {

        private Vector2 basePosition;

        private float bounce;

        public override Vector2 Position {
            get {
                return basePosition + new Vector2(0.0f, bounce);
            }
        }

        public FloatingItem(string name, Texture2D texture, Vector2 position, Rectangle destination, int quantity)
            : base(name, texture, position, destination, quantity) {

            this.basePosition = position;

            CanPickup = false;
        }

        public override void Update(GameTime gameTime) {

            // Bounce control constants
            const float BounceHeight = 0.18f;
            const float BounceRate = 3.0f;
            const float BounceSync = -0.75f;

            // Bounce along a sine curve over time.
            // Include the X coordinate so that neighboring gems bounce in a nice wave pattern.            
            double t = gameTime.TotalGameTime.TotalSeconds * BounceRate + Position.X * BounceSync;
            bounce = (float)Math.Sin(t) * BounceHeight * Bounds.Height;

            base.Update(gameTime);
        }
    }
}
