﻿using MetavirusGames.Framework.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Entities {
    public class FerocityEssence : Item {

        public FerocityEssence(Texture2D texture, Vector2 position, int minQuantity = 1, int maxQuanity = 6)
            : base("Essence of Ferocity", texture, position, new Rectangle(16, 0, 16, 16), 1) {

            this.CanPickup = true;

            this.Quantity = Random.Next(minQuantity, maxQuanity);

        }
    }
}
