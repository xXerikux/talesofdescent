﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Entities {
    public sealed class Checkpoint : FloatingItem {

        public Checkpoint(Texture2D texture, Vector2 position)
            : base("Checkpoint", texture, position, new Rectangle(64, 32, 16, 16), 0) {

                CanPickup = false;
        }
    }
}
