﻿using MetavirusGames.Framework.Runtime;
using MetavirusGames.Framework.Services;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Entities {
    public sealed class Items {

        public static readonly int CoinId = 1;

        public static readonly int HealthPotionId = 2;

        public static readonly int ManaPotionId = 3;

        public static readonly int BowId = 4;

        public static readonly int ArrowId = 5;

        public static readonly int IceArrowId = 6;

        public static readonly int FireArrowId = 7;

        public static readonly int KeyId = 8;

        public static readonly int BossKeyId = 9;

        public static readonly int SkeletonKeyId = 10;

        public static readonly int StopWatchId = 11;

        public static readonly int BombId = 12;

        public static readonly int ScrollId = 13;

        public static readonly int SpellbookId = 14;

        public static readonly int DivePearlId = 15;

        public static readonly int SmokeBombId = 16;

        public static readonly int RopeId = 17;

        public static Texture2D ItemsTexture { get; private set; }

        public static Texture2D ItemsUITexture { get; private set; }

        public static Rectangle[] Data { get; private set; }

        public static Rectangle[] UIData { get; private set; }

        public static void Initialize() {
            IXNAService gameService = ServiceLocator.Instance.Get<IXNAService>();

            ItemsTexture = gameService.ContentManager.Load<Texture2D>(@"Textures/NewItems");

            ItemsUITexture = gameService.ContentManager.Load<Texture2D>(@"Textures/ItemsUI");

            Data = new Rectangle[ItemsTexture.Width / 16 * ItemsTexture.Height / 16];
            UIData = new Rectangle[ItemsUITexture.Width / 32 * ItemsUITexture.Height / 32];

            int i = 0;
            for (int y = 0; y < (ItemsTexture.Height / 16); y++) {
                for (int x = 0; x < (ItemsTexture.Width / 16); x++) {
                    Data[i] = new Rectangle(x * 16, y * 16, 16, 16);
                    UIData[i] = new Rectangle(x * 32, y * 32, 32, 32);
                    i++;
                }
            }
        }

        public static Rectangle GetItemData(int id) {
            return Data[id];
        }

        public static Rectangle GetUIItemData(int id) {
            return UIData[id];
        }
    }
}
