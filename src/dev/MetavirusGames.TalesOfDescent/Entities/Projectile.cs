﻿using MetavirusGames.Framework.Graphics.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Entities {
    public abstract class Projectile : IRenderable {

        public Vector2 Position { get; set; }

        public Vector2 Velocity { get; set; }

        public Texture2D Texture { get; set; }

        public Rectangle Source { get; set; }

        public float Rotation { get; set; }

        public Rectangle Bounds {
            get {
                return new Rectangle((int)Position.X, (int)Position.Y, 16, 16);
            }
        }

        public bool IsAlive { get; set; }

        public Projectile(Texture2D texture, Rectangle source, Vector2 position, Vector2 velocity) {

            this.Texture = texture;

            this.Position = position;

            this.Velocity = velocity;

            this.Source = source;

            IsAlive = true;
        }

        public void Update(GameTime gameTime) {

            Position += Velocity * (float)gameTime.ElapsedGameTime.TotalSeconds;

        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch) {

            spriteBatch.Draw(Texture, Position, Source, Color.White, Rotation, new Vector2(8, 8), 1f, SpriteEffects.None, 0f);

        }
    }
}
