﻿using MetavirusGames.Framework.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Entities {
    public class HealthPotion : Item {

        public HealthPotion(Texture2D texture, Vector2 position, bool canPickup)
            : base("Health Potion", texture, position, new Rectangle(0, 32, 16, 16), 1) {

                CanPickup = canPickup;
        }
    }
}
