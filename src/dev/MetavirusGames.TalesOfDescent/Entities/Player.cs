﻿using MetavirusGames.Framework.Data;
using MetavirusGames.Framework.Entities;
using MetavirusGames.Framework.Runtime;
using MetavirusGames.Framework.Services;
using MetavirusGames.TalesOfDescent.Components;
using MetavirusGames.TalesOfDescent.Graphics;
using MetavirusGames.TalesOfDescent.Screens;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Entities {
    public class Player : Character {

        private const int MaxHealth = 15;

        private const int BaseAttackPower = 3;

        private const int MaxStamina = 10;

        private Timer _healthTimer;

        private Timer _bleedTimer;

        private Random _random;

        public ArtifactService ArtifactService {
            get {
                return ServiceLocator.Instance.Get<ArtifactService>();
            }
        }

        public Player(string name, Vector2 position)
            : base(name, position, MaxHealth, MaxHealth, MaxStamina, MaxStamina, BaseAttackPower, 1, EntityState.Idle, EntityHeading.Down) {

                _random = new Random(new Random().Next(0, 10000));

                _healthTimer = new Timer(() => {

                    if (Health > (int)Math.Round((double)(MaximumHealth / 2)) && Health < MaximumHealth) {
                        Heal(1);
                    } else if (Health < (int)Math.Round((double)(MaximumHealth / 2)) && Health > 0) {
                        Heal(1);
                    }

                }, 10f, repeats: true);
        }

        public override void Load(IAssetService assetService) {

            RegisterAnimation(EntityHeading.Up, EntityState.Walking,
                new Animation("WalkUp", assetService.GameService.ContentManager.Load<Texture2D>(@"Textures/Animations/HeroWalkUp"), 16, 16, 0.25f, Vector2.Zero, Color.White));
            RegisterAnimation(EntityHeading.Down, EntityState.Walking,
                new Animation("WalkDown", assetService.GameService.ContentManager.Load<Texture2D>(@"Textures/Animations/HeroWalkDown"), 16, 16, 0.25f, Vector2.Zero, Color.White));
            RegisterAnimation(EntityHeading.Left, EntityState.Walking,
                new Animation("WalkLeft", assetService.GameService.ContentManager.Load<Texture2D>(@"Textures/Animations/HeroWalkLeft"), 16, 16, 0.25f, Vector2.Zero, Color.White));
            RegisterAnimation(EntityHeading.Right, EntityState.Walking,
                new Animation("WalkRight", assetService.GameService.ContentManager.Load<Texture2D>(@"Textures/Animations/HeroWalkRight"), 16, 16, 0.25f, Vector2.Zero, Color.White));

            RegisterAnimation(EntityHeading.Up, EntityState.Idle,
                new Animation("IdleUp", assetService.GameService.ContentManager.Load<Texture2D>(@"Textures/Animations/HeroWalkUp"), 16, 16, 0.25f, Vector2.Zero, Color.White, firstFrameOnly: true));
            RegisterAnimation(EntityHeading.Down, EntityState.Idle,
                new Animation("IdleDown", assetService.GameService.ContentManager.Load<Texture2D>(@"Textures/Animations/HeroWalkDown"), 16, 16, 0.25f, Vector2.Zero, Color.White, firstFrameOnly: true));
            RegisterAnimation(EntityHeading.Left, EntityState.Idle,
                new Animation("IdleLeft", assetService.GameService.ContentManager.Load<Texture2D>(@"Textures/Animations/HeroWalkLeft"), 16, 16, 0.25f, Vector2.Zero, Color.White, firstFrameOnly: true));
            RegisterAnimation(EntityHeading.Right, EntityState.Idle,
                new Animation("IdleRight", assetService.GameService.ContentManager.Load<Texture2D>(@"Textures/Animations/HeroWalkRight"), 16, 16, 0.25f, Vector2.Zero, Color.White, firstFrameOnly: true));

            RegisterAnimation(EntityHeading.Up, EntityState.Attacking,
                new Animation("AttackUp", assetService.GameService.ContentManager.Load<Texture2D>(@"Textures/Animations/HeroAttackUp"), 16, 24, 0.05f, Vector2.Zero, Color.White, yOffset: -4, loop: false, animationEndAction: () => 
                {
                    ResetAttack = true;
                    this.SetState(EntityState.Idle); 
                }));
            RegisterAnimation(EntityHeading.Down, EntityState.Attacking,
                new Animation("AttackDown", assetService.GameService.ContentManager.Load<Texture2D>(@"Textures/Animations/HeroAttackDown"), 16, 24, 0.05f, Vector2.Zero, Color.White, yOffset: -4, loop: false, animationEndAction: () => 
                {
                    ResetAttack = true;
                    this.SetState(EntityState.Idle); 
                }));
            RegisterAnimation(EntityHeading.Left, EntityState.Attacking,
                new Animation("AttackLeft", assetService.GameService.ContentManager.Load<Texture2D>(@"Textures/Animations/HeroAttackLeft"), 32, 16, 0.05f, Vector2.Zero, Color.White, xOffset: -8, loop: false, animationEndAction: () => 
                {
                    ResetAttack = true;
                    this.SetState(EntityState.Idle); 
                }));
            RegisterAnimation(EntityHeading.Right, EntityState.Attacking,
                new Animation("AttackRight", assetService.GameService.ContentManager.Load<Texture2D>(@"Textures/Animations/HeroAttackRight"), 32, 16, 0.05f, Vector2.Zero, Color.White, xOffset: -8, loop: false, animationEndAction: () => 
                {
                    ResetAttack = true;
                    this.SetState(EntityState.Idle);
                }));

            AttackSound = assetService.GameService.ContentManager.Load<SoundEffect>(@"SFX/Sword");

            base.Load(assetService);
        }

        public override void Update(GameTime gameTime) {

            var player = this;

            _healthTimer.Update(gameTime);

            if (_bleedTimer != null) {
                _bleedTimer.Update(gameTime);
            }

            base.Update(gameTime);
        }

        public override void Damage(Framework.Entities.Interfaces.ILivingEntity attacker) {

            _bleedTimer = new Timer(() => {

                int bloodPattern = _random.Next(4, 8);

                Rectangle source = new Rectangle(bloodPattern * 16, 0, 16, 16);

                ArtifactService.AddArtifact(new Artifact(ArtifactService.Texture, new Vector2(Bounds.Center.X, Bounds.Center.Y), source, 1f));

            }, 0.250f, 9, true);
            
            base.Damage(attacker);
        }
    }
}
