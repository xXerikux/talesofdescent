﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Data.Interfaces {
    public interface IPersistenceAdapter {

        Type Type { get; }

        void Create(object value);

        void Save(object value);

        object Load(string key);

    }
}
