﻿using MetavirusGames.TalesOfDescent.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Data {
    public sealed class Persistence {

        private static object SyncRoot = new object();

        private List<IPersistenceAdapter> _adapters;

        public Persistence() {

            this._adapters = new List<IPersistenceAdapter>();

        }

        /// <summary>
        /// Saves an object to an adapter if available
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        public void Create(object value) {

            IEnumerable<IPersistenceAdapter> adaptersForType = this._adapters.Where(a => a.Type == value.GetType());

            foreach (IPersistenceAdapter adapter in adaptersForType) {

                adapter.Create(value);

            }

        }

        /// <summary>
        /// Saves an object to an adapter if available
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        public void Save(object value) {

            IEnumerable<IPersistenceAdapter> adaptersForType = this._adapters.Where(a => a.Type == value.GetType());

            foreach (IPersistenceAdapter adapter in adaptersForType) {

                adapter.Save(value);

            }

        }

        /// <summary>
        /// Registers an 
        /// </summary>
        /// <param name="adapter"></param>
        public void RegisterAdapter(IPersistenceAdapter adapter) {

            lock (SyncRoot) {
                _adapters.Add(adapter);
            }

        }

        public void RemoveAdapter(IPersistenceAdapter adapter) {

            lock (SyncRoot) {
                _adapters.Remove(adapter);
            }

        }


    }
}
