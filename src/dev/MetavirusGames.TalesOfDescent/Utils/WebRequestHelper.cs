﻿using MetavirusGames.TalesOfDescent.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace MetavirusGames.TalesOfDescent.Utils {
    public class WebRequestHelper {

        public static void PostGameData(string uri, string key, string value) {

            string postData = String.Format("{0}/setGameData?api_key={1}&game_id={2}&response={3}&key={4}&value={5}",
                HttpUtility.HtmlEncode(uri),
                HttpUtility.HtmlEncode(ScoreoidService.API_KEY),
                HttpUtility.HtmlEncode(ScoreoidService.GAME_KEY),
                HttpUtility.HtmlEncode("JSON"),
                HttpUtility.HtmlEncode(key),
                HttpUtility.HtmlEncode(value)
            );

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(postData);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            WebResponse response = request.GetResponse ();

            Stream stream = response.GetResponseStream();
            string responseData = String.Empty;

            using (StreamReader reader = new StreamReader(stream)) {
                responseData = reader.ReadToEnd();
            }

            Console.WriteLine(postData);

        }

        public static string GetGameData(string uri, string key) {

            //string parameters = BuildParametersString("api_key=" + ScoreoidService.API_KEY, "game_id=" + ScoreoidService.GAME_KEY, "response=JSON", "key=" + key);

            string postData = String.Format("{0}/getGameData?api_key={1}&game_id={2}&response={3}&key={4}",
                HttpUtility.HtmlEncode(uri),
                HttpUtility.HtmlEncode(ScoreoidService.API_KEY),
                HttpUtility.HtmlEncode(ScoreoidService.GAME_KEY),
                HttpUtility.HtmlEncode("JSON"),
                HttpUtility.HtmlEncode(key)
            );

            //string postData = String.Format("{0}/gameGameData?{1}", uri, parameters);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(postData);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            Timer timer = new Timer(new TimerCallback(
                obj => {

                    if (obj is HttpWebRequest) {
                        (obj as HttpWebRequest).Abort();

                        throw new Exception(String.Format("Timed out while getting data from Scoreoid for key: {0}", key));
                    }

                }), request, 1000, Timeout.Infinite);

            WebResponse response = request.GetResponse();

            Stream stream = response.GetResponseStream();
            string responseData = String.Empty;

            using (StreamReader reader = new StreamReader(stream)) {
                responseData = reader.ReadToEnd();
            }

            Debug.Print(responseData);

            return responseData;
        }

        private static string BuildParametersString(params string[] args) {
            string parameters = String.Empty;

            for (int i = 0; i < args.Length; i++) {

                if (i == 0) {
                    parameters += String.Format("{0}", HttpUtility.HtmlEncode(args[i]));
                } else {
                    parameters += String.Format("&{0}", HttpUtility.HtmlEncode(args[i]));
                }

            }

            return parameters;

        }

    }
}
