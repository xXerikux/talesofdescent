﻿#region Using Statements
using MetavirusGames.Framework;
using MetavirusGames.Framework.Data;
using MetavirusGames.Framework.Data.Tiled.IO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
#endregion

namespace MetavirusGames.TalesOfDescent {
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {

            //try {
                

                if (File.Exists("TalesOfDescent.cfg")) {
                    SettingsSerializer serializer = new SettingsSerializer();
                    Settings settings = serializer.Deserialize("TalesOfDescent.cfg");

                    using (var game = new TalesOfDescentGame(settings))
                        game.Run();
                } else {
                    using (var game = new TalesOfDescentGame())
                        game.Run();
                }
//            } catch (Exception ex) {

//#if DEBUG
//                throw ex;
//#endif
//                Console.WriteLine(ex.ToString());

//                string file = "crash.log";

//                if (!File.Exists(file)) {
//                    using (StreamWriter writer = new StreamWriter(file)) {
//                        writer.WriteLine(ex.ToString());
//                    }
//                } else {

//                    using (StreamWriter writer = File.AppendText(file)) {
//                        writer.WriteLine(ex.ToString());
//                    }
//                }


                //string arguments = String.Format("version={0} error=\"{1}\" timestamp={2}", "1.0", ex.ToString(), DateTime.Now.ToString());

                //Process.Start("Error.exe", arguments);
            //}

        }
    }
#endif
}
