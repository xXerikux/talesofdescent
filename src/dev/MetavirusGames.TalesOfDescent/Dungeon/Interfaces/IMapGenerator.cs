﻿using MetavirusGames.Framework.Services;

namespace MetavirusGames.TalesOfDescent.Dungeon.Interfaces {
    public interface IMapGenerator {

        Map Generate(Difficulty difficulty, IXNAService gameService);

    }
}
