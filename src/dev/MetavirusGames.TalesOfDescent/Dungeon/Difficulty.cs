﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Dungeon {
    public enum Difficulty {
        Easy,
        Normal,
        Medium,
        Hard
    }
}
