﻿#region Using Statements
using MetavirusGames.Framework.Audio;
using MetavirusGames.Framework.Runtime;
using MetavirusGames.Framework.ScreenManager;
using MetavirusGames.Framework.Services;
using MetavirusGames.TalesOfDescent.Data;
using MetavirusGames.TalesOfDescent.Data.Adapters;
using MetavirusGames.TalesOfDescent.Entities;
using MetavirusGames.TalesOfDescent.Screens;
using MetavirusGames.TalesOfDescent.Services;
using MetavirusGames.TalesOfDescent.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Nuclex.UserInterface;
using Nuclex.UserInterface.Controls.Desktop;
using System;
#endregion

namespace MetavirusGames.TalesOfDescent {
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class TalesOfDescentGame : Game, IXNAService {

        public GraphicsDeviceManager Graphics { get; private set; }

        private Nuclex.Input.InputManager input;

        private Nuclex.UserInterface.GuiManager gui;

        public ContentManager ContentManager {
            get {
                return this.Content;
            }
        }

        internal MetavirusGames.Framework.Settings Settings { get; private set; }

        private ScreenManager screenManager;

        static readonly string[] preloadAssets =
        {
            "gradient",
        };

        public TalesOfDescentGame()
            : this(new MetavirusGames.Framework.Settings() { ScreenWidth = 800, ScreenHeight = 600, FullScreen = false }) {
        }

        public TalesOfDescentGame(MetavirusGames.Framework.Settings settings)
            : base() {

            this.Settings = settings;

            this.Graphics = new GraphicsDeviceManager(this) {
                PreferredBackBufferWidth = settings.ScreenWidth,
                PreferredBackBufferHeight = settings.ScreenHeight,
                IsFullScreen = settings.FullScreen,
                GraphicsProfile = Microsoft.Xna.Framework.Graphics.GraphicsProfile.HiDef
            };

            ServiceLocator.Instance.Register(typeof(IXNAService), this);

            this.Content.RootDirectory = "Content";

            screenManager = new ScreenManager(this);

            this.gui = new GuiManager(this.Services);

            this.input = new Nuclex.Input.InputManager(this.Services);

            Components.Add(screenManager);

            Components.Add(input);

            Components.Add(gui);

            this.gui.DrawOrder = 10000;

            Screen screen = new Nuclex.UserInterface.Screen(800, 600);

            this.gui.Screen = screen;

            //createDesktopControls(screen);

            screen.Desktop.Bounds = new Nuclex.UserInterface.UniRectangle(
        new Nuclex.UserInterface.UniScalar(0.1f, 0.0f), new Nuclex.UserInterface.UniScalar(0.1f, 0.0f), // x and y = 10%
        new Nuclex.UserInterface.UniScalar(0.8f, 0.0f), new Nuclex.UserInterface.UniScalar(0.8f, 0.0f) // width and height = 80%
      );

            // Create an instance of the demonstration dialog and add it to the desktop
            // control, which means it will become visible and interactive.
            //screen.Desktop.Children.Add(new LoginDialog());

            // Activate the first screens.
            screenManager.AddScreen(new BackgroundScreen(), PlayerIndex.One);
            screenManager.AddScreen(new MainMenuScreen(), PlayerIndex.One);

            Music.Volume = 0.1f;

            Music.Initialize(new AssetService(this));

            this.IsMouseVisible = true;
            //Persistence persistence = new Persistence();

            //persistence.RegisterAdapter(new ScoreoidPlayerAdapter());

            //persistence.Create(new Player("test", new Vector2()));

            //ScoreoidService service = new ScoreoidService();

            //service.SetGameData("test", "test123", () => { }, error => { }, () => { });

            //service.GetGameData("test", () => { }, error => { }, () => { });
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent() {

            Items.Initialize();

            foreach (string asset in preloadAssets) {
                Content.Load<object>(asset);
            }

            //Player.Load(this.AssetsService);
            //Skeleton.Load(this.AssetsService);
        }

        private void createDesktopControls(Nuclex.UserInterface.Screen mainScreen) {

            // Button to open another "New Game" dialog
            ButtonControl newGameButton = new ButtonControl();
            newGameButton.Text = "New Game";
            newGameButton.Bounds = new UniRectangle(
              new UniScalar(1.0f, -190.0f), new UniScalar(1.0f, -32.0f), 100, 32
            );
            newGameButton.Pressed += delegate(object sender, EventArgs arguments) {
                // Insert at index 0 to make it the firstmost window
                this.gui.Screen.Desktop.Children.Insert(0, new LoginDialog());
            };
            mainScreen.Desktop.Children.Add(newGameButton);

            // Button through which the user can quit the application
            ButtonControl quitButton = new ButtonControl();
            quitButton.Text = "Quit";
            quitButton.Bounds = new UniRectangle(
              new UniScalar(1.0f, -80.0f), new UniScalar(1.0f, -32.0f), 80, 32
            );
            quitButton.Pressed += delegate(object sender, EventArgs arguments) { Exit(); };
            mainScreen.Desktop.Children.Add(quitButton);

        }
    }
}
