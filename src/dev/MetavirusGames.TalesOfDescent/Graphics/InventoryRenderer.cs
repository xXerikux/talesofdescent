﻿using MetavirusGames.Framework.Graphics;
using MetavirusGames.Framework.Graphics.Interfaces;
using MetavirusGames.Framework.Runtime;
using MetavirusGames.Framework.Services;
using MetavirusGames.TalesOfDescent.Entities;
using MetavirusGames.TalesOfDescent.Services;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Graphics {
    public sealed class InventoryRenderer : IRenderable {

        private Texture2D _texture;

        private SpriteFont _font;

        private Texture2D _itemFrame;

        private Texture2D _useItemGlyph;

        public MapService MapService {
            get {
                return ServiceLocator.Instance.Get<MapService>();
            }
        }

        public InventoryService InventoryService {
            get {
                return ServiceLocator.Instance.Get<InventoryService>();
            }
        }

        public InventoryRenderer(Texture2D texture, SpriteFont font) {

            _texture = texture;

            _font = font;

            IXNAService gameService = ServiceLocator.Instance.Get<IXNAService>();

            _itemFrame = gameService.ContentManager.Load<Texture2D>(@"UI/ItemFrame");

            _useItemGlyph = gameService.ContentManager.Load<Texture2D>(@"UI/KEYBOARD_KEY_Z");

        }

        public void Draw(Microsoft.Xna.Framework.GameTime gameTime, SpriteBatch spriteBatch) {

            //spriteBatch.Draw(_texture, new Vector2(275, 10), new Rectangle(0, 0, 32, 32), Color.White);

            //FontHelper.DrawString(spriteBatch, _font, new Vector2(295, 30), _inventory.BoneEssences.ToString(), Color.White, Color.Black, true);


            //spriteBatch.Draw(_texture, new Vector2(310, 10), new Rectangle(32, 0, 32, 32), Color.White);

            //FontHelper.DrawString(spriteBatch, _font, new Vector2(330, 30), _inventory.FerocityEssences.ToString(), Color.White, Color.Black, true);


            //spriteBatch.Draw(_texture, new Vector2(345, 10), new Rectangle(64, 0, 32, 32), Color.White);

            //FontHelper.DrawString(spriteBatch, _font, new Vector2(365, 30), _inventory.LifeEssences.ToString(), Color.White, Color.Black, true);


            //spriteBatch.Draw(_texture, new Vector2(380, 10), new Rectangle(96, 0, 32, 32), Color.White);

            //FontHelper.DrawString(spriteBatch, _font, new Vector2(400, 30), _inventory.EnduranceEssences.ToString(), Color.White, Color.Black, true);


            //spriteBatch.Draw(_texture, new Vector2(415, 10), new Rectangle(128, 0, 32, 32), Color.White);

            //FontHelper.DrawString(spriteBatch, _font, new Vector2(435, 30), _inventory.MagicEssences.ToString(), Color.White, Color.Black, true);

            //FontHelper.DrawString(spriteBatch, _font, new Vector2(45, spriteBatch.GraphicsDevice.Viewport.Height - 42), _inventory.HealthPotions.ToString(), Color.White, Color.Black, true);

            //spriteBatch.Draw(_texture, new Vector2(10, spriteBatch.GraphicsDevice.Viewport.Height - 42), new Rectangle(0, 64, 32, 32), Color.White);

            //FontHelper.DrawString(spriteBatch, _font, new Vector2(95, spriteBatch.GraphicsDevice.Viewport.Height - 42), _inventory.StaminaPotions.ToString(), Color.White, Color.Black, true);

            //spriteBatch.Draw(_texture, new Vector2(60, spriteBatch.GraphicsDevice.Viewport.Height - 42), new Rectangle(32, 64, 32, 32), Color.White);

            spriteBatch.Draw(_itemFrame, new Vector2(spriteBatch.GraphicsDevice.Viewport.Width / 2 - _itemFrame.Width / 2, 10), Color.White);

            //var tiledObject = _map.SelectObjects(o => o.Type == _inventory.CurrentItem).FirstOrDefault();

            if (InventoryService.CurrentItem > 0) {
                Vector2 itemPosition;// = new Vector2(spriteBatch.GraphicsDevice.Viewport.Width / 2 - 20, 16);

                if (InventoryService.CurrentItem == Items.HealthPotionId) {
                    itemPosition = new Vector2(spriteBatch.GraphicsDevice.Viewport.Width / 2 - 20, 18);
                } else {
                    itemPosition = new Vector2((spriteBatch.GraphicsDevice.Viewport.Width / 2) - 16, 18);
                }
                spriteBatch.Draw(_texture, itemPosition, Items.GetUIItemData(InventoryService.CurrentItem), Color.White);

                if (InventoryService.CurrentItem == Items.HealthPotionId) {
                    FontHelper.DrawString(spriteBatch, _font, new Vector2(spriteBatch.GraphicsDevice.Viewport.Width / 2 + 4, 32), InventoryService.GetCurrentCount(InventoryService.CurrentItem).ToString(), Color.White, Color.Black, true);
                }
            }

            spriteBatch.Draw(_useItemGlyph, new Vector2(spriteBatch.GraphicsDevice.Viewport.Width / 2 + 16, 48), Color.White);

        }

    }
}
