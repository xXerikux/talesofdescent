﻿using MetavirusGames.Framework.Graphics;
using MetavirusGames.Framework.Graphics.Interfaces;
using MetavirusGames.Framework.Services;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Graphics {
    public sealed class PopupManager : IRenderable {

        private static Dictionary<string, string> _messages;

        private static string _currentPopup = "";

        private static Vector2 _position;

        private static bool _initialized;

        private static SpriteFont _font;

        public static string CurrentMessageKey;

        public static void Initialize(IAssetService assetService) {

            if (!_initialized) {
                _messages = new Dictionary<string, string>();

                _font = assetService.GameService.ContentManager.Load<SpriteFont>(@"Fonts/04b03_06");

                //_messages["NotEnough"] = assetService.GameService.ContentManager.Load<Texture2D>(@"UI/NotEnough");
                //_messages["BoneRunes"] = assetService.GameService.ContentManager.Load<Texture2D>(@"UI/BoneRunes");
                _messages["ShopKeepMessage"] = "Just walk up to the item\nand press C to purchase!";
                _messages["NotEnough"] = "You don't have\nenough essences for\nthat right now!";
                _messages["BoneRunes"] = "When you are ready \nto purchase, press C!";
                _messages["Dungeon"] = "If you are ready to \nenter the dungeon, \nPress C";
                _initialized = true;
            }
        }

        public static void Show(string key, Vector2 position) {

            if (_messages.ContainsKey(key)) {
                CurrentMessageKey = key;
                _currentPopup = _messages[key];
                _position = position;
            }
        }

        public static void Close() {

            _currentPopup = "";

        }

        public void Draw(Microsoft.Xna.Framework.GameTime gameTime, SpriteBatch spriteBatch) {

            if (_currentPopup != "") {

                FontHelper.DrawString(spriteBatch, _font, _position, _currentPopup, Color.White, Color.Black, true);

            }
        }
    }
}
