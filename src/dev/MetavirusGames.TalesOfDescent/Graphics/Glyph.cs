﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Graphics {
    public sealed class Glyph {

        public int GID { get; set; }

        public Vector2 Position { get; set; }

        public Vector2 InitialPosition { get; set; }

        public Vector2 DestinationPosition { get; set; }

    }
}
