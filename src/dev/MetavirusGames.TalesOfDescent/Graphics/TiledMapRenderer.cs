﻿using MetavirusGames.Framework.Data.Tiled;
using MetavirusGames.Framework.Graphics.Interfaces;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Graphics {
    public sealed class TiledMapRenderer : IRenderable {

        private Map _map;

        private MetavirusGames.Framework.Data.Tiled.MapReference _mapReference;

        public TiledMapRenderer(Map map) {

            this._map = map;

            this._mapReference = new MapReference(this._map);

        }

        public void Draw(Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch) {

            for (int y = 0; y < this._map.Height; y++) {
                for (int x = 0; x < this._map.Width; x++) {

                    foreach (Layer layer in _map.Layers){

                        if (layer is TileLayer){
                            TileLayer tileLayer = (layer as TileLayer);
                            int gid = tileLayer.Data[x, y];

                            if (gid > 0) {
                                MetavirusGames.Framework.Data.Tiled.Tileset tilesetForTile = this._mapReference.GetTilesetForGID(gid);

                                gid = gid - (tilesetForTile.FirstGID - 1);

                                if (layer.Name == "Collisions") {
                                    if (GlobalSettings.ShowCollisionLayer) {
                                        spriteBatch.Draw(tilesetForTile.Texture, new Vector2(x * tilesetForTile.TileWidth, y * tilesetForTile.TileHeight), tilesetForTile.Data[gid], new Color(1f, 1f, 1f, layer.Opacity));
                                    }
                                } else {
                                    spriteBatch.Draw(tilesetForTile.Texture, new Vector2(x * tilesetForTile.TileWidth, y * tilesetForTile.TileHeight), tilesetForTile.Data[gid], Color.White);
                                }
                            }
                        }
                        
                    }

                }
            }

            ObjectLayer objectLayer = (ObjectLayer)_map.SelectLayer<ObjectLayer>("Entities");

            foreach (TiledObject tiledObject in objectLayer.Objects) {

                if (tiledObject.Type != "Light" && tiledObject.Type != "Spawn") {
                    int gid = tiledObject.GID;

                    if (gid > 0) {
                        MetavirusGames.Framework.Data.Tiled.Tileset tilesetForTile = _map.GetTilesetForGid(gid);
                        gid = gid - (tilesetForTile.FirstGID - 1);

                        spriteBatch.Draw(tilesetForTile.Texture, new Vector2(tiledObject.X, tiledObject.Y - tilesetForTile.TileHeight), tilesetForTile.Data[gid], Color.White);

                    }
                }
            }


        }
    }
}
