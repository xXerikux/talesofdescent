﻿using MetavirusGames.Framework.Graphics.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Graphics {
    public sealed class Artifact : IRenderable {

        public Texture2D Texture { get; set; }

        public Vector2 Position { get; set; }

        public float Rotation { get; set; }

        public Rectangle Source { get; set; }

        public Artifact(Texture2D texture, Vector2 position, Rectangle source, float rotation) {

            this.Texture = texture;

            this.Position = position;

            this.Rotation = rotation;

            this.Source = source;

        }

        public void Draw(Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch) {
            spriteBatch.Draw(Texture, Position, Source, Color.White, Rotation, new Vector2(8, 8), 1f, SpriteEffects.None, 0f);
        }
    }
}
