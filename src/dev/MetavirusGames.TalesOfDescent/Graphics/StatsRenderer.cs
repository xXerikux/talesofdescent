﻿using MetavirusGames.Framework.Entities;
using MetavirusGames.Framework.Graphics;
using MetavirusGames.Framework.Graphics.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Graphics {
    public sealed class StatsRenderer : IRenderable {

        private Character _character;

        private SpriteFont _font;

        public StatsRenderer(Character character, SpriteFont font) {

            this._character = character;

            this._font = font;

        }

        public void Draw(Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch) {

            FontHelper.DrawString(spriteBatch, this._font, new Vector2(10, 75), "Attack: " + _character.AttackPower.ToString(), Color.White, Color.Black, true);

            FontHelper.DrawString(spriteBatch, this._font, new Vector2(10, 100), "Defense: " + _character.Defense.ToString(), Color.White, Color.Black, true);

        }

    }
}
