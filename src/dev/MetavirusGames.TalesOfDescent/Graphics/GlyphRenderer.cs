﻿using MetavirusGames.Framework.Data.Tiled;
using MetavirusGames.Framework.Graphics.Interfaces;
using MetavirusGames.Framework.Runtime;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Graphics {
    public sealed class GlyphRenderer : IRenderable {

        private Map _map;

        private MapReference _mapReference;

        private List<Glyph> _glyphs;

        private List<Timer> _glyphRemovalQueue;

        public GlyphRenderer(Map map) {

            this._map = map;

            this._mapReference = new MapReference(this._map);

            this._glyphs = new List<Glyph>();

            this._glyphRemovalQueue = new List<Timer>();

        }

        public void Update(GameTime gameTime) {

            foreach (Glyph glyph in _glyphs) {

                Vector2 distance = Vector2.Subtract(glyph.DestinationPosition, glyph.InitialPosition);
                distance.Normalize();

                if (glyph.Position.Y > glyph.DestinationPosition.Y) {
                    glyph.Position += distance * (float)gameTime.ElapsedGameTime.TotalSeconds * 20;
                } else {
                    glyph.Position = glyph.DestinationPosition;

                    this._glyphRemovalQueue.Add(new Timer(() => {
                        this._glyphs.Remove(glyph);
                    }, 3));
                }

            }

            foreach (var item in _glyphRemovalQueue){

                item.Update(gameTime);

            }

        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch) {
            
            foreach (Glyph glyph in _glyphs) {

                MetavirusGames.Framework.Data.Tiled.Tileset tileset = _mapReference.GetTilesetForGID(glyph.GID);

                int gid = glyph.GID - (tileset.FirstGID - 1);

                spriteBatch.Draw(tileset.Texture, glyph.Position, tileset.Data[gid], Color.White);

            }
        }

        public void AddGlyph(int gid, Vector2 initialPosition, Vector2 destinationPosition) {

            this._glyphs.Add(new Glyph() { GID = gid, Position = initialPosition, InitialPosition = initialPosition, DestinationPosition = destinationPosition });

        }
    }
}
