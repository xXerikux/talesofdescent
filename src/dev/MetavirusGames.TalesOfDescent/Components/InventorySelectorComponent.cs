using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using MetavirusGames.TalesOfDescent.Screens;
using MetavirusGames.TalesOfDescent.Entities;
using MetavirusGames.Framework.Audio;
using MetavirusGames.TalesOfDescent.Services;
using MetavirusGames.Framework.Runtime;


namespace MetavirusGames.TalesOfDescent {
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    /// 
    public enum EasingState {
        EasingIn,
        EasingOut,
        Idle
    }

    public class InventorySelectorComponent : Microsoft.Xna.Framework.DrawableGameComponent {
        SpriteBatch spriteBatch;
        Texture2D _inventoryFrame;
        Texture2D _smallOrangeSquare;
        Texture2D _selector;
        SoundEffect _selectItemSound;

        Vector2 _position;
        Vector2 _destinationPosition;
        EasingState State = EasingState.Idle;
        List<Color> _items;
        int _index;

        Vector2 _originalPosition;
        Vector2 _originalDestination;
        KeyboardState _previousKeyboard;

        public InventoryService InventoryService {
            get {
                return ServiceLocator.Instance.Get<InventoryService>();
            }
        }

        public bool IsOpen {
            get {
                return this.State == EasingState.EasingIn;
            }
        }

        public InventorySelectorComponent(Game game)
            : base(game) { }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize() {

            _inventoryFrame = Game.Content.Load<Texture2D>(@"Textures/InventoryFrame");

            _smallOrangeSquare = Game.Content.Load<Texture2D>(@"Textures/ItemsUI");

            _selector = Game.Content.Load<Texture2D>(@"Textures/Selector");

            _selectItemSound = Game.Content.Load<SoundEffect>(@"SFX/selectitem");

            _position = new Vector2(Game.GraphicsDevice.Viewport.Width / 2 - _inventoryFrame.Width / 2, Game.GraphicsDevice.Viewport.Height + 16);

            _destinationPosition = new Vector2(Game.GraphicsDevice.Viewport.Width / 2 - _inventoryFrame.Width / 2, Game.GraphicsDevice.Viewport.Height / 2 - _inventoryFrame.Height / 2);

            _originalPosition = _position;

            _originalDestination = _destinationPosition;

            _items = new List<Color>();
            _items.Add(Color.Green);
            _items.Add(Color.Blue);
            _items.Add(Color.Red);

            base.Initialize();
        }

        public override void Update(GameTime gameTime) {
            KeyboardState keyboard = Keyboard.GetState();

            if (keyboard.IsKeyDown(Keys.Space) && !_previousKeyboard.IsKeyDown(Keys.Space)) {
                if (State == EasingState.Idle || State == EasingState.EasingOut) {
                    State = EasingState.EasingIn;
                    _position = _originalPosition;
                    _destinationPosition = _originalDestination;

                } else {
                    State = EasingState.EasingOut;
                    _position = _originalDestination;
                    _destinationPosition = _originalPosition;
                }
            }

            if (State == EasingState.EasingIn || State == EasingState.EasingOut) {
                _position = Vector2.Lerp(_position, _destinationPosition, (float)gameTime.ElapsedGameTime.TotalSeconds * 6f);
            }

            if (State == EasingState.EasingIn) {
                if (keyboard.IsKeyDown(Keys.Left) && !_previousKeyboard.IsKeyDown(Keys.Left)) {
                    if (_index - 1 < 0) {
                        _index = InventoryService.Items.Count() - 1;
                    } else {
                        _index -= 1;
                    }

                    _selectItemSound.Play(Music.Volume, 0f, 0f);
                }

                if (keyboard.IsKeyDown(Keys.Right) && !_previousKeyboard.IsKeyDown(Keys.Right)) {
                    if (_index + 1 > InventoryService.Items.Count() - 1) {
                        _index = 0;
                    } else {
                        _index += 1;
                    }

                    _selectItemSound.Play(Music.Volume, 0f, 0f);
                }

                if (InventoryService.Items.Count() > 0) {
                    int key = InventoryService.Items.Keys.ToList()[_index];
                    InventoryService.SetCurrentItem(key);
                }

            }

            _previousKeyboard = keyboard;

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime) {
            //Game.GraphicsDevice.Clear(Color.CornflowerBlue);

            if (spriteBatch == null) {
                spriteBatch = new SpriteBatch(Game.GraphicsDevice);
            }

            spriteBatch.Begin();

            spriteBatch.Draw(_inventoryFrame, _position, Color.White);



            if (InventoryService.Items.Keys.Count > 0 && _index >= 0) {
                //int currentItemIndex = 0;

                int key = InventoryService.Items.Keys.ToList()[_index];

                int negativeKey = GetNextNegativeIndex(_index);

                int previousItemKey = InventoryService.Items.Keys.ToList()[negativeKey];

                int positiveKey = GetNextPositiveIndex(_index);

                int nextItemKey = InventoryService.Items.Keys.ToList()[positiveKey];
                //GameplayScreen.PlayerInventory.Items.TryGetValue(key, out currentItemIndex);


                Rectangle currentItem = Items.GetUIItemData(key);
                Rectangle previousItem = Items.GetUIItemData(previousItemKey);
                Rectangle nextItem = Items.GetUIItemData(nextItemKey);

                spriteBatch.Draw(_smallOrangeSquare, _position + new Vector2((_inventoryFrame.Width / 2 - 32 / 2), (_inventoryFrame.Height / 2 - 32 / 2)), currentItem, Color.White);

                spriteBatch.Draw(_selector, _position + new Vector2((_inventoryFrame.Width / 2 - 48 / 2), (_inventoryFrame.Height / 2 - 48 / 2)), Color.White);

                spriteBatch.Draw(_smallOrangeSquare, _position + new Vector2((_inventoryFrame.Width / 2 - 32 / 2), (_inventoryFrame.Height / 2 - 32 / 2)) + new Vector2(-64, 0), previousItem, Color.White);

                spriteBatch.Draw(_smallOrangeSquare, _position + new Vector2((_inventoryFrame.Width / 2 - 32 / 2), (_inventoryFrame.Height / 2 - 32 / 2)) + new Vector2(64, 0), nextItem, Color.White);

            }



            //spriteBatch.Draw(_smallOrangeSquare, _position + new Vector2((_inventoryFrame.Width / 2 - 16 / 2), (_inventoryFrame.Height / 2 - 16 / 2)) + new Vector2(-64, 0), new Rectangle(32, 0, 32, 32), Color.White);

            //spriteBatch.Draw(_smallOrangeSquare, _position + new Vector2((_inventoryFrame.Width / 2 - 16 / 2), (_inventoryFrame.Height / 2 - 16 / 2)) + new Vector2(64, 0), new Rectangle(32, 0, 32, 32), Color.White);

            spriteBatch.End();

            base.Draw(gameTime);
        }
        private int GetNextNegativeIndex(int index) {
            if (index - 1 < 0) {
                index = InventoryService.Items.Count() - 1;
            } else {
                index -= 1;
            }

            return index;
        }

        private int GetNextPositiveIndex(int index) {
            if (index + 1 > InventoryService.Items.Count() - 1) {
                index = 0;
            } else {
                index += 1;
            }

            return index;
        }
    }
}
