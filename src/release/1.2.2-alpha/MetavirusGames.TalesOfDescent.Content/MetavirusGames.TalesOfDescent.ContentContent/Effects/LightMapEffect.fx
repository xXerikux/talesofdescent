float4x4 World;
float4x4 View;
float4x4 Projection;
texture Texture;
texture LightMap;
float2 CanvasSize;

sampler TextureSampler = sampler_state
{
	Texture = <Texture>;
	MINFILTER = POINT;
	MAGFILTER = POINT;
	MIPFILTER = POINT;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};

sampler LightMapSampler = sampler_state
{
	Texture = <LightMap>;
	MINFILTER = POINT;
	MAGFILTER = POINT;
	MIPFILTER = POINT;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};

struct VertexShaderInput
{
    float4 Position : POSITION0;
	float2 UV : TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
	float2 UV : TEXCOORD0;
	float2 ScreenPos : TEXCOORD1;
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;
    float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);

    output.Position = mul(viewPosition, Projection);
	output.UV = input.UV;

	output.ScreenPos = worldPosition;
	output.ScreenPos.xy /= CanvasSize;

    return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
	// Read the color of the sprite being drawn.
    float4 result = tex2D(TextureSampler, input.UV);

	// Multiply the sprite color by the light value represented on the lightmap.
	result *= tex2D(LightMapSampler, input.ScreenPos);

    return result;
}

technique Technique1
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 VertexShaderFunction();
        PixelShader = compile ps_3_0 PixelShaderFunction();
    }
}