﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MetavirusGames.TalesOfDescent.ExceptionManager {
    public partial class MainForm : Form {
        public MainForm() {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e) {
            string[] args = Environment.GetCommandLineArgs();

            foreach (string argument in args) {
                if (argument.Contains("error")) {
                    string[] message = argument.Split('=');

                    this.txtMessage.Text = message[1];
                }
            }
        }

        private void btnSend_Click(object sender, EventArgs e) {

            MailMessage mail = new MailMessage("donotreply@metavirusgames.com", "will@metavirusgames.com");
            SmtpClient client = new SmtpClient();
            client.Port = 25;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Credentials = new System.Net.NetworkCredential("will@metavirusgames.com", "Xec90paz!");
            client.Host = "mail.metavirusgames.com";
            mail.Subject = "Tales of Descent Error";
            mail.Body = this.txtMessage.Text;
            client.Send(mail);

            Application.Exit();
        }
    }
}
