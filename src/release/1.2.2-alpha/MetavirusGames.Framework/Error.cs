﻿using MetavirusGames.Framework.Graphics.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework {
    public sealed class Error {

        public static void FileNotFound(string value, params string[] args) {
            throw new FileNotFoundException(String.Format(value, args));
        }

        public static void DirectoryNotFound(string value, params string[] args) {
            throw new DirectoryNotFoundException(String.Format(value, args));
        }

        public static void InvalidOperation(string value, params string[] args) {
            throw new InvalidOperationException(String.Format(value, args));
        }

        public static void FileLoad(string value, params string[] args) {
            throw new FileLoadException(String.Format(value, args));
        }

        public static void InvalidGraphicsEngineState(string message, params string[] args) {
            throw new InvalidGraphicsStateException(message, args);
        }
    }
}
