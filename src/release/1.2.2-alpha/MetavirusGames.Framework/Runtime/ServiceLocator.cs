﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Runtime {
    public sealed class ServiceLocator {

        private Dictionary<Type, object> _objects;

        private static object SyncRoot = new object();

        private static ServiceLocator _instance;

        public static ServiceLocator Instance {
            get {
                if (_instance == null) {

                    lock (SyncRoot) {

                        if (_instance == null) {

                            return _instance = new ServiceLocator();
                        }

                    }

                }

                return _instance;
            }
        }

        private ServiceLocator() {

            _objects = new Dictionary<Type, object>();
        }

        /// <summary>
        /// Registers and constructs a default instance for the specified type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public void Register<T>() {

            var constructor = typeof(T).GetConstructor(Type.EmptyTypes);

            if (constructor != null) {

                var instance = Activator.CreateInstance<T>();

                if (!_objects.ContainsKey(typeof(T))) {
                    _objects.Add(typeof(T), instance);
                } else {
                    throw new Exception(String.Format("The service locator can only contain one key for the type: {0}", typeof(T).ToString()));
                }

            } else {
                throw new Exception("Objects registered this way must contain a parameterless constructor");
            }
        }

        public void Register(Type type, object instance) {

            if (instance == null) throw new Exception("Cannot provide a null instance");

            if (!_objects.ContainsKey(type)) {
                _objects.Add(type, instance);
            } else {
                throw new Exception(String.Format("The service locator can only contain one key for the type: {0}", type.ToString()));
            }

        }

        public T Get<T>() {

            if (_objects.ContainsKey(typeof(T))) {

                return (T)_objects[typeof(T)];

            } else {

                throw new Exception(String.Format("An object of the type {0} could not be found in the service locator", typeof(T).ToString()));

            }

        }
    }
}
