﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Runtime {
    public class Timer {

        private Action action;
        private float interval;
        private float tick;
        private bool repeats = false;
        public bool IsActive { get; private set; }
        private bool enabled = false;
        private int maxIntervals;
        private int currentIntervals;

        public Timer ( Action action, float interval, int maxIntervals = 0, bool repeats = false ) {
            if ( action == null ) { throw new Exception("Cannot pass a null action to a timer"); }
            
            this.action = action;

            this.interval = interval;

            this.repeats = repeats;

            this.IsActive = true;

            this.enabled = true;

            this.maxIntervals = maxIntervals;
        }

        public void Update ( GameTime gameTime ) {

            if ( IsActive ) {
                if ( tick <= interval ) {
                    tick += (float)gameTime.ElapsedGameTime.TotalSeconds;
                } else {
                    tick = 0f;

                    if ( action != null ) {
                        action();
                    }

                    if ( !repeats ) {
                        IsActive = false;
                    }

                    if ( maxIntervals > 0 ) {
                        currentIntervals += 1;

                        if ( currentIntervals >= maxIntervals ) {
                            Disable();
                            Deactivate();
                            return;
                        }
                    }
                }
            }
        }

        public void Disable () {
            enabled = false;
        }

        public void Enable () {
            enabled = true;
        }

        public void Reset () {
            tick = 0f;
        }

        public void Deactivate () {
            IsActive = false;
        }

        public void Activate () {
            IsActive = true;
        }
    }
}
