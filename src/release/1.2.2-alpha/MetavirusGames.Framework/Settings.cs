﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework {
    public sealed class Settings {

        public int ScreenWidth { get; set; }

        public int ScreenHeight { get; set; }

        public bool FullScreen { get; set; }

    }
}
