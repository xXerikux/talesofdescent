﻿using MetavirusGames.Framework.Entities;
using MetavirusGames.Framework.Graphics;
using MetavirusGames.Framework.Graphics.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.UI {
    public sealed class Meter : IRenderable {

        private int _maxValue;

        private int _currentValue;

        private Texture2D _texture;

        private Rectangle _frame;

        private Rectangle _fill;

        private Rectangle _background;

        private Vector2 _position;

        private float _fillAmount;

        private Rectangle _currentFillRectangle;

        private SpriteFont _font;

        private string _text = String.Empty;

        public Meter(Texture2D texture, int currentValue, int maxValue, Rectangle frame, Rectangle fill, Rectangle background, Vector2 position, SpriteFont font) {

            this._texture = texture;

            this._currentValue = currentValue;

            this._maxValue = maxValue;

            this._frame = frame;

            this._fill = fill;

            this._background = background;

            this._position = position;

            this._font = font;

            this.UpdateFillAmount();
        }

        public void Draw(Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch) {

            spriteBatch.Draw(this._texture, this._position, _background, Color.White);

            spriteBatch.Draw(this._texture, this._position, _frame, Color.White);

            spriteBatch.Draw(this._texture, this._position, this._currentFillRectangle, Color.White);

            if (this._text != String.Empty) {

                Vector2 messageSize = this._font.MeasureString(this._text);
                Vector2 textPosition = new Vector2(this._frame.Width / 2 - messageSize.X / 2, (this._frame.Height / 2 - messageSize.Y / 2));

                FontHelper.DrawString(spriteBatch, this._font, this._position + textPosition, this._text, Color.White, Color.Black, true);

            }

        }

        public void SetCurrentValue(int value) {

            this._currentValue = value;

            this.UpdateFillAmount();
        }

        public void SetMaxValue(int value) {

            this._maxValue = value;

            this.UpdateFillAmount();
        }

        public void SetText(string value) {

            this._text = value;

        }

        private void UpdateFillAmount() {

            float currentPercent = (float)this._currentValue / (float)this._maxValue;

            this._fillAmount = (int)((float)this._fill.Width * currentPercent);

            this._currentFillRectangle = new Rectangle(this._fill.X, this._fill.Y, (int)this._fillAmount, this._fill.Height);
        }

      
    }
}
