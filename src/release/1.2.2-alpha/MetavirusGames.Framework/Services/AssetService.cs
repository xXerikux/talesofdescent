﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Services {

    public class AssetService : IAssetService {


        public IGameService GameService { get; private set; }

        public AssetService(IGameService gameService) {

            this.GameService = gameService;

        }

        public Microsoft.Xna.Framework.Graphics.Texture2D LoadTexture(string filename) {

            if (File.Exists(filename)) {
                using (Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read)) {

                    Texture2D result = Texture2D.FromStream(this.GameService.Graphics.GraphicsDevice, stream);

                    return result;

                }
            } else {
                Error.FileNotFound("Could not find the file: {0}", filename);
                return null;
            }
        }
    }
}
