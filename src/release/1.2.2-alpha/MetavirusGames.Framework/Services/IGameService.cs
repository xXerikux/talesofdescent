﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Services {
    public interface IGameService {

        GraphicsDeviceManager Graphics { get; }

        ContentManager ContentManager { get; }
    }
}
