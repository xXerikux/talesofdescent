﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Services {
    public interface IAssetService {

        IGameService GameService { get; }

        Texture2D LoadTexture(string filename);

    }
}
