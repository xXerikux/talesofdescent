﻿using MetavirusGames.Framework.Services;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Audio {
    public sealed class Music {

        private static IAssetService AssetService;

        private static Song Song;

        public static float Volume { get; set; }

        public static void SetVolume(float value) {

            Volume = value;

            MediaPlayer.Volume = value;

        }

        public static void Initialize(IAssetService assetService) {

            AssetService = assetService;

            MediaPlayer.IsRepeating = true;
        }

        public static void Play(string filename, bool isLooped = false) {
            Song = AssetService.GameService.ContentManager.Load<Song>(@"Music/" + filename);

            MediaPlayer.IsRepeating = isLooped;

            MediaPlayer.Volume = Volume;

            MediaPlayer.Play(Song);
        }

        public static void Stop() {
            MediaPlayer.Stop();
        }

        public static void ChangeVolume(string key, float volume) {

            MediaPlayer.Volume = volume;

        }

        static void Stream_StreamStopped(object sender, EventArgs e) {

        }
    }
}
