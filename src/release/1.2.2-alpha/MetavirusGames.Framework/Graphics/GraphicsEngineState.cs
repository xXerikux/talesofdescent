﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Graphics {
    public enum GraphicsEngineState {
        Null,
        Begin,
        Drawing,
        End,
        Awaiting
    }
}
