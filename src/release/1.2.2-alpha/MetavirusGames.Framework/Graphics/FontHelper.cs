﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Graphics {
    public sealed class FontHelper {

        public static void DrawString(SpriteBatch spriteBatch, SpriteFont font, Vector2 position, string text, Color color, Color strokeColor, bool stroked) {

            if (stroked) {
                spriteBatch.DrawString(font, text, position + new Vector2(-1, -1), strokeColor);
                spriteBatch.DrawString(font, text, position + new Vector2(0, -1), strokeColor);
                spriteBatch.DrawString(font, text, position + new Vector2(1, -1), strokeColor);

                spriteBatch.DrawString(font, text, position + new Vector2(-1, 0), strokeColor);
                //spriteBatch.DrawString(font, text, position, color);
                spriteBatch.DrawString(font, text, position + new Vector2(+1, 0), strokeColor);

                spriteBatch.DrawString(font, text, position + new Vector2(-1, +1), strokeColor);
                spriteBatch.DrawString(font, text, position + new Vector2(0, +1), strokeColor);
                spriteBatch.DrawString(font, text, position + new Vector2(+1, +1), strokeColor);
            }

            spriteBatch.DrawString(font, text, position, color);
        }
    }
}
