﻿using MetavirusGames.Framework.Graphics.Interfaces;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Graphics {
    public sealed class BasicCamera : ICamera {

        private Matrix _matrix;

        private Vector3 _position;

        public BasicCamera() {

            _position = new Vector3();

            _position.Z = 0.5f;

        }

        public void Move(float x, float y) {

            _position.X += x;
            
            _position.Y += y;

        }

        public void Zoom(float z) {

            _position.Z += z;

        }

        public void SetPosition(float x, float y) {

            _position.X = x;

            _position.Y = y;

        }

        public void SetZoom(float z) {

            _position.Z = z;

        }

        public Microsoft.Xna.Framework.Matrix GetMatrix() {

            this._matrix = Matrix.CreateTranslation(this._position.X, this._position.Y, 0f) * Matrix.CreateScale(this._position.Z);

            return this._matrix;

        }
    }
}
