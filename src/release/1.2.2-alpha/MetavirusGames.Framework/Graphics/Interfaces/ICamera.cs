﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Graphics.Interfaces {
    public interface ICamera {

        void Move(float x, float y);

        void Zoom(float z);

        void SetPosition(float x, float y);

        void SetZoom(float z);

        Matrix GetMatrix();
    }
}
