﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.AI {
    public class PathfindingTile {
        private int tilesize = 30;
        private Vector2 tilePos;
        public Vector2 TilePos { get { return tilePos; } }
        private int id = 0;
        public int ID { get { return id; } }
        public int Type = 0;
        private Texture2D standardTexture; //value 0
        private Texture2D startTexture; //value 1
        private Texture2D endTexture; //value 2
        private Texture2D wayTexture; //value 4
        private Texture2D wallTexture; //value 3

        public Rectangle recTile {
            get { return new Rectangle((int)tilePos.X * tilesize, (int)tilePos.Y * tilesize, tilesize, tilesize); }
        }
        public PathfindingTile(Vector2 tilePos, GraphicsDevice gDevice, int newId) {
            this.tilePos = tilePos;
            this.id = newId;

            standardTexture = new Texture2D(gDevice, 1, 1);
            standardTexture.SetData(new[] { Color.Black });

            startTexture = new Texture2D(gDevice, 1, 1);
            startTexture.SetData(new[] { Color.Green });

            endTexture = new Texture2D(gDevice, 1, 1);
            endTexture.SetData(new[] { Color.Red });

            wayTexture = new Texture2D(gDevice, 1, 1);
            wayTexture.SetData(new[] { Color.Yellow });

            wallTexture = new Texture2D(gDevice, 1, 1);
            wallTexture.SetData(new[] { Color.Blue });

        }
        public void Draw(SpriteBatch spriteBatch) {
            switch (Type) {
                case 0:
                    spriteBatch.Begin();
                    spriteBatch.Draw(standardTexture, recTile, Color.White);
                    spriteBatch.End();
                    break;
                case 1:
                    spriteBatch.Begin();
                    spriteBatch.Draw(startTexture, recTile, Color.White);
                    spriteBatch.End();
                    break;
                case 2:
                    spriteBatch.Begin();
                    spriteBatch.Draw(endTexture, recTile, Color.White);
                    spriteBatch.End();
                    break;
                case 4:
                    spriteBatch.Begin();
                    spriteBatch.Draw(wayTexture, recTile, Color.White);
                    spriteBatch.End();
                    break;
                case 3:
                    spriteBatch.Begin();
                    spriteBatch.Draw(wallTexture, recTile, Color.White);
                    spriteBatch.End();
                    break;
            }
        }
    }

}
