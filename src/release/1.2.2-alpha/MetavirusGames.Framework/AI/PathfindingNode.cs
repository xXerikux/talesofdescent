﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.AI {
    public class PathfindingNode {
        public PathfindingNode(int cellIndex, PathfindingNode parent) {
            CellIndex = cellIndex;
            Parent = parent;
        }

        public int F { get; set; }
        public int G { get; set; }
        public int H { get; set; }
        public PathfindingNode Parent { get; set; }
        public int CellIndex { get; set; }
    }

}
