﻿using MetavirusGames.Framework.Entities;
using MetavirusGames.Framework.Runtime;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MetavirusGames.Framework.AI {
    public sealed class RandomMovementTowardsPlayerBehavior  : IAIBehavior {

        private Character _character;

        private Timer _movementTimer;

        private static Random _random = new Random(new Random().Next(0, 100000));

        public RandomMovementTowardsPlayerBehavior(Character character, float interval = 1f, int idleChanceThreshold = 20) {

            this._character = character;

            _movementTimer = new Timer(() => {

                int idleChance = _random.Next(0, 100);

                if (idleChance < idleChanceThreshold) {
                    Axis axis;

                    int axisChance = _random.Next(0, 100);

                    axis = axisChance <= 50 ? Axis.Horizontal : Axis.Vertical;

                    int moveChance = _random.Next(-100, 100);

                    int moveVector = moveChance <= 0 ? -1 : 1;

                    if (axis == Axis.Vertical) {
                        this._character.Move(new Vector2(0, moveVector));
                        if (moveVector < 0) {
                            this._character.SetHeading(EntityHeading.Up);

                        } else {
                            this._character.SetHeading(EntityHeading.Down);
                        }
                        this._character.SetState(EntityState.Walking);
                    } else {
                        this._character.Move(new Vector2(moveVector, 0));

                        if (moveVector < 0) {
                            this._character.SetHeading(EntityHeading.Left);
                        } else {
                            this._character.SetHeading(EntityHeading.Right);
                        }
                        this._character.SetState(EntityState.Walking);
                    }
                }

            }, interval, repeats: true);
        }

        public void Update(GameTime gameTime) {

            _movementTimer.Update(gameTime);

        }

    }
}
