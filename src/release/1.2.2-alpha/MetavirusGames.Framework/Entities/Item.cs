﻿using MetavirusGames.Framework.Entities.Interfaces;
using MetavirusGames.Framework.Graphics.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Entities {
    public abstract class Item : IGameEntity, IRenderable {
        public string Name { get; private set; }

        public virtual Microsoft.Xna.Framework.Vector2 Position { get; private set; }

        public Texture2D Texture { get; private set; }

        public Rectangle Coordinates { get; private set; }

        public int Quantity { get; protected set; }

        public bool CanPickup { get; set; }

        public bool IsForSale { get; set; }

        public int Cost { get; set; }

        public Rectangle CurrencyTextureCoordinates { get; set; }

        protected Random Random = new Random(new Random().Next(1000000));

        public Rectangle Bounds {
            get {
                return new Rectangle((int)Position.X, (int)Position.Y, 16, 16);
            }
        }

        public Item(string name, Texture2D texture, Vector2 position, Rectangle rectangle, int quantity) {

            this.Name = name;

            this.Texture = texture;

            this.Position = position;

            this.Coordinates = rectangle;

            this.Quantity = quantity;

        }

        public void Move(Microsoft.Xna.Framework.Vector2 amount) {

            Position += amount;

        }

        public virtual void Update(GameTime gameTime) { }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch) {

            spriteBatch.Draw(this.Texture, this.Position, this.Coordinates, Color.White);

        }
    }
}
