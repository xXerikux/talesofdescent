﻿using MetavirusGames.Framework.AI;
using MetavirusGames.Framework.Audio;
using MetavirusGames.Framework.Data;
using MetavirusGames.Framework.Entities.Interfaces;
using MetavirusGames.Framework.Graphics.Interfaces;
using MetavirusGames.Framework.Runtime;
using MetavirusGames.Framework.Services;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Entities {

    public abstract class Character : IGameEntity, ILivingEntity, ICombattant, IRenderable, IMoveableEntity {

        public string Name { get; private set; }

        public bool IsAlive { get; private set; }

        public Microsoft.Xna.Framework.Vector2 Position { get; private set; }

        public Microsoft.Xna.Framework.Vector2 Velocity { get; private set; }

        public Microsoft.Xna.Framework.Vector2 MovementDelta { get; set; }

        public int Health { get; private set; }

        public int MaximumHealth { get; private set; }

        public int Stamina { get; private set; }

        public int MaximumStamina { get; private set; }

        public int AttackPower { get; private set; }

        public int Defense { get; private set; }

        public Vector2 SpawnPosition { get; private set; }

        public float MovementSpeed { get; set; }

        public bool StuckInCurrentRoom { get; set; }

        public SoundEffect AttackSound { get; protected set; }

        protected bool ResetAttack = false;

        public SoundEffect DamagedSound {
            get {
                return DamagedSoundPool[new Random().Next(0, DamagedSoundPool.Count -1)];
            }
        }

        //Using a compound key to simplify state changing operations
        public Dictionary<Tuple<EntityState, EntityHeading>, Animation> Animations { get; private set; }

        public EntityState State { get; private set; }

        public EntityHeading Heading { get; private set; }

        public IList<IAIBehavior> Behaviors { get; private set; }

        public IAIBehavior CurrentBehavior { get; private set; }

        private bool _canBeAttacked = true;

        private Timer _flashTimer;

        private Timer _invulnerabilityTimer;

        private Timer _staminaReplenishmentTimer;

        private Color _renderColor = Color.White;

        protected IList<SoundEffect> DamagedSoundPool;

        public virtual Rectangle Bounds {
            get {
                return new Rectangle((int)Math.Round(Position.X) + 2, (int)Math.Round(Position.Y) + 4, 16 - 4, 16 - 4);
            }
        }

        public Vector2 Center {
            get {
                return new Vector2(Bounds.Center.X, Bounds.Center.Y);
            }
        }

        public Rectangle AttackBounds {
            get {
                if (Heading == EntityHeading.Up) {
                    return new Rectangle(Bounds.X, Bounds.Y - 16, Bounds.Width, Bounds.Height);
                }
                if (Heading == EntityHeading.Down) {
                    return new Rectangle(Bounds.X, Bounds.Y + 16, Bounds.Width, Bounds.Height);
                }
                if (Heading == EntityHeading.Left) {
                    return new Rectangle(Bounds.X - 16, Bounds.Y, Bounds.Width, Bounds.Height);
                }
                if (Heading == EntityHeading.Right) {
                    return new Rectangle(Bounds.X + 16, Bounds.Y, Bounds.Width, Bounds.Height);
                }

                return new Rectangle();
            }
        }

        public Animation CurrentAnimation {
            get {
                try {
                    return Animations[Tuple.Create<EntityState, EntityHeading>(this.State, this.Heading)];
                } catch (Exception ex) {
                    return null;
                }
            }
        }

        public Effects.Effect Effect { get; private set; }

        public Character(string name, Vector2 position, int health, int maxHealth, int stamina, int maxStamina, int attackPower, int defense, EntityState state, EntityHeading heading, bool stuckInCurrentRoom = false) {

            this.IsAlive = true;

            this.Name = name;

            this.Position = position;

            this.SpawnPosition = position;

            this.Health = health;

            this.MaximumHealth = maxHealth;

            this.Stamina = stamina;

            this.MaximumStamina = maxStamina;

            this.AttackPower = attackPower;

            this.Defense = defense;

            this.State = state;

            this.Heading = heading;

            this.Animations = new Dictionary<Tuple<EntityState, EntityHeading>, Animation>();

            this.Behaviors = new List<IAIBehavior>();

            this.MovementSpeed = 20f;

            this.StuckInCurrentRoom = stuckInCurrentRoom;

            _flashTimer = new Timer(() => {

                if (!_canBeAttacked) {

                    if (_renderColor == Color.White) {

                        if (this.Effect != null) {
                            _renderColor = this.Effect.FlashColor;
                        } else {
                            _renderColor = Color.Red;
                        }
                    } else {
                        _renderColor = Color.White;
                    }
                } else {
                    _renderColor = Color.White;
                }

            }, 0.05f, repeats: true);

            _invulnerabilityTimer = new Timer(() => {

                if (!_canBeAttacked) {
                    _canBeAttacked = true;
                }
            }, 1f, repeats: true);

            _staminaReplenishmentTimer = new Timer(() => {

                if (this.Stamina < this.MaximumStamina) {
                    this.Stamina += 1;
                }

            }, 0.75f, repeats: true);

        }

        public virtual void Load(IAssetService assetService) {

            if (DamagedSoundPool == null) {
                DamagedSoundPool = new List<SoundEffect>();
                DamagedSoundPool.Add(assetService.GameService.ContentManager.Load<SoundEffect>(@"SFX/Hit1"));
                DamagedSoundPool.Add(assetService.GameService.ContentManager.Load<SoundEffect>(@"SFX/Hit2"));
                DamagedSoundPool.Add(assetService.GameService.ContentManager.Load<SoundEffect>(@"SFX/Hit3"));
            }
        }

        public virtual void Update(GameTime gameTime) {

            if (this.IsAlive) {
                this.CurrentAnimation.Update(gameTime);

                this._flashTimer.Update(gameTime);

                if (!this._canBeAttacked) {
                    this._invulnerabilityTimer.Update(gameTime);
                }

                if (CurrentBehavior != null) {
                    CurrentBehavior.Update(gameTime);
                }

                this._staminaReplenishmentTimer.Update(gameTime);
            }



            if (this.Effect != null) {
                this.Effect.Update(gameTime);

                if (!this.Effect.IsActive) {
                    this.Effect = null;
                }
            }

            if (ResetAttack) {
                ResetAttack = false;
                SetState(EntityState.Idle);

            }

        }

        public virtual void Draw(GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch) {

            if (this.IsAlive) {
                CurrentAnimation.SetPosition(this.Position);

                CurrentAnimation.SetColor(_renderColor);

                CurrentAnimation.Draw(gameTime, spriteBatch);
            }

        }

        public void Move(Vector2 amount) {

            amount.X = MathHelper.Clamp(amount.X, -1, 1);
            amount.Y = MathHelper.Clamp(amount.Y, -1, 1);

            this.MovementDelta = amount;

        }

        public void ResetMovement() {

            this.MovementDelta = Vector2.Zero;

        }

        public virtual void Collide(IMoveableEntity collider) {

        }

        public virtual void Damage(ILivingEntity attacker) {

            if (_canBeAttacked) {
                _canBeAttacked = false;
                _invulnerabilityTimer.Reset();

                if (attacker is ICombattant) {
                    ICombattant combattant = (attacker as ICombattant);
                    int changeValue = (combattant.AttackPower - this.Defense);
                    if (changeValue < 0) changeValue = 0;

                    Health -= changeValue;
                    this.MovementDelta = Vector2.Zero;
                    this.Velocity = Vector2.Zero;
                } else {
                    Health -= 1;
                    this.MovementDelta = Vector2.Zero;
                    this.Velocity = Vector2.Zero;
                }

                DamagedSound.Play(Music.Volume, 0f, 0f);

                if (Health <= 0) {
                    Kill(attacker);
                }
            }
        }

        public void Kill(ILivingEntity attacker) {

            Health = 0;

            IsAlive = false;

        }

        public void Spawn() {

            Health = MaximumHealth;

            Position = SpawnPosition;

            IsAlive = true;
        }

        public void Heal(int amount) {

            Health = (int)MathHelper.Clamp(Health + amount, 0, MaximumHealth);
        }

        public void Rest(int amount) {

            Stamina = (int)MathHelper.Clamp(Stamina + amount, 0, MaximumStamina);

        }

        public void Attack(ILivingEntity target) {

            target.Damage(this);

            Stamina -= 1;

            this.State = EntityState.Attacking;

        }


        public void SetHeading(EntityHeading heading) {

            this.Heading = heading;

            this.CurrentAnimation.Restart();

        }

        public void SetState(EntityState state) {

            this.State = state;

            if (this.State == EntityState.Attacking) {
                CurrentAnimation.Restart();
            }

        }

        public void RegisterAnimation(EntityHeading heading, EntityState state, Animation animation) {

            Animations[Tuple.Create<EntityState, EntityHeading>(state, heading)] = animation;

        }

        public void SetPosition(Vector2 value) {
            
            this.Position = value;

        }

        public void SetSpawnPosition(Vector2 value) {

            this.SpawnPosition = value;

        }

        public void SetVelocity(Vector2 value) {

            this.Velocity = value;

        }

        public void SetBehavior(IAIBehavior behavior) {

            this.CurrentBehavior = behavior;

        }

        public void UseStamina() {

            this.Stamina -= 1;

            if (this.Stamina < 0) {
                this.Stamina = 0;
            }
        }

        public void IncreaseDefense() {

            Defense += 1;

        }

        public void IncreaseAttack() {

            AttackPower += 1;

        }

        public void SetEffect(Effects.Effect effect) {

            this.Effect = effect;

        }
    }

}
