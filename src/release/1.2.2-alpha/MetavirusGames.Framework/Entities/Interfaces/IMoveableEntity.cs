﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Entities.Interfaces {
    public interface IMoveableEntity {

        Vector2 Position { get; }

        Vector2 Velocity { get; }

        Vector2 MovementDelta { get; set; }

        float MovementSpeed { get; set; }

        Rectangle Bounds { get; }

        void SetPosition(Vector2 value);

        void SetVelocity(Vector2 value);

        void Collide(IMoveableEntity collider);
    }
}
