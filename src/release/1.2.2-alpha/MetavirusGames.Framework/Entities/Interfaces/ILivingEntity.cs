﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Entities.Interfaces {
    public interface ILivingEntity {

        bool IsAlive { get; }

        int Health { get; }

        int MaximumHealth { get; }

        Vector2 SpawnPosition { get; }

        void Damage(ILivingEntity attacker);

        void Kill(ILivingEntity attacker);

        void Heal(int amount);

        void Spawn();
    }
}
