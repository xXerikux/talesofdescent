﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Entities.Effects {
    public sealed class PoisonEffect : Effect {

        public PoisonEffect(int howLong, Character character)
            : base(1f, howLong, () => {
                character.Damage(null);
            }) {

                FlashColor = Color.Green;

        }
    }
}
