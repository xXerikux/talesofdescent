﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Entities.Effects {
    public interface IEffect {

        float Interval { get; }

        int DurationInSeconds { get; }

        Action Action { get; }

    }
}
