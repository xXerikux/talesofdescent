﻿using MetavirusGames.Framework.Runtime;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.Framework.Entities.Effects {

    public abstract class Effect : IEffect {

        public float Interval { get; protected set; }

        public int DurationInSeconds { get; protected set; }

        public Action Action { get; protected set; }

        public bool IsActive { get; protected set; }

        public Color FlashColor { get; protected set; }

        private Timer _timer;

        public Effect(float interval, int durationInSeconds, Action action) {

            IsActive = true;

            this.Interval = interval;

            this.DurationInSeconds = durationInSeconds;

            this.Action = action;

            _timer = new Timer(this.Action, interval, durationInSeconds, true);

        }

        public void Update(GameTime gameTime) {

            _timer.Update(gameTime);

            if (!_timer.IsActive) {
                this.IsActive = false;
            }

        }

    }
}
