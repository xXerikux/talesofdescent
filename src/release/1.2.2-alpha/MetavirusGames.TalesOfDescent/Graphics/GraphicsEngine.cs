﻿using MetavirusGames.Framework;
using MetavirusGames.Framework.Graphics;
using MetavirusGames.Framework.Graphics.Interfaces;
using MetavirusGames.Framework.Services;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Graphics {
    public sealed class GraphicsEngine : IGraphicsEngine {

        internal SpriteBatch SpriteBatch;

        internal GraphicsEngineState State;

        public ICamera Camera { get; private set; }

        public Effect LightingEffect { get; private set; }

        private IAssetService _assetService;

        private RenderTarget2D _worldTarget;

        private RenderTarget2D _lightMap;

        private Texture2D _lightTexture;

        private Light _light;

        private Light _outerLight;

        private Color _ambientColor;

        private IList<Light> _lights;

        public GraphicsEngine(SpriteBatch spriteBatch, ICamera camera, IAssetService assetService) {

            this.SpriteBatch = spriteBatch;

            this.State = GraphicsEngineState.Awaiting;

            this.Camera = camera;

            this._assetService = assetService;

            this.LightingEffect = assetService.GameService.ContentManager.Load<Effect>(@"Effects/LightMapEffect");

            //TODO: Clean this up with local variables, breaking all kinds of good design
            this.LightingEffect.Parameters["CanvasSize"].SetValue(new Vector2(800, 600));

            Matrix projection = Matrix.CreateOrthographicOffCenter(0, 800, 600, 0, 0, 1);

            Matrix halfPixelOffset = Matrix.CreateTranslation(-0.5f, -0.5f, 0);

            this.LightingEffect.Parameters["World"].SetValue(Matrix.Identity);

            this.LightingEffect.Parameters["View"].SetValue(Matrix.Identity);

            this.LightingEffect.Parameters["Projection"].SetValue(halfPixelOffset * projection);

            this._ambientColor = new Color(0.015f, 0.015f, 0.015f, 0.015f);

            this._worldTarget = new RenderTarget2D(assetService.GameService.Graphics.GraphicsDevice, 800, 600);

            this._lightMap = new RenderTarget2D(assetService.GameService.Graphics.GraphicsDevice, 800, 600);

            this._lightTexture = assetService.LoadTexture(@"Content/Textures/effect_lightmap.png");

            this._lights = new List<Light>();

            this._light = new Light(this._lightTexture, 400, 300, this._lightTexture.Width / 2, this._lightTexture.Height / 2, 0f, 2f, new Color(242, 228, 157, 150));
            this._outerLight = new Light(this._lightTexture, 400, 300, this._lightTexture.Width / 2, this._lightTexture.Height / 2, 0f, 5f, new Color(242, 228, 157, 150));
        }

        public void BeginDraw() {

            if (this.State == GraphicsEngineState.Awaiting) {
                this.State = GraphicsEngineState.Begin;

                this.SpriteBatch.GraphicsDevice.SetRenderTarget(null);

                this.SpriteBatch.GraphicsDevice.SetRenderTarget(this._worldTarget);

                this.SpriteBatch.GraphicsDevice.Clear(Color.Black);

                this.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, DepthStencilState.Default, RasterizerState.CullNone, null, this.Camera.GetMatrix());
            } else {
                Error.InvalidGraphicsEngineState("EndDraw() must be called before BeginDraw() can be called");
            }

        }

        public void BeginDrawUI() {

            if (this.State == GraphicsEngineState.Awaiting) {
                this.State = GraphicsEngineState.Begin;

                //this.SpriteBatch.GraphicsDevice.Clear(Color.Black);

                this.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
            } else {
                Error.InvalidGraphicsEngineState("EndDraw() must be called before BeginDraw() can be called");
            }

        }

        public void Draw(GameTime gameTime, IRenderable renderableObject) {

            if (this.State == GraphicsEngineState.Begin || this.State == GraphicsEngineState.Drawing) {
                this.State = GraphicsEngineState.Drawing;

                if (renderableObject != null) {
                    renderableObject.Draw(gameTime, this.SpriteBatch);
                } else {
                    Error.InvalidOperation("Cannot pass a null object to the renderer!");
                }

                

            } else {
                Error.InvalidGraphicsEngineState("BeginDraw() must be called before Draw(IRenderable) can be called");
            }
        }

        public void DrawUI(GameTime gameTime, IRenderable renderableObject) {

            if (this.State == GraphicsEngineState.Begin || this.State == GraphicsEngineState.Drawing) {
                this.State = GraphicsEngineState.Drawing;

                if (renderableObject != null) {
                    renderableObject.Draw(gameTime, this.SpriteBatch);
                } else {
                    Error.InvalidOperation("Cannot pass a null object to the renderer!");
                }

            } else {
                Error.InvalidGraphicsEngineState("BeginDraw() must be called before Draw(IRenderable) can be called");
            }
        }

        public void EndDraw() {

            if (this.State == GraphicsEngineState.Drawing || this.State == GraphicsEngineState.Begin) {
                this.State = GraphicsEngineState.End;

                this.SpriteBatch.End();

                this.SpriteBatch.GraphicsDevice.SetRenderTarget(null);

                this.SpriteBatch.GraphicsDevice.SetRenderTarget(this._lightMap);

                this.SpriteBatch.GraphicsDevice.Clear(this._ambientColor);

                this.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Additive);

                this.SpriteBatch.Draw(_light.Texture, _light.Position, null, _light.Tint, _light.Rotation, _light.Origin, _light.Scale, SpriteEffects.None, 0);

                this.SpriteBatch.Draw(this._outerLight.Texture, this._outerLight.Position, null, this._outerLight.Tint, this._outerLight.Rotation, this._outerLight.Origin, this._outerLight.Scale, SpriteEffects.None, 0);

                this.SpriteBatch.End();

                this.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Additive, null, null, null, null, this.Camera.GetMatrix());

                foreach (Light light in _lights) {
                    this.SpriteBatch.Draw(light.Texture, light.Position, null, light.Tint, light.Rotation, light.Origin, light.Scale, SpriteEffects.None, 0);
                }

                this.SpriteBatch.End();

                this.SpriteBatch.GraphicsDevice.SetRenderTarget(null);

                this.LightingEffect.Parameters["LightMap"].SetValue(_lightMap);

                this.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Opaque, SamplerState.PointClamp, DepthStencilState.Default, RasterizerState.CullNone, this.LightingEffect);

                this.SpriteBatch.Draw(_worldTarget, Vector2.Zero, Color.White);

                this.SpriteBatch.End();
            } else {
                Error.InvalidGraphicsEngineState("BeginDraw() must be called before EndDraw() can be called");
            }

            this.State = GraphicsEngineState.Awaiting;
        }

        public void EndDrawUI() {

            if (this.State == GraphicsEngineState.Drawing || this.State == GraphicsEngineState.Begin) {
                this.State = GraphicsEngineState.End;

                this.SpriteBatch.End();

            } else {
                Error.InvalidGraphicsEngineState("BeginDraw() must be called before EndDraw() can be called");
            }

            this.State = GraphicsEngineState.Awaiting;
        }

        public void AddLight(Light light) {

            this._lights.Add(light);
        }

        public void ClearLights() {

            this._lights.Clear();

        }
    }
}
