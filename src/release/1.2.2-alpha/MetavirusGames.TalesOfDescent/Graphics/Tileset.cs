﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Graphics {
    public class Tileset {

        public int TileWidth { get; private set; }

        public int TileHeight { get; private set; }

        public Texture2D Texture { get; private set; }

        public Rectangle[] Tiles { get; private set; }

        public Tileset(Texture2D texture, int tileWidth, int tileHeight) {

            this.Texture = texture;

            this.TileHeight = tileHeight;

            this.TileWidth = tileWidth;

            InitializeTileset();

        }

        private void InitializeTileset() {

            Tiles = new Rectangle[this.Texture.Width / TileWidth * this.Texture.Height / TileHeight];

            int i = 0;

            for (int y = 0; y < this.Texture.Height / TileHeight; y++) {
                for (int x = 0; x < this.Texture.Width / TileWidth; x++) {
                    Tiles[i] = new Rectangle(x * TileWidth, y * TileHeight, TileWidth, TileHeight);
                    i++;
                }
            }

        }

    }
}
