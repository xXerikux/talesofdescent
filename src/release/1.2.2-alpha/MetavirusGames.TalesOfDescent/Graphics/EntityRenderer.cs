﻿using MetavirusGames.Framework.Entities;
using MetavirusGames.Framework.Graphics.Interfaces;
using MetavirusGames.TalesOfDescent.Entities;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Graphics {
    public class EntityRenderer : IRenderable {

        public IList<Character> Characters { get; private set; }

        public EntityRenderer(IList<Character> characters) {

            this.Characters = characters;

        }

        public void Draw(Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch) {

            Characters = Characters.OrderBy(c => c.Center.Y).ToList<Character>();

            Player player = (Player)Characters.FirstOrDefault(c => c is Player);

            foreach (Character character in this.Characters) {

                float distance = Vector2.Distance(player.Position, character.Position);

                if (distance < 300) {
                    character.Draw(gameTime, spriteBatch);
                }
            }

        }

    }
}
