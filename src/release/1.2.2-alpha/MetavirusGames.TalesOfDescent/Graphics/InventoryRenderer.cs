﻿using MetavirusGames.Framework.Graphics;
using MetavirusGames.Framework.Graphics.Interfaces;
using MetavirusGames.TalesOfDescent.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Graphics {
    public sealed class InventoryRenderer : IRenderable {

        private Texture2D _texture;

        private SpriteFont _font;

        private Inventory _inventory;

        public InventoryRenderer(Texture2D texture, SpriteFont font, Inventory inventory) {

            _texture = texture;

            _font = font;

            _inventory = inventory;

        }

        public void Draw(Microsoft.Xna.Framework.GameTime gameTime, SpriteBatch spriteBatch) {

            spriteBatch.Draw(_texture, new Vector2(275, 10), new Rectangle(0, 0, 32, 32), Color.White);

            FontHelper.DrawString(spriteBatch, _font, new Vector2(295, 30), _inventory.BoneEssences.ToString(), Color.White, Color.Black, true);


            spriteBatch.Draw(_texture, new Vector2(310, 10), new Rectangle(32, 0, 32, 32), Color.White);

            FontHelper.DrawString(spriteBatch, _font, new Vector2(330, 30), _inventory.FerocityEssences.ToString(), Color.White, Color.Black, true);


            spriteBatch.Draw(_texture, new Vector2(345, 10), new Rectangle(64, 0, 32, 32), Color.White);

            FontHelper.DrawString(spriteBatch, _font, new Vector2(365, 30), _inventory.LifeEssences.ToString(), Color.White, Color.Black, true);


            spriteBatch.Draw(_texture, new Vector2(380, 10), new Rectangle(96, 0, 32, 32), Color.White);

            FontHelper.DrawString(spriteBatch, _font, new Vector2(400, 30), _inventory.EnduranceEssences.ToString(), Color.White, Color.Black, true);


            spriteBatch.Draw(_texture, new Vector2(415, 10), new Rectangle(128, 0, 32, 32), Color.White);

            FontHelper.DrawString(spriteBatch, _font, new Vector2(435, 30), _inventory.MagicEssences.ToString(), Color.White, Color.Black, true);

            FontHelper.DrawString(spriteBatch, _font, new Vector2(45, spriteBatch.GraphicsDevice.Viewport.Height - 42), _inventory.HealthPotions.ToString(), Color.White, Color.Black, true);

            spriteBatch.Draw(_texture, new Vector2(10, spriteBatch.GraphicsDevice.Viewport.Height - 42), new Rectangle(0, 64, 32, 32), Color.White);


            FontHelper.DrawString(spriteBatch, _font, new Vector2(95, spriteBatch.GraphicsDevice.Viewport.Height - 42), _inventory.StaminaPotions.ToString(), Color.White, Color.Black, true);

            spriteBatch.Draw(_texture, new Vector2(60, spriteBatch.GraphicsDevice.Viewport.Height - 42), new Rectangle(32, 64, 32, 32), Color.White);
        }

        public void SetInventory(Inventory inventory) {

            this._inventory = inventory;

        }
    }
}
