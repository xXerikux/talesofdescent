﻿using MetavirusGames.Framework.Entities;
using MetavirusGames.Framework.Graphics;
using MetavirusGames.Framework.Graphics.Interfaces;
using MetavirusGames.TalesOfDescent.Dungeon;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Graphics {
    public sealed class MapRenderer : IRenderable {

        private Map _map;

        private Tileset _tileset;

        private Character _character;

        private IGraphicsEngine _graphicsEngine;

        private List<int> _randomBlocks = new List<int>() { 37, 38, 39 };

        private Random _random = new Random(new Random().Next(0, 100000));

        private SpriteFont _font;

        public MapRenderer(IGraphicsEngine graphicsEngine, Map map, Tileset tileset, Character character, SpriteFont font) {

            this._graphicsEngine = graphicsEngine;

            this._map = map;

            this._tileset = tileset;

            this._character = character;

            this._font = font;

        }

        public void Draw(Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch) {

            Vector2 playerRoomPosition = this._map.GetRoomPositionOfEntity(this._tileset, this._character);

            //Iterate each room on the map
            for (int mapY = 0; mapY < this._map.Height; mapY++) {
                for (int mapX = 0; mapX < this._map.Width; mapX++) {

                    Color renderColor = Color.White;

                    //if (mapX == this._map.SpawnRoomPosition.X && mapY == this._map.SpawnRoomPosition.Y) {
                    //    renderColor = ColorHelper.CreateColor(Color.LightBlue, 50f);
                    //}
                    //if (mapX == this._map.ExitRoomPosition.X && mapY == this._map.ExitRoomPosition.Y) {
                    //    renderColor = ColorHelper.CreateColor(Color.Red, 50f);
                    //}
                    Rectangle roomBounds = new Rectangle
                                (
                                mapX * Room.MaxWidth * this._tileset.TileWidth,
                                mapY * Room.MaxHeight * this._tileset.TileHeight,
                                Room.MaxWidth * this._tileset.TileWidth,
                                Room.MaxHeight * this._tileset.TileHeight
                                );

                    bool playerInRoom = roomBounds.Intersects(_character.Bounds);
                    float distance = Vector2.Distance(playerRoomPosition, new Vector2(mapX, mapY));

                    if (distance < 2) {
                        //Draw each room
                        for (int roomY = 0; roomY < Room.MaxHeight; roomY++) {
                            for (int roomX = 0; roomX < Room.MaxWidth; roomX++) {



                                //TODO: Decide if a rectangle x rectangle intersection test would provide a smoother transition
                                if (this._map.Rooms[mapX, mapY] != null) {
                                    int tileValue = this._map.Rooms[mapX, mapY].Data[roomX, roomY];

                                    if (tileValue == 0) {
                                        spriteBatch.Draw(this._tileset.Texture, new Vector2(
                                        (mapX * Room.MaxWidth * this._tileset.TileWidth) + (roomX * this._tileset.TileWidth),
                                        (mapY * Room.MaxHeight * this._tileset.TileHeight) + (roomY * this._tileset.TileHeight)), this._tileset.Tiles[6], renderColor);


                                    } else if (tileValue == 1 || tileValue == 6) {
                                        spriteBatch.Draw(this._tileset.Texture, new Vector2(
                                        (mapX * Room.MaxWidth * this._tileset.TileWidth) + (roomX * this._tileset.TileWidth),
                                        (mapY * Room.MaxHeight * this._tileset.TileHeight) + (roomY * this._tileset.TileHeight)), this._tileset.Tiles[7], renderColor);
                                    } else if (tileValue == 2) {
                                        int block = _randomBlocks[_random.Next(0, _randomBlocks.Count - 1)];

                                        spriteBatch.Draw(this._tileset.Texture, new Vector2(
                                        (mapX * Room.MaxWidth * this._tileset.TileWidth) + (roomX * this._tileset.TileWidth),
                                        (mapY * Room.MaxHeight * this._tileset.TileHeight) + (roomY * this._tileset.TileHeight)), this._tileset.Tiles[7], renderColor);

                                        spriteBatch.Draw(this._tileset.Texture, new Vector2(
                                        (mapX * Room.MaxWidth * this._tileset.TileWidth) + (roomX * this._tileset.TileWidth),
                                        (mapY * Room.MaxHeight * this._tileset.TileHeight) + (roomY * this._tileset.TileHeight)), this._tileset.Tiles[39], renderColor);
                                    } else if (tileValue == 3) {
                                        spriteBatch.Draw(this._tileset.Texture, new Vector2(
                                        (mapX * Room.MaxWidth * this._tileset.TileWidth) + (roomX * this._tileset.TileWidth),
                                        (mapY * Room.MaxHeight * this._tileset.TileHeight) + (roomY * this._tileset.TileHeight)), this._tileset.Tiles[7], renderColor);

                                        spriteBatch.Draw(this._tileset.Texture, new Vector2(
                                        (mapX * Room.MaxWidth * this._tileset.TileWidth) + (roomX * this._tileset.TileWidth),
                                        (mapY * Room.MaxHeight * this._tileset.TileHeight) + (roomY * this._tileset.TileHeight)), this._tileset.Tiles[8], renderColor);
                                    } else if (tileValue == 4) {
                                        spriteBatch.Draw(this._tileset.Texture, new Vector2(
                                        (mapX * Room.MaxWidth * this._tileset.TileWidth) + (roomX * this._tileset.TileWidth),
                                        (mapY * Room.MaxHeight * this._tileset.TileHeight) + (roomY * this._tileset.TileHeight)), this._tileset.Tiles[12], renderColor);
                                    } else if (tileValue == 5) {
                                        spriteBatch.Draw(this._tileset.Texture, new Vector2(
                                        (mapX * Room.MaxWidth * this._tileset.TileWidth) + (roomX * this._tileset.TileWidth),
                                        (mapY * Room.MaxHeight * this._tileset.TileHeight) + (roomY * this._tileset.TileHeight)), this._tileset.Tiles[13], renderColor);
                                    } else if (tileValue == 7) {
                                        spriteBatch.Draw(this._tileset.Texture, new Vector2(
                                        (mapX * Room.MaxWidth * this._tileset.TileWidth) + (roomX * this._tileset.TileWidth),
                                        (mapY * Room.MaxHeight * this._tileset.TileHeight) + (roomY * this._tileset.TileHeight)), this._tileset.Tiles[71], renderColor);
                                    }

                                    if (_character.StuckInCurrentRoom) {

                                        Vector2 roomPos = this._map.GetRoomPositionOfEntity(this._tileset, _character);

                                        if ((tileValue == 1 || tileValue == 3 || tileValue == 4) && mapX == roomPos.X && mapY == roomPos.Y) {
                                            if (roomX == 9 && roomY == 0) {
                                                spriteBatch.Draw(this._tileset.Texture, new Vector2(
                                            (mapX * Room.MaxWidth * this._tileset.TileWidth) + (roomX * this._tileset.TileWidth),
                                            (mapY * Room.MaxHeight * this._tileset.TileHeight) + (roomY * this._tileset.TileHeight)), this._tileset.Tiles[71], renderColor);
                                            } else if (roomX == 0 && roomY == 8) {
                                                spriteBatch.Draw(this._tileset.Texture, new Vector2(
                                            (mapX * Room.MaxWidth * this._tileset.TileWidth) + (roomX * this._tileset.TileWidth),
                                            (mapY * Room.MaxHeight * this._tileset.TileHeight) + (roomY * this._tileset.TileHeight)), this._tileset.Tiles[71], renderColor);
                                            } else if (roomX == 9 && roomY == 16) {
                                                spriteBatch.Draw(this._tileset.Texture, new Vector2(
                                            (mapX * Room.MaxWidth * this._tileset.TileWidth) + (roomX * this._tileset.TileWidth),
                                            (mapY * Room.MaxHeight * this._tileset.TileHeight) + (roomY * this._tileset.TileHeight)), this._tileset.Tiles[71], renderColor);
                                            } else if (roomX == 18 && roomY == 8) {
                                                spriteBatch.Draw(this._tileset.Texture, new Vector2(
                                            (mapX * Room.MaxWidth * this._tileset.TileWidth) + (roomX * this._tileset.TileWidth),
                                            (mapY * Room.MaxHeight * this._tileset.TileHeight) + (roomY * this._tileset.TileHeight)), this._tileset.Tiles[71], renderColor);
                                            }
                                        }
                                    }

                                } //End for x
                            }
                        } //End for y

                    } else { //End distance check
                        //Debug.WriteLine(String.Format("Not drawing room {0}, {1} player at position {2}, {3}", mapX, mapY, playerRoomPosition.X, playerRoomPosition.Y));
                    }

                    foreach (Item item in this._map.Items) {

                        item.Draw(gameTime, spriteBatch);

                        if (item.IsForSale) {

                            

                            Vector2 stringSize = _font.MeasureString(item.Cost.ToString());

                            Vector2 stringPosition = new Vector2(item.Bounds.Center.X - stringSize.X / 2, item.Bounds.Center.Y + 8);
                            spriteBatch.Draw(item.Texture, stringPosition - new Vector2(item.CurrencyTextureCoordinates.Width * 0.8f, 5), item.CurrencyTextureCoordinates, Color.White);
                            FontHelper.DrawString(spriteBatch, _font, stringPosition, item.Cost.ToString(), Color.White, Color.Black, true);

                        }
                    }
                }
            }

        }

        private int GetTileForPosition(int x, int y) {

            //Top Left
            if (x == 0 && y == 0) {
                return 32;
            } else  if (x == 0 && y == 8){
                return 73;
            //Left Middle
            } else if (x == 0 && y < Room.MaxHeight - 1) {
                return 64;
            //Right Middle
            } else if (x == Room.MaxWidth - 1 && y < Room.MaxHeight - 1 && y > 0) {
                return 66;
            //Bottom Middle
            } else if (x > 0 && x < Room.MaxWidth - 1 && y == Room.MaxHeight - 1) {
                return 97;
            //Top Middle
            } else if (x > 0 && x < Room.MaxWidth - 1 && y == 0) {
                return 33;
            //Top Right
            } else if (x == Room.MaxWidth - 1 && y == 0) {
                return 34;
            //Bottom Left
            } else if (x == 0 && y == Room.MaxHeight - 1) {
                return 96;
            //Top Right
            } else if (x == Room.MaxWidth - 1 && y == Room.MaxHeight - 1) {
                return 98;
            }

            return 0;
        }
    }
}
