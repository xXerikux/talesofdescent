﻿using MetavirusGames.Framework.Graphics.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Graphics {
    public sealed class MinimapRenderer : IRenderable {

        private Texture2D _roomTexture;

        private IList<Vector2> _exploredRooms;

        public MinimapRenderer(IList<Vector2> exploredRooms, Texture2D roomTexture){

            this._exploredRooms = exploredRooms;

            this._roomTexture = roomTexture;

        }

        public void Draw(Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch) {

            foreach (Vector2 position in this._exploredRooms) {
                Vector2 currentPosition = this._map.GetRoomPositionOfEntity(this._tileset, this._character);

                Color renderColor = currentPosition == position ? Color.Yellow : Color.White;

                if (position == new Vector2(this._map.SpawnRoomPosition.X, this._map.SpawnRoomPosition.Y)) {
                    renderColor = Color.LightBlue;
                }

                if (position == new Vector2(this._map.ExitRoomPosition.X, this._map.ExitRoomPosition.Y)) {
                    renderColor = Color.Red;
                }

                spriteBatch.Draw(this._roomTexture, new Vector2(5, 5) + new Vector2(position.X * 18, position.Y * 18), renderColor);
            }

        }

    }
}
