#region File Description
//-----------------------------------------------------------------------------
// GameplayScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using MetavirusGames.Framework.Audio;
using MetavirusGames.Framework.Data;
using MetavirusGames.Framework.Entities;
using MetavirusGames.Framework.Entities.Effects;
using MetavirusGames.Framework.Entities.Interfaces;
using MetavirusGames.Framework.Graphics;
using MetavirusGames.Framework.Graphics.Interfaces;
using MetavirusGames.Framework.ScreenManager;
using MetavirusGames.Framework.Services;
using MetavirusGames.Framework.UI;
using MetavirusGames.TalesOfDescent.Dungeon;
using MetavirusGames.TalesOfDescent.Dungeon.Interfaces;
using MetavirusGames.TalesOfDescent.Entities;
using MetavirusGames.TalesOfDescent.Graphics;
using MetavirusGames.TalesOfDescent.Services;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
#endregion

namespace MetavirusGames.TalesOfDescent.Screens {
    /// <summary>
    /// This screen implements the actual game logic. It is just a
    /// placeholder to get the idea across: you'll probably want to
    /// put some more interesting gameplay in here!
    /// </summary>
    class GameplayScreen : GameScreen {
        #region Fields

        private SpriteBatch spriteBatch;

        private Map map;

        private Texture2D _overlayTexture;

        private KeyboardState previousKeyboard;

        private Tileset tileset;

        private SoundEffect _respawnSound;

        public MetavirusGames.Framework.Settings Settings { get; private set; }

        internal IGraphicsEngine GraphicsEngine;

        internal IAssetService AssetsService;

        internal RoomService RoomService;

        internal IMapGenerator MapGenerator;

        internal MapRenderer MapRenderer;

        internal Player Player = null;

        internal SkeletonBoss SkeletonBoss = null;

        public static IList<Character> Characters = null;

        internal CharacterControllerService CharacterControllerService { get; private set; }

        internal PhysicsService PhysicsService { get; private set; }

        internal EntityRenderer EntityRenderer { get; private set; }

        internal StatsRenderer StatsRenderer { get; private set; }

        internal IList<Animation> AnimationQueue { get; private set; }

        internal Meter PlayerHealthMeter { get; private set; }

        internal Meter PlayerStaminaMeter { get; private set; }

        internal Meter BossHealthMeter { get; private set; }

        internal Texture2D ItemsTexture { get; private set; }

        internal PopupManager PopupManager { get; private set; }

        internal InventoryRenderer InventoryRenderer { get; private set; }

        internal SoundEffect PickupSound { get; private set; }

        internal static Inventory PlayerInventory;

        public bool IsPaused { get; private set; }

        ContentManager content;

        SpriteFont gameFont;

        Random random = new Random();

        float pauseAlpha;

        #endregion

        #region Initialization


        /// <summary>
        /// Constructor.
        /// </summary>
        public GameplayScreen() {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
        }

        public override void Initialize() {

            ICamera camera = new BasicCamera();

            if (PlayerInventory == null) {
                PlayerInventory = new Inventory();
            }

            this.AssetsService = new AssetService((ScreenManager.Game as IGameService));

            PopupManager.Initialize(this.AssetsService);

            this.spriteBatch = new SpriteBatch(ScreenManager.Game.GraphicsDevice);

            this.GraphicsEngine = new GraphicsEngine(spriteBatch, camera, this.AssetsService);

            this.MapGenerator = new ShopMapGenerator();//new BasicMapGenerator(new RoomService(this.AssetsService));

            map = this.MapGenerator.Generate(Difficulty.Easy, (ScreenManager.Game as IGameService));

            this.GraphicsEngine.ClearLights();

            foreach (Light light in this.map.LightPositions) {
                this.GraphicsEngine.AddLight(light);
            }

            this.PhysicsService = new PhysicsService();

            this.tileset = new Tileset(this.AssetsService.LoadTexture(@"Content/Textures/Tileset.png"), 16, 16);

            this.ItemsTexture = this.AssetsService.LoadTexture(@"Content/Textures/Items.png");

            this._respawnSound = this.ScreenManager.Game.Content.Load<SoundEffect>(@"SFX/Respawn");

            this.PickupSound = this.ScreenManager.Game.Content.Load<SoundEffect>(@"SFX/coin");

            GraphicsEngine.Camera.SetZoom(2f);

            Characters = new List<Character>();

            Player = new Player("Will", new Vector2
                (
                map.SpawnRoomPosition.X * (float)Room.MaxWidth * tileset.TileWidth + ((float)(Room.MaxWidth * tileset.TileWidth) / 2) - (tileset.TileWidth / 2),
                map.SpawnRoomPosition.Y * (float)Room.MaxHeight * tileset.TileHeight + ((float)(Room.MaxHeight * tileset.TileHeight) / 2) - (tileset.TileHeight / 2))
                );

            if (this.MapGenerator is ShopMapGenerator) {
                Player.SetPosition(Player.Position + new Vector2(0, 32));
            }

            Characters.Add(Player);

            if (!(this.MapGenerator is ShopMapGenerator)) {
                SkeletonBoss = new SkeletonBoss(new Vector2
                    (
                    map.ExitRoomPosition.X * (float)Room.MaxWidth * tileset.TileWidth + ((float)(Room.MaxWidth * tileset.TileWidth) / 2) - (tileset.TileWidth / 2),
                    map.ExitRoomPosition.Y * (float)Room.MaxHeight * tileset.TileHeight + ((float)(Room.MaxHeight * tileset.TileHeight) / 2) - (tileset.TileHeight / 2))
                    );

                Characters.Add(SkeletonBoss);

                this.map.Boss = SkeletonBoss;

                for (int y = 0; y < map.Height; y++) {
                    for (int x = 0; x < map.Width; x++) {

                        Room room = null;

                        room = this.map.Rooms[x, y];

                        if ((this.map.SpawnRoomPosition != new Point(x, y) && this.map.ExitRoomPosition != new Point(x, y))) {
                            if (room != null) {
                                int totalMobs = new Random(new Random().Next(0, 100000)).Next(1, 3);
                                for (int i = 0; i < totalMobs; i++) {
                                    var skeleton = new Skeleton(new Vector2
                                    (
                                    x * (float)Room.MaxWidth * tileset.TileWidth + ((float)(Room.MaxWidth * tileset.TileWidth) / 2) - (tileset.TileWidth / 2),
                                    y * (float)Room.MaxHeight * tileset.TileHeight + ((float)(Room.MaxHeight * tileset.TileHeight) / 2) - (tileset.TileHeight / 2))
                                    );

                                    Characters.Add(skeleton);
                                }
                            }
                        }
                    }
                }
            } else {

                Vector2 getSpawnPosition = this.map.Rooms[0, 0].GetRandomOpenPosition();

                var shopKeeper = new ShopKeeper(new Vector2
                                    (
                                    0 * (float)Room.MaxWidth * tileset.TileWidth + ((float)(Room.MaxWidth * tileset.TileWidth) / 2) - (tileset.TileWidth / 2),
                                    0 * (float)Room.MaxHeight * tileset.TileHeight + ((float)(Room.MaxHeight * tileset.TileHeight) / 2) - (tileset.TileHeight / 2))
                                    );

                Characters.Add(shopKeeper);

                this.map.AddItem(new BoneCharm(this.ItemsTexture, new Vector2((0 * (float)Room.MaxWidth) + Room.MaxWidth / 2, (0 * (float)Room.MaxHeight) + Room.MaxHeight / 4) * this.tileset.TileWidth, false) { IsForSale = true, Cost = 50, CurrencyTextureCoordinates = new Rectangle(0, 0, 16, 16) });

                this.map.AddItem(new HealthPotion(this.ItemsTexture, (new Vector2((0 * (float)Room.MaxWidth) + Room.MaxWidth / 2, (0 * (float)Room.MaxHeight) + Room.MaxHeight / 4) * this.tileset.TileWidth) + new Vector2(35, 0), false) { IsForSale = true, Cost = 15, CurrencyTextureCoordinates = new Rectangle(32, 0, 16, 16) });

                this.map.AddItem(new FerocityCharm(this.ItemsTexture, (new Vector2((0 * (float)Room.MaxWidth) + Room.MaxWidth / 2, (0 * (float)Room.MaxHeight) + Room.MaxHeight / 4) * this.tileset.TileWidth) - new Vector2(35, 0), false) { IsForSale = true, Cost = 50, CurrencyTextureCoordinates = new Rectangle(16, 0, 16, 16) });

            }


            this._overlayTexture = this.AssetsService.LoadTexture(@"Content/Textures/RoomTexture.png");

            MapRenderer = new MapRenderer(this.GraphicsEngine, map, tileset, Player, this.ScreenManager.Game.Content.Load<SpriteFont>(@"Fonts/04b03_06"));

            this.EntityRenderer = new EntityRenderer(Characters);

            this.StatsRenderer = new StatsRenderer(Player, this.ScreenManager.Game.Content.Load<SpriteFont>(@"Fonts/04b03_16"));

            this.CharacterControllerService = new CharacterControllerService(Player, (ScreenManager.Game as TalesOfDescentGame));

            this.PopupManager = new PopupManager();

            this.AnimationQueue = new List<Animation>();

            this.PlayerHealthMeter = new Meter(this.AssetsService.LoadTexture(@"Content/Textures/HealthMeter.png"), this.Player.Health, this.Player.MaximumHealth,
                new Rectangle(190, 22, 197, 35), new Rectangle(190, 70, 195, 26), new Rectangle(190, 118, 195, 28), new Vector2(10, 10),
                this.ScreenManager.Game.Content.Load<SpriteFont>(@"Fonts/04b03_16"));

            this.PlayerHealthMeter.SetText(String.Format("HP {0}/{1}", this.Player.Health, this.Player.MaximumHealth));



            this.PlayerStaminaMeter = new Meter(this.AssetsService.LoadTexture(@"Content/Textures/HealthMeter.png"), this.Player.Stamina, this.Player.MaximumStamina,
                new Rectangle(190, 22, 197, 35), new Rectangle(190, 94, 195, 26), new Rectangle(190, 118, 195, 28), new Vector2(45, 45),
                this.ScreenManager.Game.Content.Load<SpriteFont>(@"Fonts/04b03_16"));

            this.PlayerStaminaMeter.SetText(String.Format("STA {0}/{1}", this.Player.Stamina, this.Player.MaximumStamina));


            base.Initialize();
        }

        /// <summary>
        /// Load graphics content for the game.
        /// </summary>
        public override void LoadContent() {
            if (content == null)
                content = new ContentManager(ScreenManager.Game.Services, "Content");

            gameFont = content.Load<SpriteFont>("gamefont");

            Animation boneAttack = new Animation("Bone", this.AssetsService.LoadTexture(@"Content/Textures/Animations/BoneAnimation.png"), 16, 16, 0.025f, new Vector2(), Color.White);

            AnimationService.Instance.AddAnimation("BoneAttack", boneAttack);

            Animation smokeAnimation = new Animation("Smoke", this.AssetsService.LoadTexture(@"Content/Textures/Animations/SmokeAnimation.png"), 16, 16, 0.05f, Vector2.Zero, Color.White, loop: false);

            AnimationService.Instance.AddAnimation("Smoke", smokeAnimation);

            Animation bossBoneAttack = new Animation("BoneBoss", this.AssetsService.LoadTexture(@"Content/Textures/Animations/SkeletonBossBoneAnimation.png"), 64, 64, 0.025f, new Vector2(), Color.White);

            AnimationService.Instance.AddAnimation("BossBoneAttack", bossBoneAttack);

            Animation squish = new Animation("Squish", this.AssetsService.LoadTexture(@"Content/Textures/Animations/Squish2.png"), 16, 16, 0.075f, new Vector2(), Color.White, loop: true);

            AnimationService.Instance.AddAnimation("Squish", squish);

            foreach (Character character in Characters) {
                character.Load(this.AssetsService);
            }

            // A real game would probably have more content than this sample, so
            // it would take longer to load. We simulate that by delaying for a
            // while, giving you a chance to admire the beautiful loading screen.
            Thread.Sleep(1000);

            Music.Play("DST-Aronara", true);

            // once the load has finished, we use ResetElapsedTime to tell the game's
            // timing mechanism that we have just finished a very long frame, and that
            // it should not try to catch up.
            ScreenManager.Game.ResetElapsedTime();

            this.InventoryRenderer = new InventoryRenderer(this.AssetsService.LoadTexture(@"Content/Textures/ItemsUI.png"), this.ScreenManager.Game.Content.Load<SpriteFont>(@"Fonts/04b03_16"), PlayerInventory);

        }


        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void UnloadContent() {
            Music.Stop();
            content.Unload();
        }


        #endregion

        #region Update and Draw

        public void RegenerateAsShop() {
            this.MapGenerator = new ShopMapGenerator();
            this.map = this.MapGenerator.Generate(Difficulty.Easy, (this.ScreenManager.Game as IGameService));

            Tileset tileset = new Tileset(this.AssetsService.LoadTexture(@"Content/Textures/Tileset.png"), 16, 16);
            this.MapRenderer = new MapRenderer(this.GraphicsEngine, this.map, tileset, Player, this.ScreenManager.Game.Content.Load<SpriteFont>(@"Fonts/04b03_06"));
            Player.SetPosition(new Vector2
            (
            map.SpawnRoomPosition.X * (float)Room.MaxWidth * tileset.TileWidth + ((float)(Room.MaxWidth * tileset.TileWidth) / 2) - (tileset.TileWidth / 2),
            map.SpawnRoomPosition.Y * (float)Room.MaxHeight * tileset.TileHeight + ((float)(Room.MaxHeight * tileset.TileHeight) / 2) - (tileset.TileHeight / 2))
            );

            Player.SetSpawnPosition(Player.Position);

            Characters.Clear();

            Characters.Add(Player);

            var shopKeeper = new ShopKeeper(new Vector2
                                    (
                                    0 * (float)Room.MaxWidth * tileset.TileWidth + ((float)(Room.MaxWidth * tileset.TileWidth) / 2) - (tileset.TileWidth / 2),
                                    0 * (float)Room.MaxHeight * tileset.TileHeight + ((float)(Room.MaxHeight * tileset.TileHeight) / 2) - (tileset.TileHeight / 2))
                                    );

            Characters.Add(shopKeeper);

            this.map.AddItem(new BoneCharm(this.ItemsTexture, new Vector2((0 * (float)Room.MaxWidth) + Room.MaxWidth / 2, (0 * (float)Room.MaxHeight) + Room.MaxHeight / 4) * this.tileset.TileWidth, false) { IsForSale = true, Cost = 50, CurrencyTextureCoordinates = new Rectangle(0, 0, 16, 16) });


            foreach (Character character in Characters) {
                character.Load(this.AssetsService);
            }

            this.EntityRenderer = new EntityRenderer(Characters);

            this.GraphicsEngine.ClearLights();

            foreach (Light light in this.map.LightPositions) {
                this.GraphicsEngine.AddLight(light);
            }
        }

        public void RegenerateAsDungeon() {
            this.MapGenerator = new BasicMapGenerator(new RoomService(this.AssetsService));

            PopupManager.Close();

            this.map = this.MapGenerator.Generate(Difficulty.Easy, (this.ScreenManager.Game as IGameService));
            Tileset tileset = new Tileset(this.AssetsService.LoadTexture(@"Content/Textures/Tileset.png"), 16, 16);
            this.MapRenderer = new MapRenderer(this.GraphicsEngine, this.map, tileset, Player, this.ScreenManager.Game.Content.Load<SpriteFont>(@"Fonts/04b03_06"));
            Player.SetPosition(new Vector2
            (
            map.SpawnRoomPosition.X * (float)Room.MaxWidth * tileset.TileWidth + ((float)(Room.MaxWidth * tileset.TileWidth) / 2) - (tileset.TileWidth / 2),
            map.SpawnRoomPosition.Y * (float)Room.MaxHeight * tileset.TileHeight + ((float)(Room.MaxHeight * tileset.TileHeight) / 2) - (tileset.TileHeight / 2))
            );

            Player.SetSpawnPosition(Player.Position);

            Characters.Clear();

            Characters.Add(Player);

            SkeletonBoss = new SkeletonBoss(new Vector2
                    (
                    map.ExitRoomPosition.X * (float)Room.MaxWidth * tileset.TileWidth + ((float)(Room.MaxWidth * tileset.TileWidth) / 2) - (tileset.TileWidth / 2),
                    map.ExitRoomPosition.Y * (float)Room.MaxHeight * tileset.TileHeight + ((float)(Room.MaxHeight * tileset.TileHeight) / 2) - (tileset.TileHeight / 2))
                    );

            this.BossHealthMeter = new Meter(this.AssetsService.LoadTexture(@"Content/Textures/HealthMeter.png"), this.SkeletonBoss.Health, this.SkeletonBoss.MaximumHealth,
                new Rectangle(190, 22, 197, 35), new Rectangle(190, 70, 195, 26), new Rectangle(190, 118, 195, 28), new Vector2(400 - 95, ScreenManager.GraphicsDevice.Viewport.Height - 32),
                this.ScreenManager.Game.Content.Load<SpriteFont>(@"Fonts/04b03_16"));

            this.BossHealthMeter.SetText(String.Format("HP {0}/{1}", this.SkeletonBoss.Health, this.SkeletonBoss.MaximumHealth));

            Characters.Add(SkeletonBoss);

            this.map.Boss = SkeletonBoss;

            for (int y = 0; y < map.Height; y++) {
                for (int x = 0; x < map.Width; x++) {

                    Room room = null;

                    room = this.map.Rooms[x, y];

                    if ((this.map.SpawnRoomPosition != new Point(x, y))) {
                        if (room != null) {

                            int checkpointChance = random.Next(1, 101);

                            if (checkpointChance <= 10) {

            //                    if ((this.map.ExitRoomPosition != new Point(x, y))) {

            //                        this.map.AddItem(new Checkpoint(this.ItemsTexture, new Vector2
            //(
            //x * (float)Room.MaxWidth * tileset.TileWidth + ((float)(Room.MaxWidth * tileset.TileWidth) / 2) - (tileset.TileWidth / 2),
            //y * (float)Room.MaxHeight * tileset.TileHeight + ((float)(Room.MaxHeight * tileset.TileHeight) / 2) - (tileset.TileHeight / 2))));

            //                    }

                            } else {
                                int totalMobs = new Random(new Random().Next(0, 100000)).Next(1, 4);

                                //HACK
                                if (this.map.ExitRoomPosition == new Point(x, y)) {
                                    totalMobs = 0;
                                }

                                for (int i = 0; i < totalMobs; i++) {

                                    Vector2 randomSpawnPosition = room.GetRandomOpenPosition();

                                    int mobChance = random.Next(1, 101);

                                    if (mobChance <= 50) {
                                        var skeleton = new Skeleton(new Vector2
                                        (
                                        x * (float)Room.MaxWidth * tileset.TileWidth + randomSpawnPosition.X * tileset.TileWidth,
                                        y * (float)Room.MaxHeight * tileset.TileHeight + randomSpawnPosition.Y * tileset.TileHeight)
                                        );

                                        room.Characters.Add(skeleton);

                                        Characters.Add(skeleton);
                                    } else {
                                        var zombie = new Zombie(new Vector2
                                        (
                                        x * (float)Room.MaxWidth * tileset.TileWidth + randomSpawnPosition.X * tileset.TileWidth,
                                        y * (float)Room.MaxHeight * tileset.TileHeight + randomSpawnPosition.Y * tileset.TileHeight)
                                        );

                                        room.Characters.Add(zombie);

                                        Characters.Add(zombie);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            foreach (Character character in Characters) {
                character.Load(this.AssetsService);
            }

            this.EntityRenderer = new EntityRenderer(Characters);

            this.GraphicsEngine.ClearLights();

            foreach (Light light in this.map.LightPositions) {
                this.GraphicsEngine.AddLight(light);
            }
        }

        /// <summary>
        /// Updates the state of the game. This method checks the GameScreen.IsActive
        /// property, so the game will stop updating when the pause menu is active,
        /// or if you tab away to a different application.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen) {
            base.Update(gameTime, otherScreenHasFocus, false);

            KeyboardState keyboard = Keyboard.GetState();

            bool regenerate = false;

            UpdateCamera();

            //UpdateUIInformation
            UpdateUIInformation();

            Vector2 playerRoomPosition = this.map.GetRoomPositionOfEntity(this.tileset, this.Player);

            if (!this.IsPaused) {
                //Process Character Specific Logic
                foreach (Character character in Characters) {

                    Vector2 playerRoom = this.map.GetRoomPositionOfEntity(this.tileset, this.Player);
                    Vector2 entityRoom = this.map.GetRoomPositionOfEntity(this.tileset, character);
                    Vector2 characterRoomPosition = this.map.GetRoomPositionOfEntity(this.tileset, character);

                    float distance = Vector2.Distance(playerRoomPosition, characterRoomPosition);

                    if (distance < 3) {

                        if (playerRoomPosition == new Vector2(this.map.ExitRoomPosition.X, this.map.ExitRoomPosition.Y)) {
                            if (characterRoomPosition == playerRoomPosition) {
                                character.Update(gameTime);
                            }
                        } else {
                            character.Update(gameTime);
                        }

                        //if (playerRoom == entityRoom) {
                        
                    } else {
                        //return;
                    }

                    UpdateEntitySpecificLogic(gameTime, character);

                    if (character is Player) {


                        Room currentRoom = this.map.GetRoomOfEntity(this.tileset, Player);
                        Room deflatedRoom = this.map.GetDeflatedRoomOfEntity(this.tileset, Player);

                        if (deflatedRoom != null && deflatedRoom == this.map.Rooms[this.map.ExitRoomPosition.X, this.map.ExitRoomPosition.Y]) {

                            if (SkeletonBoss != null) {
                                if (SkeletonBoss.IsAlive) {
                                    Player.StuckInCurrentRoom = true;
                                } else {
                                    Player.StuckInCurrentRoom = false;
                                }
                            }

                        } else {
                            //Player.StuckInCurrentRoom = false;
                        }

                        if (currentRoom != null) {
                            foreach (Rectangle warp in currentRoom.Warps) {
                                if (Player.Bounds.Intersects(warp)) {

                                    PopupManager.Show("Dungeon", new Vector2(Player.Position.X - 50, Player.Position.Y - 25));

                                    if (keyboard.IsKeyDown(Keys.C) && !previousKeyboard.IsKeyDown(Keys.C)) {
                                        regenerate = true;

                                    }
                                } else {

                                    if (PopupManager.CurrentMessageKey == "Dungeon") {
                                        PopupManager.Close();
                                    }
                                }
                            }
                        }

                        UpdateItems(gameTime);

                    }

                    UpdateEntityCollisions(character);

                    this.PhysicsService.ResolvePosition(character, this.map, this.tileset, gameTime);
                    //}

                } //End foreach:character

                UpdateCombat();

                UpdateAnimationQueue(gameTime);

                UpdateDeadCharacters();

                UpdateTransition(coveredByOtherScreen);

                previousKeyboard = keyboard;

                if (regenerate) {
                    HandleRegenerationRequest();
                }

            }
        }

        private void UpdateCamera() {
            GraphicsEngine.Camera.SetPosition(
                -Player.Position.X + (this.ScreenManager.Game as IGameService).Graphics.GraphicsDevice.Viewport.Width / 4 - (this.tileset.TileWidth / 2),
                -Player.Position.Y + (this.ScreenManager.Game as IGameService).Graphics.GraphicsDevice.Viewport.Height / 4 - (this.tileset.TileHeight / 2));
        }

        private void HandleRegenerationRequest() {
            RegenerateAsDungeon();
            this.EntityRenderer = new EntityRenderer(Characters);
        }

        private void UpdateUIInformation() {

            this.PlayerHealthMeter.SetText(String.Format("HP {0}/{1}", this.Player.Health, this.Player.MaximumHealth));

            this.PlayerHealthMeter.SetCurrentValue(Player.Health);

            this.PlayerHealthMeter.SetMaxValue(Player.MaximumHealth);

            this.PlayerStaminaMeter.SetText(String.Format("STA {0}/{1}", this.Player.Stamina, this.Player.MaximumStamina));

            this.PlayerStaminaMeter.SetCurrentValue(Player.Stamina);

            this.PlayerStaminaMeter.SetMaxValue(Player.MaximumStamina);

            if (this.BossHealthMeter != null) {
                this.BossHealthMeter.SetCurrentValue(SkeletonBoss.Health);

                this.BossHealthMeter.SetMaxValue(SkeletonBoss.MaximumHealth);

                this.BossHealthMeter.SetText(String.Format("HP {0}/{1}", this.SkeletonBoss.Health, this.SkeletonBoss.MaximumHealth));
            }

            this.InventoryRenderer.SetInventory(PlayerInventory);

        }

        private void UpdateCombat() {
            if (this.Player.State == EntityState.Attacking) {
                IEnumerable<Character> attackedCharacters = Characters.Where(c => this.Player.AttackBounds.Inflate(4).Intersects(c.Bounds) && (c != this.Player));

                foreach (Character character in attackedCharacters) {
                    character.Damage(this.Player);
                }

            }
        }

        private void UpdateTransition(bool coveredByOtherScreen) {
            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
                pauseAlpha = Math.Min(pauseAlpha + 1f / 32, 1);
            else
                pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);
        }

        bool added = false;

        private void UpdateEntitySpecificLogic(GameTime gameTime, Character character) {

            if (!added) {
                Animation squishAnimation = (Animation)AnimationService.Instance.GetAnimation("Squish").Clone();
                squishAnimation.SetPosition(Player.Position);
                this.AnimationQueue.Add(squishAnimation);
                added = true;
            }

            if (character is Skeleton) {
                Skeleton skeleton = (character as Skeleton);

                List<BoneAttackEntity> bonesToRemove = new List<BoneAttackEntity>();

                foreach (BoneAttackEntity bone in skeleton.Bones) {
                    Rectangle boneBounds = new Rectangle((int)bone.Position.X, (int)bone.Position.Y, 16, 16);

                    float distance = Vector2.Distance(Player.Position, bone.Position);

                    if (distance < 300) {
                        if (boneBounds.Intersects(Player.Bounds)) {
                            Player.Damage(null);
                            bone.Collide(Player);
                        }

                        this.PhysicsService.ResolvePosition(bone, this.map, this.tileset, gameTime);

                        if (!bone.IsAlive) {
                            bonesToRemove.Add(bone);

                            Animation animation = (Animation)AnimationService.Instance.GetAnimation("Smoke").Clone();
                            animation.SetPosition(bone.Position);
                            this.AnimationQueue.Add(animation);
                        }
                    } else {
                        bonesToRemove.Add(bone);
                    }
                }

                foreach (BoneAttackEntity bone in bonesToRemove) {
                    skeleton.Bones.Remove(bone);
                }
            }

            if (character is Zombie) {

                if (character.State == EntityState.Attacking) {

                    if (character.Bounds.Inflate(8).Intersects(Player.Bounds)) {
                        Player.Damage(character);

                        int poisonChance = random.Next(1, 101);

                        if (poisonChance <= 10) {
                            if (!(Player.Effect is HealOverTime)) {
                                Player.SetEffect(new PoisonEffect(3, Player));
                            }
                        }
                    }
                }
            }

            if (character is SkeletonBoss) {
                SkeletonBoss skeleton = (character as SkeletonBoss);
                List<BoneAttackEntity> bonesToRemove = new List<BoneAttackEntity>();

                foreach (BoneAttackEntity bone in skeleton.Bones) {
                    Rectangle boneBounds = new Rectangle((int)bone.Position.X + 4, (int)bone.Position.Y + 4, 64 - 8, 64 - 8);

                    if (boneBounds.Intersects(Player.Bounds)) {
                        Player.Damage(skeleton);
                        bone.Collide(null);
                    }

                    this.PhysicsService.ResolvePosition(bone, this.map, this.tileset, gameTime);

                    if (!bone.IsAlive) {
                        bonesToRemove.Add(bone);
                    }
                }

                foreach (BoneAttackEntity bone in bonesToRemove) {
                    skeleton.Bones.Remove(bone);
                }
            }
        }

        private void UpdateItems(GameTime gameTime) {
            KeyboardState keyboard = Keyboard.GetState();

            IList<Item> itemsToRemove = new List<Item>();

            foreach (Item item in this.map.Items) {

                bool intersects = Player.Bounds.Inflate(14).Intersects(item.Bounds);
                bool normalIntersects = Player.Bounds.Intersects(item.Bounds);

                item.Update(gameTime);

                //if (intersects) {
                //    PopupManager.Show("BoneRunes", new Vector2(item.Position.X - 50, item.Position.Y - 25));
                //    //return;
                //}
                if (item is Checkpoint && normalIntersects) {
                    itemsToRemove.Add(item);

                    Player.SetSpawnPosition(item.Position);

                }

                if (normalIntersects && item.CanPickup) {
                    itemsToRemove.Add(item);
                }
                if (intersects && item.IsForSale) {
                    // if (item is BoneCharm) {
                    //PopupManager.Show("BoneRunes", new Vector2(item.Position.X - 50, item.Position.Y - 25));

                    if (keyboard.IsKeyDown(Keys.C) && !previousKeyboard.IsKeyDown(Keys.C)) {
                        if (item is BoneCharm) {
                            if (PlayerInventory.BoneEssences >= item.Cost) {
                                PlayerInventory.BoneEssences -= item.Cost;
                                Player.IncreaseDefense();
                            }
                        } else if (item is HealthPotion) {
                            if (PlayerInventory.LifeEssences >= item.Cost) {
                                PlayerInventory.LifeEssences -= item.Cost;
                                PlayerInventory.HealthPotions += 1;
                            }
                        } else if (item is FerocityCharm) {
                            if (PlayerInventory.FerocityEssences >= item.Cost) {
                                PlayerInventory.FerocityEssences -= item.Cost;
                                Player.IncreaseAttack();
                            }
                        }
                    }
                    // }
                } else {
                    if (PopupManager.CurrentMessageKey == "BoneRunes") {
                        PopupManager.Close();
                    }
                }
            }

            foreach (Item item in itemsToRemove) {

                if (item is BoneEssence) {
                    PlayerInventory.BoneEssences += item.Quantity;
                }

                if (item is FerocityEssence) {
                    PlayerInventory.FerocityEssences += item.Quantity;
                }

                if (item is LifeEssence) {
                    PlayerInventory.LifeEssences += item.Quantity;
                }

                if (item is HealthPotion) {
                    PlayerInventory.HealthPotions += 1;
                }

                if (item is StaminaPotion) {
                    PlayerInventory.StaminaPotions += 1;
                }

                PickupSound.Play(Music.Volume, 0f, 0f);

                this.map.RemoveItem(item);
            }
        }

        private void UpdateEntityCollisions(Character character) {

            KeyboardState keyboard = Keyboard.GetState();

            foreach (Character otherCharacter in Characters) {
                if (character != otherCharacter) {


                    if (otherCharacter is ShopKeeper) {

                        if (Player.Bounds.Inflate(10).Intersects(otherCharacter.Bounds.Inflate(10))) {
                            if (keyboard.IsKeyDown(Keys.C) && !previousKeyboard.IsKeyDown(Keys.C)) {
                                PopupManager.Show("ShopKeepMessage", new Vector2(otherCharacter.Position.X - 50, otherCharacter.Position.Y - 25));
                            }
                        } else {
                            if (PopupManager.CurrentMessageKey == "ShopKeepMessage") {
                                PopupManager.Close();
                            }
                        }
                    }

                    if (otherCharacter is Player) {
                        if (Player.Bounds.Inflate(10).Intersects(character.Bounds.Inflate(10))) {
                            character.SetVelocity(Vector2.Zero);
                        }
                    }

                    if (character.Bounds.Intersects(otherCharacter.Bounds)) {
                        otherCharacter.Collide(character);
                    }

                    Vector2 currentPosition = character.Position;

                    //TODO: Measure from centers for accurate distance regardless of sprite size
                    float xDistance = otherCharacter.Center.X - character.Center.X;
                    float yDistance = otherCharacter.Center.Y - character.Center.Y;

                    if (character.Velocity != Vector2.Zero) {
                        //Debug.Print(currentPosition.ToString());
                        if (Math.Abs(xDistance) > Math.Abs(yDistance)) {
                            if (character.Center.X < otherCharacter.Center.X) {
                                this.PhysicsService.ResolveCollision(character, otherCharacter, this.map, this.tileset, Framework.Runtime.Axis.Horizontal);
                            } if (character.Center.X >= otherCharacter.Center.X) {
                                this.PhysicsService.ResolveCollision(character, otherCharacter, this.map, this.tileset, Framework.Runtime.Axis.Horizontal);
                            }
                        } else {
                            if (character.Center.Y < otherCharacter.Center.Y) {
                                this.PhysicsService.ResolveCollision(character, otherCharacter, this.map, this.tileset, Framework.Runtime.Axis.Vertical);
                            } if (character.Center.Y >= otherCharacter.Center.Y) {
                                this.PhysicsService.ResolveCollision(character, otherCharacter, this.map, this.tileset, Framework.Runtime.Axis.Vertical);
                            }
                        }
                    }

                }
            } //End foreach:character (Physics)
        }

        /// <summary>
        /// Update the world animation queue
        /// </summary>
        /// <param name="gameTime"></param>
        private void UpdateAnimationQueue(GameTime gameTime) {

            foreach (Animation animation in this.AnimationQueue) {

                animation.Update(gameTime);

            }

            for (int i = 0; i < this.AnimationQueue.Count; i++) {
                if (this.AnimationQueue[i].Enabled == false) {
                    this.AnimationQueue.RemoveAt(i);
                    i--;
                }
            }
        }

        private Vector2 PositionAdditive {
            get {
                return new Vector2(random.Next(-4, 4), random.Next(-4, 4));
            }
        }
        /// <summary>
        /// Updates Dead Characters - Either removing them from the characters list
        /// or respawning them during specific scenarios
        /// </summary>
        private void UpdateDeadCharacters() {
            //Remove dead characters
            for (int i = 0; i < Characters.Count; i++) {
                if (Characters[i].IsAlive == false) {
                    Animation animation = (Animation)AnimationService.Instance.GetAnimation("Smoke").Clone();
                    animation.SetPosition(Characters[i].Position);
                    this.AnimationQueue.Add(animation);

                    if (!(Characters[i] is Player)) {

                        if (Characters[i] is Skeleton || Characters[i] is Zombie) {
                            this.map.AddItem(new BoneEssence(this.ItemsTexture, Characters[i].Position + PositionAdditive));
                            this.map.AddItem(new FerocityEssence(this.ItemsTexture, Characters[i].Position + PositionAdditive));

                            int healthPotionChance = random.Next(1, 101);
                            int staminaPotionChance = random.Next(1, 101);
                            int lifeEssenceChance = random.Next(1, 101);

                            if (healthPotionChance <= 30) {
                                this.map.AddItem(new HealthPotion(this.ItemsTexture, Characters[i].Position + PositionAdditive, true));
                            }

                            if (staminaPotionChance <= 30) {
                                this.map.AddItem(new StaminaPotion(this.ItemsTexture, Characters[i].Position + PositionAdditive, true));
                            }

                            if (lifeEssenceChance <= 40) {
                                this.map.AddItem(new LifeEssence(this.ItemsTexture, Characters[i].Position + PositionAdditive));
                            }

                        } else if (Characters[i] is SkeletonBoss) {
                            this.map.AddItem(new BoneEssence(this.ItemsTexture, Characters[i].Position + PositionAdditive, 8, 15));
                            this.map.AddItem(new FerocityEssence(this.ItemsTexture, Characters[i].Position + PositionAdditive));

                            int healthPotionChance = random.Next(1, 101);
                            int lifeEssenceChance = random.Next(1, 101);

                            if (healthPotionChance <= 80) {
                                this.map.AddItem(new HealthPotion(this.ItemsTexture, Characters[i].Position + PositionAdditive, true));
                            }
                            if (lifeEssenceChance <= 70) {
                                this.map.AddItem(new LifeEssence(this.ItemsTexture, Characters[i].Position + PositionAdditive, 5, 10));
                            }
                        }

                        Room room = map.GetRoomOfEntity(this.tileset, Characters[i]);

                        if (room.Characters.Contains(Characters[i])) {
                            room.Characters.Remove(Characters[i]);
                        }

                        Characters.RemoveAt(i);

                        i--;
                    } else {
                        this.Player.StuckInCurrentRoom = false;
                        this.Player.Spawn();
                        _respawnSound.Play(Music.Volume, 0f, 0f);
                    }
                }
            }
        }


        /// <summary>
        /// Lets the game respond to player input. Unlike the Update method,
        /// this will only be called when the gameplay screen is active.
        /// </summary>
        public override void HandleInput(InputState input) {
            if (input == null)
                throw new ArgumentNullException("input");

            // Look up inputs for the active player profile.
            int playerIndex = (int)ControllingPlayer.Value;

            this.IsPaused = input.IsPauseGame(ControllingPlayer);

            KeyboardState keyboardState = input.CurrentKeyboardStates[playerIndex];
            GamePadState gamePadState = input.CurrentGamePadStates[playerIndex];

            if (!input.IsPauseGame(ControllingPlayer)) {
                this.CharacterControllerService.Update();
            }
            // The game pauses either if the user presses the pause button, or if
            // they unplug the active gamepad. This requires us to keep track of
            // whether a gamepad was ever plugged in, because we don't want to pause
            // on PC if they are playing with a keyboard and have no gamepad at all!
            bool gamePadDisconnected = !gamePadState.IsConnected &&
                                       input.GamePadWasConnected[playerIndex];

            if (input.IsPauseGame(ControllingPlayer) || gamePadDisconnected) {
                ScreenManager.AddScreen(new PauseMenuScreen(), PlayerIndex.One);
            }
        }


        /// <summary>
        /// Draws the gameplay screen.
        /// </summary>
        public override void Draw(GameTime gameTime) {

            this.GraphicsEngine.BeginDraw();

            this.GraphicsEngine.Draw(gameTime, this.MapRenderer);

            this.GraphicsEngine.Draw(gameTime, this.EntityRenderer);

            foreach (Animation animation in this.AnimationQueue) {
                this.GraphicsEngine.Draw(gameTime, animation);
            }

            this.GraphicsEngine.Draw(gameTime, this.PopupManager);

            this.GraphicsEngine.EndDraw();

            this.GraphicsEngine.BeginDrawUI();

            this.GraphicsEngine.DrawUI(gameTime, this.PlayerHealthMeter);

            this.GraphicsEngine.DrawUI(gameTime, this.PlayerStaminaMeter);

            if (this.map.Boss != null && (this.map.Boss as ILivingEntity).IsAlive) {
                Vector2 playerRoomCoordinates = this.map.GetRoomPositionOfEntity(this.tileset, Player);

                if (playerRoomCoordinates == new Vector2(this.map.ExitRoomPosition.X, this.map.ExitRoomPosition.Y)) {
                    this.GraphicsEngine.DrawUI(gameTime, this.BossHealthMeter);
                }
            }

            this.GraphicsEngine.DrawUI(gameTime, this.InventoryRenderer);

            this.GraphicsEngine.DrawUI(gameTime, this.StatsRenderer);

            this.GraphicsEngine.EndDrawUI();

            // If the game is transitioning on or off, fade it out to black.
            if (TransitionPosition > 0 || pauseAlpha > 0) {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);

                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }


        #endregion
    }
}
