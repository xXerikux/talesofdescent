﻿using MetavirusGames.Framework.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Entities {
    public sealed class LifeEssence : Item {

        public LifeEssence(Texture2D texture, Vector2 position, int minQuantity = 1, int maxQuanity = 4)
            : base("Essence of Life", texture, position, new Rectangle(32, 0, 16, 16), 1) {

            this.CanPickup = true;

            this.Quantity = Random.Next(minQuantity, maxQuanity);

        }
    }
}
