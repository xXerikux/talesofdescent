﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Entities {
    public class Inventory {

        public int BoneEssences;

        public int FerocityEssences;

        public int EnduranceEssences;

        public int LifeEssences;

        public int MagicEssences;

        public int SkeletonKeys;

        public int Keys;

        public int HealthPotions;

        public int StaminaPotions;

    }
}
