﻿using MetavirusGames.Framework.AI;
using MetavirusGames.Framework.Data;
using MetavirusGames.Framework.Entities;
using MetavirusGames.Framework.Entities.Interfaces;
using MetavirusGames.Framework.Runtime;
using MetavirusGames.Framework.Services;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Entities {
    public sealed class SkeletonBoss : Character, IBoss {

        private const int MaxHealth = 25;

        private const int MaxStamina = 5;
        
        private const int BaseAttackPower = 4;

        public IList<BoneAttackEntity> Bones;

        private Timer _allowBoneThrowTimer;

        private bool _canThrowBones = true;

        public override Rectangle Bounds {
            get {
                return new Rectangle((int)Math.Round(Position.X) + 6, (int)Math.Round(Position.Y) + 2, 64 - 12, 64 - 4);
            }
        }

        public SkeletonBoss(Vector2 position)
            : base("Skeleton", position, MaxHealth, MaxHealth, MaxStamina, MaxStamina, BaseAttackPower, 1, EntityState.Idle, EntityHeading.Down, true) {

            Bones = new List<BoneAttackEntity>();

            _allowBoneThrowTimer = new Timer(() => { this._canThrowBones = true; }, 3f, repeats: true);

        }

        public override void Load(IAssetService assetService) {

            RegisterAnimation(EntityHeading.Up, EntityState.Walking, new Animation("WalkUp", assetService.LoadTexture(@"Content/Textures/Animations/SkeletonBossWalkUp.png"), 64, 64, 0.25f, Vector2.Zero, Color.White));
            RegisterAnimation(EntityHeading.Down, EntityState.Walking, new Animation("WalkDown", assetService.LoadTexture(@"Content/Textures/Animations/SkeletonBossWalkDown.png"), 64, 64, 0.25f, Vector2.Zero, Color.White));
            RegisterAnimation(EntityHeading.Left, EntityState.Walking, new Animation("WalkLeft", assetService.LoadTexture(@"Content/Textures/Animations/SkeletonBossWalkLeft.png"), 64, 64, 0.25f, Vector2.Zero, Color.White));
            RegisterAnimation(EntityHeading.Right, EntityState.Walking, new Animation("WalkRight", assetService.LoadTexture(@"Content/Textures/Animations/SkeletonBossWalkRight.png"), 64, 64, 0.25f, Vector2.Zero, Color.White));

            RegisterAnimation(EntityHeading.Up, EntityState.Idle, new Animation("IdleUp", assetService.LoadTexture(@"Content/Textures/Animations/SkeletonBossWalkUp.png"), 64, 64, 0.25f, Vector2.Zero, Color.White, firstFrameOnly: true));
            RegisterAnimation(EntityHeading.Down, EntityState.Idle, new Animation("IdleDown", assetService.LoadTexture(@"Content/Textures/Animations/SkeletonBossWalkDown.png"), 64, 64, 0.25f, Vector2.Zero, Color.White, firstFrameOnly: true));
            RegisterAnimation(EntityHeading.Left, EntityState.Idle, new Animation("IdleLeft", assetService.LoadTexture(@"Content/Textures/Animations/SkeletonBossWalkLeft.png"), 64, 64, 0.25f, Vector2.Zero, Color.White, firstFrameOnly: true));
            RegisterAnimation(EntityHeading.Right, EntityState.Idle, new Animation("IdleRight", assetService.LoadTexture(@"Content/Textures/Animations/SkeletonBossWalkRight.png"), 64, 64, 0.25f, Vector2.Zero, Color.White, firstFrameOnly: true));

            SetBehavior(new RandomMovementTowardsPlayerBehavior(this, 0.75f, 10));

            base.Load(assetService);
        }

        public override void Update(GameTime gameTime) {

            if (this.IsAlive) {
                this._allowBoneThrowTimer.Update(gameTime);

                if (this._canThrowBones) {
                    this._canThrowBones = false;

                    BoneAttackEntity boneToAdd = null;

                    if (this.Heading == EntityHeading.Up) {
                        boneToAdd = new BoneAttackEntity(this, this.Position + new Vector2(0, -50), new Vector2(0, -2.5f), "BossBoneAttack");
                    } else if (this.Heading == EntityHeading.Down) {
                        boneToAdd = new BoneAttackEntity(this, this.Position + new Vector2(0, 50), new Vector2(0, 2.5f), "BossBoneAttack");
                    } else if (this.Heading == EntityHeading.Left) {
                        boneToAdd = new BoneAttackEntity(this, this.Position + new Vector2(-50, 0), new Vector2(-2.5f, 0), "BossBoneAttack");
                    } else if (this.Heading == EntityHeading.Right) {
                        boneToAdd = new BoneAttackEntity(this, this.Position + new Vector2(50, 0), new Vector2(2.5f, 0), "BossBoneAttack");
                    }

                    if (boneToAdd != null) {
                        this.Bones.Add(boneToAdd);
                    }
                }
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch) {

            if (this.IsAlive) {
                foreach (BoneAttackEntity bone in Bones) {

                    bone.Draw(gameTime, spriteBatch);
                }
            }

            base.Draw(gameTime, spriteBatch);
            
        }

        public override void Damage(Framework.Entities.Interfaces.ILivingEntity attacker) {

            if (attacker is Character) {
                Character character = (attacker as Character);

                float xDistance = this.Bounds.Center.X - character.Bounds.Center.X;
                float yDistance = this.Bounds.Center.Y - character.Bounds.Center.Y;

                if (Math.Abs(xDistance) > Math.Abs(yDistance)) {
                    if (character.Bounds.Center.X < this.Bounds.Center.X) {
                        this.SetHeading(EntityHeading.Left);
                        //this.SetPosition(this.Position + new Vector2(5, 0));
                    } if (character.Bounds.Center.X >= this.Bounds.Center.X) {
                        this.SetHeading(EntityHeading.Right);
                        //this.SetPosition(this.Position + new Vector2(-5, 0));
                    }
                } else {
                    if (character.Bounds.Center.Y < this.Bounds.Center.Y) {
                        this.SetHeading(EntityHeading.Up);
                        //this.SetPosition(this.Position + new Vector2(0, 5));
                    } if (character.Bounds.Center.Y >= this.Bounds.Center.Y) {
                        this.SetHeading(EntityHeading.Down);
                        //this.SetPosition(this.Position + new Vector2(0, -5));
                    }
                }



            }

            base.Damage(attacker);
        }
    }
}
