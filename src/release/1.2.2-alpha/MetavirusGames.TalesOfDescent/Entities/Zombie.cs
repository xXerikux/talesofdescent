﻿using MetavirusGames.Framework.AI;
using MetavirusGames.Framework.Data;
using MetavirusGames.Framework.Entities;
using MetavirusGames.Framework.Runtime;
using MetavirusGames.Framework.Services;
using MetavirusGames.TalesOfDescent.AI;
using MetavirusGames.TalesOfDescent.Screens;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace MetavirusGames.TalesOfDescent.Entities {
    public sealed class Zombie  : Character {

        private const int MaxHealth = 15;

        private const int MaxStamina = 5;

        private const int BaseAttackPower = 3;

        public Zombie(Vector2 position)
            : base("Zombie", position, MaxHealth, MaxHealth, MaxStamina, MaxStamina, BaseAttackPower, 1, EntityState.Idle, EntityHeading.Down, true) {

        }

        public override void Load(IAssetService assetService) {

            Random random = new Random(new Random().Next(0, 10000));

            int zombie = random.Next(1, 3);

            RegisterAnimation(EntityHeading.Up, EntityState.Walking, new Animation("WalkUp", assetService.LoadTexture(String.Format(@"Content/Textures/Animations/Zombie{0}WalkUp.png", zombie)), 16, 16, 0.25f, Vector2.Zero, Color.White));
            RegisterAnimation(EntityHeading.Down, EntityState.Walking, new Animation("WalkDown", assetService.LoadTexture(String.Format(@"Content/Textures/Animations/Zombie{0}WalkDown.png", zombie)), 16, 16, 0.25f, Vector2.Zero, Color.White));
            RegisterAnimation(EntityHeading.Left, EntityState.Walking, new Animation("WalkLeft", assetService.LoadTexture(String.Format(@"Content/Textures/Animations/Zombie{0}WalkLeft.png", zombie)), 16, 16, 0.25f, Vector2.Zero, Color.White));
            RegisterAnimation(EntityHeading.Right, EntityState.Walking, new Animation("WalkRight", assetService.LoadTexture(String.Format(@"Content/Textures/Animations/Zombie{0}WalkRight.png", zombie)), 16, 16, 0.25f, Vector2.Zero, Color.White));

            RegisterAnimation(EntityHeading.Up, EntityState.Idle, new Animation("IdleUp", assetService.LoadTexture(String.Format(@"Content/Textures/Animations/Zombie{0}WalkUp.png", zombie)), 16, 16, 0.25f, Vector2.Zero, Color.White, firstFrameOnly: true));
            RegisterAnimation(EntityHeading.Down, EntityState.Idle, new Animation("IdleDown", assetService.LoadTexture(String.Format(@"Content/Textures/Animations/Zombie{0}WalkDown.png", zombie)), 16, 16, 0.25f, Vector2.Zero, Color.White, firstFrameOnly: true));
            RegisterAnimation(EntityHeading.Left, EntityState.Idle, new Animation("IdleLeft", assetService.LoadTexture(String.Format(@"Content/Textures/Animations/Zombie{0}WalkLeft.png", zombie)), 16, 16, 0.25f, Vector2.Zero, Color.White, firstFrameOnly: true));
            RegisterAnimation(EntityHeading.Right, EntityState.Idle, new Animation("IdleRight", assetService.LoadTexture(String.Format(@"Content/Textures/Animations/Zombie{0}WalkRight.png", zombie)), 16, 16, 0.25f, Vector2.Zero, Color.White, firstFrameOnly: true));

            RegisterAnimation(EntityHeading.Up, EntityState.Attacking,
                new Animation("AttackUp", assetService.LoadTexture(String.Format(@"Content/Textures/Animations/Zombie{0}WalkUp.png", zombie)), 16, 16, 0.025f, Vector2.Zero, Color.White,loop: false, animationEndAction: () => {
                    this.SetState(EntityState.Idle);
                }));
            RegisterAnimation(EntityHeading.Down, EntityState.Attacking,
                new Animation("AttackDown", assetService.LoadTexture(String.Format(@"Content/Textures/Animations/Zombie{0}WalkDown.png", zombie)), 16, 16, 0.025f, Vector2.Zero, Color.White,loop: false, animationEndAction: () => {
                    this.SetState(EntityState.Idle);
                }));
            RegisterAnimation(EntityHeading.Left, EntityState.Attacking,
                new Animation("AttackLeft", assetService.LoadTexture(String.Format(@"Content/Textures/Animations/Zombie{0}WalkLeft.png", zombie)), 16, 16, 0.025f, Vector2.Zero, Color.White, loop: false, animationEndAction: () => {
                    this.SetState(EntityState.Idle);
                }));
            RegisterAnimation(EntityHeading.Right, EntityState.Attacking,
                new Animation("AttackRight", assetService.LoadTexture(String.Format(@"Content/Textures/Animations/Zombie{0}WalkRight.png", zombie)), 16, 16, 0.025f, Vector2.Zero, Color.White, loop: false, animationEndAction: () => {
                    this.SetState(EntityState.Idle);
                }));

            SetBehavior(new AttackAtCloseRangeBehavior(this, 1f));

            base.Load(assetService);
        }

        public override void Update(GameTime gameTime) {

            if (this.IsAlive) {
                
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch) {

            if (this.IsAlive) {
                
            }

            base.Draw(gameTime, spriteBatch);
        }

        public override void Damage(Framework.Entities.Interfaces.ILivingEntity attacker) {

            if (attacker is Character) {
                Character character = (attacker as Character);

                float xDistance = this.Bounds.Center.X - character.Bounds.Center.X;
                float yDistance = this.Bounds.Center.Y - character.Bounds.Center.Y;

                if (Math.Abs(xDistance) > Math.Abs(yDistance)) {
                    if (character.Bounds.Center.X < this.Bounds.Center.X) {
                        this.SetHeading(EntityHeading.Left);
                        //this.SetPosition(this.Position + new Vector2(5, 0));
                    } if (character.Bounds.Center.X >= this.Bounds.Center.X) {
                        this.SetHeading(EntityHeading.Right);
                        //this.SetPosition(this.Position + new Vector2(-5, 0));
                    }
                } else {
                    if (character.Bounds.Center.Y < this.Bounds.Center.Y) {
                        this.SetHeading(EntityHeading.Up);
                        //this.SetPosition(this.Position + new Vector2(0, 5));
                    } if (character.Bounds.Center.Y >= this.Bounds.Center.Y) {
                        this.SetHeading(EntityHeading.Down);
                        //this.SetPosition(this.Position + new Vector2(0, -5));
                    }
                }



            }

            base.Damage(attacker);
        }
    }
}
