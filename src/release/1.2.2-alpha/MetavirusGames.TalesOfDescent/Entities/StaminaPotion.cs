﻿using MetavirusGames.Framework.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Entities {
    public sealed class StaminaPotion : Item {

        public StaminaPotion(Texture2D texture, Vector2 position, bool canPickup)
            : base("Stamina Potion", texture, position, new Rectangle(16, 32, 16, 16), 1) {

                CanPickup = canPickup;
        }
    }
}
