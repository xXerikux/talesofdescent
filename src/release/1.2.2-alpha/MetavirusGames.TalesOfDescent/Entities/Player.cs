﻿using MetavirusGames.Framework.Data;
using MetavirusGames.Framework.Entities;
using MetavirusGames.Framework.Runtime;
using MetavirusGames.Framework.Services;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Entities {
    public class Player : Character {

        private const int MaxHealth = 15;

        private const int BaseAttackPower = 3;

        private const int MaxStamina = 10;

        private Timer _healthTimer;

        public Player(string name, Vector2 position)
            : base(name, position, MaxHealth, MaxHealth, MaxStamina, MaxStamina, BaseAttackPower, 1, EntityState.Idle, EntityHeading.Down) {

                _healthTimer = new Timer(() => {

                    if (Health > (int)Math.Round((double)(MaximumHealth / 2)) && Health < MaximumHealth) {
                        Heal(1);
                    } else if (Health < (int)Math.Round((double)(MaximumHealth / 2)) && Health > 0) {
                        Heal(1);
                    }

                }, 10f, repeats: true);
        }

        public override void Load(IAssetService assetService) {

            RegisterAnimation(EntityHeading.Up, EntityState.Walking,
                new Animation("WalkUp", assetService.LoadTexture(@"Content/Textures/Animations/HeroWalkUp.png"), 16, 16, 0.25f, Vector2.Zero, Color.White));
            RegisterAnimation(EntityHeading.Down, EntityState.Walking,
                new Animation("WalkDown", assetService.LoadTexture(@"Content/Textures/Animations/HeroWalkDown.png"), 16, 16, 0.25f, Vector2.Zero, Color.White));
            RegisterAnimation(EntityHeading.Left, EntityState.Walking,
                new Animation("WalkLeft", assetService.LoadTexture(@"Content/Textures/Animations/HeroWalkLeft.png"), 16, 16, 0.25f, Vector2.Zero, Color.White));
            RegisterAnimation(EntityHeading.Right, EntityState.Walking,
                new Animation("WalkRight", assetService.LoadTexture(@"Content/Textures/Animations/HeroWalkRight.png"), 16, 16, 0.25f, Vector2.Zero, Color.White));

            RegisterAnimation(EntityHeading.Up, EntityState.Idle,
                new Animation("IdleUp", assetService.LoadTexture(@"Content/Textures/Animations/HeroWalkUp.png"), 16, 16, 0.25f, Vector2.Zero, Color.White, firstFrameOnly: true));
            RegisterAnimation(EntityHeading.Down, EntityState.Idle,
                new Animation("IdleDown", assetService.LoadTexture(@"Content/Textures/Animations/HeroWalkDown.png"), 16, 16, 0.25f, Vector2.Zero, Color.White, firstFrameOnly: true));
            RegisterAnimation(EntityHeading.Left, EntityState.Idle,
                new Animation("IdleLeft", assetService.LoadTexture(@"Content/Textures/Animations/HeroWalkLeft.png"), 16, 16, 0.25f, Vector2.Zero, Color.White, firstFrameOnly: true));
            RegisterAnimation(EntityHeading.Right, EntityState.Idle,
                new Animation("IdleRight", assetService.LoadTexture(@"Content/Textures/Animations/HeroWalkRight.png"), 16, 16, 0.25f, Vector2.Zero, Color.White, firstFrameOnly: true));

            RegisterAnimation(EntityHeading.Up, EntityState.Attacking,
                new Animation("AttackUp", assetService.LoadTexture(@"Content/Textures/Animations/HeroAttackUp.png"), 16, 24, 0.05f, Vector2.Zero, Color.White, yOffset: -4, loop: false, animationEndAction: () => 
                {
                    ResetAttack = true;
                    this.SetState(EntityState.Idle); 
                }));
            RegisterAnimation(EntityHeading.Down, EntityState.Attacking,
                new Animation("AttackDown", assetService.LoadTexture(@"Content/Textures/Animations/HeroAttackDown.png"), 16, 24, 0.05f, Vector2.Zero, Color.White, yOffset: -4, loop: false, animationEndAction: () => 
                {
                    ResetAttack = true;
                    this.SetState(EntityState.Idle); 
                }));
            RegisterAnimation(EntityHeading.Left, EntityState.Attacking,
                new Animation("AttackLeft", assetService.LoadTexture(@"Content/Textures/Animations/HeroAttackLeft.png"), 32, 16, 0.05f, Vector2.Zero, Color.White, xOffset: -8, loop: false, animationEndAction: () => 
                {
                    ResetAttack = true;
                    this.SetState(EntityState.Idle); 
                }));
            RegisterAnimation(EntityHeading.Right, EntityState.Attacking,
                new Animation("AttackRight", assetService.LoadTexture(@"Content/Textures/Animations/HeroAttackRight.png"), 32, 16, 0.05f, Vector2.Zero, Color.White, xOffset: -8, loop: false, animationEndAction: () => 
                {
                    ResetAttack = true;
                    this.SetState(EntityState.Idle);
                }));

            AttackSound = assetService.GameService.ContentManager.Load<SoundEffect>(@"SFX/Sword");

            base.Load(assetService);
        }

        public override void Update(GameTime gameTime) {

            var player = this;

            _healthTimer.Update(gameTime);

            base.Update(gameTime);
        }
    }
}
