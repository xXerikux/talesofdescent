﻿using MetavirusGames.Framework.Data;
using MetavirusGames.Framework.Entities.Interfaces;
using MetavirusGames.Framework.Graphics.Interfaces;
using MetavirusGames.TalesOfDescent.Services;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Entities {
    public sealed class BoneAttackEntity : ICombattant, IGameEntity, IRenderable, IMoveableEntity {

        public string Name {
            get { return "Bone Attack"; }
        }

        public Microsoft.Xna.Framework.Vector2 Position { get; private set; }

        public Vector2 MovementDelta { get; set; }

        public float MovementSpeed { get; set; }

        public Vector2 Velocity { get; private set; }

        public bool IsAlive { get; private set; }

        public Rectangle Bounds {
            get {
                return new Rectangle((int)Position.X + 4, (int)Position.Y + 4, 16 - 8, 16 - 8);
            }
        }

        public int AttackPower {
            get { return 1; }
        }

        private IGameEntity _parent;

        private Animation _animation;

        private Vector2 _velocity;

        public BoneAttackEntity(IGameEntity parent, Vector2 position, Vector2 velocity, string boneAnimationKey) {

            this._parent = parent;

            this.Position = position;

            this._velocity = velocity;

            this._animation = AnimationService.Instance.GetAnimation(boneAnimationKey);

            this.IsAlive = true;
        }

        public void Attack(ILivingEntity target) {

            ILivingEntity livingEntity = this._parent is ILivingEntity ? (target as ILivingEntity) : null;

            target.Damage(livingEntity);

        }

        public void Move(Microsoft.Xna.Framework.Vector2 amount) {
            Position += amount;
        }

        public void Draw(Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch) {

            this.Position += _velocity;

            this._animation.SetPosition(this.Position);

            this._animation.Update(gameTime);

            this._animation.Draw(gameTime, spriteBatch);

        }

        public void SetPosition(Vector2 value) {

        }

        public void SetVelocity(Vector2 value) {

        }

        public void Collide(IMoveableEntity collider) {
            IsAlive = false;
        }
    }
}
