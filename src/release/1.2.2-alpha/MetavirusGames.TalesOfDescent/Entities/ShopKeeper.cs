﻿using MetavirusGames.Framework.AI;
using MetavirusGames.Framework.Data;
using MetavirusGames.Framework.Entities;
using MetavirusGames.Framework.Runtime;
using MetavirusGames.Framework.Services;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Entities {
    public sealed class ShopKeeper  : Character {

        private const int MaxHealth = 15;

        private const int MaxStamina = 5;

        private const int BaseAttackPower = 3;

        public ShopKeeper(Vector2 position)
            : base("Shop Keeper", position, MaxHealth, MaxHealth, MaxStamina, MaxStamina, BaseAttackPower, 1, EntityState.Idle, EntityHeading.Down, true) {

        }

        public override void Load(IAssetService assetService) {

            RegisterAnimation(EntityHeading.Up, EntityState.Walking, new Animation("WalkUp", assetService.LoadTexture(@"Content/Textures/Animations/ShopKeepWalkUp.png"), 16, 16, 0.25f, Vector2.Zero, Color.White));
            RegisterAnimation(EntityHeading.Down, EntityState.Walking, new Animation("WalkDown", assetService.LoadTexture(@"Content/Textures/Animations/ShopKeepWalkDown.png"), 16, 16, 0.25f, Vector2.Zero, Color.White));
            RegisterAnimation(EntityHeading.Left, EntityState.Walking, new Animation("WalkLeft", assetService.LoadTexture(@"Content/Textures/Animations/ShopKeepWalkLeft.png"), 16, 16, 0.25f, Vector2.Zero, Color.White));
            RegisterAnimation(EntityHeading.Right, EntityState.Walking, new Animation("WalkRight", assetService.LoadTexture(@"Content/Textures/Animations/ShopKeepWalkRight.png"), 16, 16, 0.25f, Vector2.Zero, Color.White));

            RegisterAnimation(EntityHeading.Up, EntityState.Idle, new Animation("IdleUp", assetService.LoadTexture(@"Content/Textures/Animations/ShopKeepWalkUp.png"), 16, 16, 0.25f, Vector2.Zero, Color.White, firstFrameOnly: true));
            RegisterAnimation(EntityHeading.Down, EntityState.Idle, new Animation("IdleDown", assetService.LoadTexture(@"Content/Textures/Animations/ShopKeepWalkDown.png"), 16, 16, 0.25f, Vector2.Zero, Color.White, firstFrameOnly: true));
            RegisterAnimation(EntityHeading.Left, EntityState.Idle, new Animation("IdleLeft", assetService.LoadTexture(@"Content/Textures/Animations/ShopKeepWalkLeft.png"), 16, 16, 0.25f, Vector2.Zero, Color.White, firstFrameOnly: true));
            RegisterAnimation(EntityHeading.Right, EntityState.Idle, new Animation("IdleRight", assetService.LoadTexture(@"Content/Textures/Animations/ShopKeepWalkRight.png"), 16, 16, 0.25f, Vector2.Zero, Color.White, firstFrameOnly: true));

            //SetBehavior(new RandomMovementBehavior(this, 3f, 50));

            base.Load(assetService);
        }

        public override void Update(GameTime gameTime) {

            if (this.IsAlive) {

            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch) {

            if (this.IsAlive) {
            }

            base.Draw(gameTime, spriteBatch);
        }

        public override void Collide(Framework.Entities.Interfaces.IMoveableEntity collider) {

            if (collider is Character) {
                Character character = (collider as Character);

                float xDistance = this.Position.X - character.Position.X;
                float yDistance = this.Position.Y - character.Position.Y;

                if (Math.Abs(xDistance) > Math.Abs(yDistance)) {
                    if (character.Position.X < this.Position.X) {
                        this.SetHeading(EntityHeading.Left);
                    } if (character.Position.X >= this.Position.X) {
                        this.SetHeading(EntityHeading.Right);
                    }
                } else {
                    if (character.Position.Y < this.Position.Y) {
                        this.SetHeading(EntityHeading.Up);
                    } if (character.Position.Y >= this.Position.Y) {
                        this.SetHeading(EntityHeading.Down);
                    }
                }



            }

            base.Collide(collider);
        }

        public override void Damage(Framework.Entities.Interfaces.ILivingEntity attacker) {

            //if (attacker is Character) {
            //    Character character = (attacker as Character);

            //    float xDistance = this.Position.X - character.Position.X;
            //    float yDistance = this.Position.Y - character.Position.Y;

            //    if (Math.Abs(xDistance) > Math.Abs(yDistance)) {
            //        if (character.Position.X < this.Position.X) {
            //            this.SetHeading(EntityHeading.Left);
            //        } if (character.Position.X >= this.Position.X) {
            //            this.SetHeading(EntityHeading.Right);
            //        }
            //    } else {
            //        if (character.Position.Y < this.Position.Y) {
            //            this.SetHeading(EntityHeading.Up);
            //        } if (character.Position.Y >= this.Position.Y) {
            //            this.SetHeading(EntityHeading.Down);
            //        }
            //    }



            //}

            //base.Damage(attacker);
        }
    }
}
