﻿using MetavirusGames.Framework.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Entities {
    public sealed class BoneCharm : Item {

        public BoneCharm(Texture2D texture, Vector2 position, bool canPickup)
            : base("Bone Charm", texture, position, new Rectangle(0, 16, 16, 16), 1) {

                CanPickup = canPickup;
        }
    }
}
