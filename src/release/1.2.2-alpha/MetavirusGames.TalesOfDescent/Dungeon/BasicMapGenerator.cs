﻿using MetavirusGames.Framework.AI;
using MetavirusGames.Framework.Graphics;
using MetavirusGames.Framework.Services;
using MetavirusGames.TalesOfDescent.Dungeon.Interfaces;
using MetavirusGames.TalesOfDescent.Services;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Dungeon {
    public sealed class BasicMapGenerator : IMapGenerator {

        private Difficulty _difficulty;

        private RoomService _roomService;

        private IList<Room> _roomPool;

        private IList<Vector2> _visitedPositions;

        private static Random Random = new Random(new Random().Next(10000));

        public BasicMapGenerator(RoomService roomService) {
            this._roomService = roomService;

            this._roomPool = this._roomService.GetAll();

            this._visitedPositions = new List<Vector2>();
        }

        public Map Generate(Difficulty difficulty, IGameService gameService) {

            _difficulty = difficulty;

            bool succesfulGeneration = false;

            while (!succesfulGeneration) {

                bool bossKeyRoomDefined = false;

                Point mapDimensions = GetRandomDimensions();

                Map map = new Map();

                map.Width = mapDimensions.X;

                map.Height = mapDimensions.Y;

                map.Rooms = new Room[map.Width, map.Height];

                //Get the random horizontal position for the starting room
                int startingRoomX = Random.Next(2, map.Width - 2);

                map.SpawnRoomPosition = new Point(startingRoomX, map.Height - 1);

                //Build the exit room positions
                int exitRoomX = Random.Next(2, map.Width - 2);

                int exitRoomY = Random.Next(0, (int)((float)(map.Height) * 0.25f));

                map.ExitRoomPosition = new Point(exitRoomX, exitRoomY);

                //Get a list of candidate rooms for the dungeon entrance
                IList<Room> startingRoomCandidates = this._roomPool.Where(r => r.TotalPaths > 1 && r.DownPath == true).ToList<Room>();

                Room startingRoom = startingRoomCandidates[Random.Next(0, startingRoomCandidates.Count())];

                map.Rooms[map.SpawnRoomPosition.X, map.SpawnRoomPosition.Y] = startingRoom;

                IList<Room> exitRoomCandidates = this._roomPool.Where(r => r.TotalPaths == 1).ToList<Room>();

                Room exitRoom = exitRoomCandidates[Random.Next(0, exitRoomCandidates.Count())];

                map.Rooms[map.ExitRoomPosition.X, map.ExitRoomPosition.Y] = exitRoom;

                List<PathfindingTile> tiles = new List<PathfindingTile>();

                int id = 0;

                for (int y = 0; y < map.Height; y++) {
                    for (int x = 0; x < map.Width; x++) {
                        PathfindingTile tile = new PathfindingTile(new Vector2(x, y), gameService.Graphics.GraphicsDevice, id);
                        if (x == map.SpawnRoomPosition.X && y == map.SpawnRoomPosition.Y) {
                            tile.Type = 1;
                        } else if (x == map.ExitRoomPosition.X && y == map.ExitRoomPosition.Y) {
                            tile.Type = 2;
                        } else if (x == map.SpawnRoomPosition.X && y == map.SpawnRoomPosition.Y - 2) {
                            tile.Type = 3;
                        } else if (x == map.ExitRoomPosition.X && y == map.ExitRoomPosition.Y + 2) {
                            tile.Type = 3;
                        } else {
                            int wallAddChance = Random.Next(0, 100);

                            if (wallAddChance <= 10) {
                                tile.Type = 3;
                            }
                        }

                        tiles.Add(tile);
                        id++;
                    }
                }


                AStarSearch aStar = new AStarSearch(tiles, new Vector2(map.Width, map.Height));

                List<PathfindingTile> searchedTiles = aStar.SearchPath();

                int tileIterator = 0;
                int[,] outcome = new int[map.Width, map.Height];

                StringBuilder sb = new StringBuilder();

                string line = String.Empty;

                for (int y = 0; y < map.Height; y++) {

                    if (line != String.Empty) {
                        sb.AppendLine(line);
                        //Console.WriteLine(line);
                        line = String.Empty;
                    }

                    for (int x = 0; x < map.Width; x++) {
                        line += searchedTiles[tileIterator].Type.ToString();
                        outcome[x, y] = searchedTiles[tileIterator].Type;
                        tileIterator++;
                    }
                }

                for (int y = 0; y < map.Height; y++) {
                    for (int x = 0; x < map.Width; x++) {

                        //if (!(x == map.SpawnRoomPosition.X && y == map.SpawnRoomPosition.Y)) {
                        if (outcome[x, y] == 4) {
                            //Vector2 nextRoomPosition = GetNextRoomPosition(map, new Vector2(x, y));
                            map.Rooms[x, y] = GetFourWayRoom();
                        }
                        //}
                    }
                }

                map.Rooms[map.SpawnRoomPosition.X, map.SpawnRoomPosition.Y] = GetStartingRoom(map);
                map.Rooms[map.ExitRoomPosition.X, map.ExitRoomPosition.Y] = GetExitRoom(map);

                IAssetService assetService = new AssetService(gameService);

                Vector2 lightPosition = new Vector2
                    (
                    map.ExitRoomPosition.X * (float)Room.MaxWidth * 16 + ((float)(Room.MaxWidth * 16) / 2) - (16 / 2),
                    map.ExitRoomPosition.Y * (float)Room.MaxHeight * 16 + ((float)(Room.MaxHeight * 16) / 2) - (16 / 2)
                    );

                Vector2 lightPosition2 = new Vector2
                    (
                    map.SpawnRoomPosition.X * (float)Room.MaxWidth * 16 + ((float)(Room.MaxWidth * 16) / 2) - (16 / 2),
                    map.SpawnRoomPosition.Y * (float)Room.MaxHeight * 16 + ((float)(Room.MaxHeight * 16) / 2) - (16 / 2)
                    );

                Light light = new Light(assetService.LoadTexture(@"Content/Textures/effect_lightmap.png"), lightPosition.X, lightPosition.Y, 64, 64, 0f, 4f, new Color(242, 20, 20, 200));
                map.LightPositions.Add(light);

                Light light2 = new Light(assetService.LoadTexture(@"Content/Textures/effect_lightmap.png"), lightPosition2.X, lightPosition2.Y, 64, 64, 0f, 4f, new Color(100, 100, 242, 200));
                map.LightPositions.Add(light2);

                for (int y = 0; y < map.Height; y++) {
                    for (int x = 0; x < map.Width; x++) {

                        if (map.Rooms[x, y] == null) {
                            map.Rooms[x, y] = GetCandidateRoom(map, new Vector2(x, y));
                        }

                    }
                }

                for (int y = 0; y < map.Height; y++) {
                    for (int x = 0; x < map.Width; x++) {

                        if (map.Rooms[x, y] != null && map.Rooms[x, y].TotalPaths < 4) {
                            int removeRoomChance = Random.Next(0, 100);

                            Point currentPoint = new Point(x, y);

                            if (currentPoint != map.SpawnRoomPosition && currentPoint != map.ExitRoomPosition) {
                                if (removeRoomChance < 75) {
                                    map.Rooms[x, y] = null;
                                }
                            }
                        }

                    }
                }

                for (int y = 0; y < map.Height; y++) {
                    for (int x = 0; x < map.Width; x++) {

                        if (map.Rooms[x, y] != null) {
                            int corridorChance = Random.Next(0, 100);

                            bool corridor = corridorChance < 25 ? true : false;

                            map.Rooms[x, y] = GetCandidateRoom(map, new Vector2(x, y));

                            if (map.SpawnRoomPosition != new Point(x, y)) {
                                int defeatAllChance = Random.Next(0, 100);

                                if (defeatAllChance < 50) {
                                    map.Rooms[x, y].MustDefaultAllToLeave = true;
                                }
                            }
                        }

                    }
                }

                map.Rooms[map.SpawnRoomPosition.X, map.SpawnRoomPosition.Y] = GetStartingRoom(map);
                map.Rooms[map.ExitRoomPosition.X, map.ExitRoomPosition.Y] = GetExitRoom(map);

                int validRoomCount = 0;

                foreach (Room room in map.Rooms) {
                    if (room != null) {
                        validRoomCount++;
                    }
                }

                if (validRoomCount > 2) {

                    //Time to set a boss key room
                    Vector2 exitPosition = new Vector2(map.ExitRoomPosition.X, map.ExitRoomPosition.Y);

                    Dictionary<Vector2, Room> candidateBossKeyRooms = new Dictionary<Vector2, Room>();

                    for (int y = 0; y < map.Height; y++) {
                        for (int x = 0; x < map.Width; x++) {

                            float distance = Vector2.Distance(exitPosition, new Vector2(x, y));

                            if (distance < 3) {

                                if (exitPosition != new Vector2(x, y)) {

                                    if (map.Rooms[x, y] != null) {
                                        candidateBossKeyRooms.Add(new Vector2(x, y), map.Rooms[x, y]);
                                    }
                                }

                            }

                        }
                    }

                    Room bossKeyRoom = candidateBossKeyRooms.Values.ToArray().ToList<Room>()[Random.Next(0, candidateBossKeyRooms.Values.Count)];

                    foreach (var kvp in candidateBossKeyRooms) {

                        if (bossKeyRoom == kvp.Value) {
                            Vector2 bossKeyLightPosition = new Vector2
                       (
                       kvp.Key.X * (float)Room.MaxWidth * 16 + ((float)(Room.MaxWidth * 16) / 2) - (16 / 2),
                       kvp.Key.Y * (float)Room.MaxHeight * 16 + ((float)(Room.MaxHeight * 16) / 2) - (16 / 2)
                       );

                            kvp.Value.BossKey = true;
                            //Light bossKeyLight = new Light(assetService.LoadTexture(@"Content/Textures/effect_lightmap.png"), bossKeyLightPosition.X, bossKeyLightPosition.Y, 64, 64, 0f, 4f, new Color(Color.Goldenrod.R, Color.Goldenrod.G, Color.Goldenrod.B, 200));
                            //map.LightPositions.Add(bossKeyLight);

                            break;
                        }
                    }


                    return map;
                }
            }

            return null;
        }

        private Point GetRandomDimensions() {
            int difficultyFactor = (int)(this._difficulty + 1) * 15;

            Random random = new Random(new Random(1000).Next(10000));

            int x = random.Next(7, difficultyFactor);

            int y = random.Next(5, difficultyFactor);

            Point result = new Point(x, y);

            return result;
        }

        private Room GetStartingRoom(Map map) {
            return GetCandidateRoom(map, new Vector2(map.SpawnRoomPosition.X, map.SpawnRoomPosition.Y));
        }

        private Room GetExitRoom(Map map) {
            return GetCandidateRoom(map, new Vector2(map.ExitRoomPosition.X, map.ExitRoomPosition.Y), true);
        }

        private Room GetFourWayRoom() {
            Room room = this._roomPool.FirstOrDefault(r => r.UpPath == true && r.DownPath == true && r.LeftPath == true && r.RightPath == true);

            if (room == null) {
                return null;
            }

            return room;
        }

        private Vector2 GetNextRoomPosition(Map map, Vector2 position) {

            Vector2 upPosition = new Vector2(position.X, position.Y - 1);
            Vector2 downPosition = new Vector2(position.X, position.Y + 1);
            Vector2 leftPosition = new Vector2(position.X - 1, position.Y);
            Vector2 rightPosition = new Vector2(position.X + 1, position.Y);

            Room upRoom = null;

            if (!this._visitedPositions.Contains(upPosition)) {
                upRoom = GetRoomAtPosition(map, upPosition);
            }

            Room downRoom = null;

            if (!this._visitedPositions.Contains(downPosition)) {
                downRoom = GetRoomAtPosition(map, downPosition);
            }

            Room leftRoom = null;

            if (!this._visitedPositions.Contains(leftPosition)) {
                leftRoom = GetRoomAtPosition(map, leftPosition);
            }

            Room rightRoom = null;

            if (!this._visitedPositions.Contains(rightPosition)) {
                rightRoom = GetRoomAtPosition(map, rightPosition);
            }


            if (upRoom != null) {
                return upPosition;
            }

            if (downRoom != null) {
                return downPosition;
            }

            if (leftRoom != null) {
                return leftPosition;
            }

            if (rightRoom != null) {
                return rightPosition;
            }

            throw new Exception();
        }

        private Room GetCandidateRoom(Map map, Vector2 position, bool exitRoom = false) {
            Func<Room, bool> expression = GetRuleExpression(map, position);

            if (expression != null) {
                IList<Room> candidateRooms = null;

                if (exitRoom) {
                    candidateRooms = this._roomService.ExitRoomCandidates.Where(expression).ToList<Room>();
                } else {
                    candidateRooms = this._roomPool.Where(expression).ToList<Room>();
                }

                if (candidateRooms.Count() > 0) {
                    int roomId = Random.Next(0, candidateRooms.Count());

                    Room room = null;

                    room = candidateRooms[roomId];

                    return room;
                }

                return this._roomPool[0];
            } else {
                return null;
            }
        }

        /// <summary>
        /// Takes in a map position, and based on the adjacent rooms, will
        /// construct an expression to help find a good candidate room for
        /// placement
        /// </summary>
        /// <param name="map"></param>
        /// <param name="position"></param>
        /// <returns></returns>
        private Func<Room, bool> GetRuleExpression(Map map, Vector2 position) {

            if (position.X >= 0 && position.X < map.Width &&
                position.Y >= 0 && position.Y < map.Height) {

                bool up = false;
                bool down = false;
                bool left = false;
                bool right = false;

                Room upRoom = GetRoomAtPosition(map, new Vector2(position.X, position.Y - 1));
                Room downRoom = GetRoomAtPosition(map, new Vector2(position.X, position.Y + 1));
                Room leftRoom = GetRoomAtPosition(map, new Vector2(position.X - 1, position.Y));
                Room rightRoom = GetRoomAtPosition(map, new Vector2(position.X + 1, position.Y));

                if (upRoom != null && upRoom.DownPath && position.Y >= 0 && position.Y < map.Height) {
                    up = true;
                }

                if (downRoom != null && downRoom.UpPath && position.Y >= 0 && position.Y < map.Height) {
                    down = true;
                }

                if (leftRoom != null && leftRoom.RightPath && position.X >= 0 && position.X < map.Width) {
                    left = true;
                }

                if (rightRoom != null && rightRoom.LeftPath && position.X >= 0 && position.X < map.Width) {
                    right = true;
                }

                bool corridor = false;

                int corridorChance = Random.Next(0, 100);

                if (corridorChance <= 10) {
                    corridor = true;
                }


                Func<Room, bool> result = new Func<Room, bool>(r => r.LeftPath == left && r.RightPath == right && r.DownPath == down && r.UpPath == up);

                if (up || down || left || right) {
                    return result;
                } else {
                    return null;
                }

            } else {
                throw new Exception("A candidate position must be within bounds");
            }
        }

        /// <summary>
        /// Returns a room at the specified location, if the 
        /// room is out of the map bounds, this will return null
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        private Room GetRoomAtPosition(Map map, Vector2 position) {

            try {
                return map.Rooms[(int)position.X, (int)position.Y];
            } catch (Exception ex) {
                return null;
            }
        }
    }
}
