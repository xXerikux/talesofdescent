﻿using MetavirusGames.Framework.Entities;
using MetavirusGames.Framework.Entities.Interfaces;
using MetavirusGames.Framework.Graphics;
using MetavirusGames.TalesOfDescent.Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Dungeon {
    public sealed class Map {

        public Room[,] Rooms { get; set; }

        public Point SpawnRoomPosition { get; set; }

        public Point ExitRoomPosition { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public IList<Item> Items { get; private set; }

        public IList<Light> LightPositions { get; set; }

        public IBoss Boss { get; set; }

        public void RemoveItem(Item item) {

            if (Items.Contains(item)) {
                Items.RemoveAt(Items.IndexOf(item));
            }

        }

        public void AddItem(Item item) {

            Items.Add(item);

        }

        public Room GetRoomOfEntity(Tileset tileset, IMoveableEntity entity) {

            for (int mapY = 0; mapY < this.Height; mapY++) {
                for (int mapX = 0; mapX < this.Width; mapX++) {

                    Color renderColor = Color.White;

                    //if (mapX == this.SpawnRoomPosition.X && mapY == this.SpawnRoomPosition.Y) {
                    //    renderColor = ColorHelper.CreateColor(Color.LightBlue, 50f);
                    //}
                    //if (mapX == this.ExitRoomPosition.X && mapY == this.ExitRoomPosition.Y) {
                    //    renderColor = ColorHelper.CreateColor(Color.Red, 50f);
                    //}
                    Rectangle roomBounds = new Rectangle
                                (
                                mapX * Room.MaxWidth * tileset.TileWidth,
                                mapY * Room.MaxHeight * tileset.TileHeight,
                                Room.MaxWidth * tileset.TileWidth,
                                Room.MaxHeight * tileset.TileHeight
                                );


                    bool playerInRoom = roomBounds.Intersects(entity.Bounds);

                    if (playerInRoom) {
                        return Rooms[mapX, mapY];
                    }

                }
            }

            return null;
        }

        public Room GetDeflatedRoomOfEntity(Tileset tileset, IMoveableEntity entity) {

            for (int mapY = 0; mapY < this.Height; mapY++) {
                for (int mapX = 0; mapX < this.Width; mapX++) {

                    Color renderColor = Color.White;

                    //if (mapX == this.SpawnRoomPosition.X && mapY == this.SpawnRoomPosition.Y) {
                    //    renderColor = ColorHelper.CreateColor(Color.LightBlue, 50f);
                    //}
                    //if (mapX == this.ExitRoomPosition.X && mapY == this.ExitRoomPosition.Y) {
                    //    renderColor = ColorHelper.CreateColor(Color.Red, 50f);
                    //}
                    Rectangle roomBounds = new Rectangle
                                (
                                (mapX * Room.MaxWidth * tileset.TileWidth) + 32,
                                (mapY * Room.MaxHeight * tileset.TileHeight) + 32,
                                (Room.MaxWidth * tileset.TileWidth) - 64,
                                (Room.MaxHeight * tileset.TileHeight) - 64
                                );


                    bool playerInRoom = roomBounds.Intersects(entity.Bounds);

                    if (playerInRoom) {
                        Room room = Rooms[mapX, mapY];

                        if (room.MustDefaultAllToLeave) {
                            int chair = 1;
                        }
                    }

                    if (playerInRoom) {
                        return Rooms[mapX, mapY];
                    }

                }
            }

            return null;
        }

        public Vector2 GetRoomPositionOfEntity(Tileset tileset, IMoveableEntity entity) {

            for (int mapY = 0; mapY < this.Height; mapY++) {
                for (int mapX = 0; mapX < this.Width; mapX++) {

                    Rectangle roomBounds = new Rectangle
                                (
                                mapX * Room.MaxWidth * tileset.TileWidth,
                                mapY * Room.MaxHeight * tileset.TileHeight,
                                Room.MaxWidth * tileset.TileWidth,
                                Room.MaxHeight * tileset.TileHeight
                                );


                    bool playerInRoom = roomBounds.Intersects(entity.Bounds);

                    if (playerInRoom) {
                        return new Vector2(mapX, mapY);
                    }

                }
            }

            return Vector2.Zero;
        }

        public Map() {
            
            this.Items = new List<Item>();

            this.LightPositions = new List<Light>();

        }
    }
}
