﻿using MetavirusGames.Framework.Graphics;
using MetavirusGames.Framework.Services;
using MetavirusGames.TalesOfDescent.Dungeon.Interfaces;
using MetavirusGames.TalesOfDescent.Graphics;
using MetavirusGames.TalesOfDescent.Services;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Dungeon {
    public sealed class ShopMapGenerator : IMapGenerator {

        public Map Generate(Difficulty difficulty, Framework.Services.IGameService gameService) {

            IAssetService assetService = new AssetService(gameService);

            RoomService roomService = new RoomService(assetService);

            Map map = new Map();

            map.Rooms = new Room[1, 1];

            map.SpawnRoomPosition = new Microsoft.Xna.Framework.Point(0, 0);

            map.ExitRoomPosition = new Microsoft.Xna.Framework.Point(0, 0);

            map.Width = 1;

            map.Height = 1;

            Room room = roomService.GetSpawnRoom();

            map.Rooms[0, 0] = room;

            Light light = new Light(assetService.LoadTexture(@"Content/Textures/effect_lightmap.png"), 0, 0, 64, 64, 0f, 3f, new Color(242, 228, 157, 200));
            Light light2 = new Light(assetService.LoadTexture(@"Content/Textures/effect_lightmap.png"), 19 * 16, 0, 64, 64, 0f, 3f, new Color(242, 228, 157, 200));
            Light light3 = new Light(assetService.LoadTexture(@"Content/Textures/effect_lightmap.png"), 0, 17 * 16, 64, 64, 0f, 3f, new Color(242, 228, 157, 200));
            Light light4 = new Light(assetService.LoadTexture(@"Content/Textures/effect_lightmap.png"), 19 * 16, 17 * 16, 64, 64, 0f, 3f, new Color(242, 228, 157, 200));


            map.LightPositions.Add(light);
            map.LightPositions.Add(light2);
            map.LightPositions.Add(light3);
            map.LightPositions.Add(light4);
            return map;
        }

    }
}
