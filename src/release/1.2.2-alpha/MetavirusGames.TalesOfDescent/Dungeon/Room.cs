﻿using MetavirusGames.Framework.Entities;
using MetavirusGames.Framework.Runtime;
using MetavirusGames.TalesOfDescent.Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Dungeon {
    public class Room {

        public const int MaxWidth = 19;

        public const int MaxHeight = 17;

        private Random _random = new Random(new Random(100000).Next());

        public List<Character> Characters { get; private set; }

        public bool MustDefaultAllToLeave { get; set; }

        public bool BossKey { get; set; }

        public bool IsCorridor {
            get {
                float floorCount = 0;
                float totalCells = MaxWidth * MaxHeight;

                foreach (int value in Data) {
                    if (value == 1 || value == 6) {
                        floorCount++;
                    }
                }

                float percentFlooring = floorCount / totalCells;

                if (percentFlooring > 0.75) {
                    return false;
                } else {
                    return true;
                }
            }
        }

        public int TotalPaths {
            get {
                int result = 0;

                if (LeftPath) result++;
                if (RightPath) result++;
                if (UpPath) result++;
                if (DownPath) result++;

                return result;
            }
        }

        public bool LeftPath {
            get {
                return RowContainsValue(0);
            }
        }

        public bool RightPath {
            get {
                return RowContainsValue(Room.MaxWidth - 1);
            }
        }

        public bool UpPath {
            get {
                return ColumnContainsValue(0);
            }
        }

        public bool DownPath {
            get {
                return ColumnContainsValue(Room.MaxHeight - 1);
            }
        }

        public int[,] Data { get; private set; }

        public List<Rectangle> Warps { get; private set; }

        private List<Vector2> openPositions;

        private Room() { }

        public Room(int[,] data) {

            this.Data = data;

            this.Warps = new List<Rectangle>();

            this.Characters = new List<Character>();

        }

        public void SetData(int[,] data) {
            this.Data = data;
        }

        public Rectangle GetBounds(Tileset tileset, int x, int y) {
            return new Rectangle(x * tileset.TileWidth, y * tileset.TileHeight, tileset.TileWidth, tileset.TileHeight);
        }

        public bool IsObstacle(int x, int y) {

            if (x >= 0 && x < MaxWidth && y >= 0 && y < MaxHeight) {
                return (Data[x, y] == 0 || Data[x, y] == 7);
            }

            return false;
        }

        public Vector2 GetIntersectionDepth(Tileset tileset, int x, int y, Rectangle entityBounds, Axis axis) {

            Vector2 depth = (axis == Axis.Vertical) ?
                new Vector2(0, entityBounds.GetVerticalIntersectionDepth(GetBounds(tileset, x, y))) :
                new Vector2(entityBounds.GetHorizontalIntersectionDepth(GetBounds(tileset, x, y)));

            return depth;
        }

        public bool TileIntersectsBounds(Rectangle player, Rectangle block, Axis direction, out Vector2 depth) {
            depth = direction == Axis.Vertical ? new Vector2(0, player.GetVerticalIntersectionDepth(block)) : new Vector2(player.GetHorizontalIntersectionDepth(block), 0);


            return depth.Y != 0 || depth.X != 0;
        }

        public List<Vector2> GetOpenPositions() {

            if (openPositions == null) {

                openPositions = new List<Vector2>();

                for (int y = 0; y < this.Data.GetLength(1); y++) {
                    for (int x = 0; x < this.Data.GetLength(0); x++) {
                        if (this.Data[x, y] == 1 || this.Data[x, y] == 6) {

                            openPositions.Add(new Vector2(x, y));
                        }
                    }
                }

            }

            return openPositions;
        }

        public Vector2 GetRandomOpenPosition() {

            List<Vector2> openPositions = GetOpenPositions();

            Vector2 result = openPositions[_random.Next(0, openPositions.Count - 1)];

            openPositions.Remove(result);

            return result;
        }

        public void AddWarp(Rectangle rectangle) {

            Warps.Add(rectangle);

        }

        private bool ColumnContainsValue(int y) {

            for (int x = 0; x < this.Data.GetLength(0); x++) {
                if (this.Data[x, y] > 0) {
                    return true;
                }
            }

            return false;
        }

        private bool RowContainsValue(int x) {

            for (int y = 0; y < this.Data.GetLength(1); y++) {
                if (this.Data[x, y] > 0) {
                    return true;
                }
            }

            return false;
        }


    }
}
