﻿#region Using Statements
using MetavirusGames.Framework.Audio;
using MetavirusGames.Framework.Runtime;
using MetavirusGames.Framework.ScreenManager;
using MetavirusGames.Framework.Services;
using MetavirusGames.TalesOfDescent.Screens;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
#endregion

namespace MetavirusGames.TalesOfDescent {
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class TalesOfDescentGame : Game, IGameService {

        public GraphicsDeviceManager Graphics { get; private set; }

        public ContentManager ContentManager {
            get {
                return this.Content;
            }
        }

        internal MetavirusGames.Framework.Settings Settings { get; private set; }

        private ScreenManager screenManager;

        static readonly string[] preloadAssets =
        {
            "gradient",
        };

        public TalesOfDescentGame()
            : this(new MetavirusGames.Framework.Settings() { ScreenWidth = 800, ScreenHeight = 600, FullScreen = false }) {
        }

        public TalesOfDescentGame(MetavirusGames.Framework.Settings settings)
            : base() {

            this.Settings = settings;

            this.Graphics = new GraphicsDeviceManager(this) {
                PreferredBackBufferWidth = settings.ScreenWidth,
                PreferredBackBufferHeight = settings.ScreenHeight,
                IsFullScreen = settings.FullScreen,
                GraphicsProfile = Microsoft.Xna.Framework.Graphics.GraphicsProfile.HiDef
            };

            this.Content.RootDirectory = "Content";

            screenManager = new ScreenManager(this);

            Components.Add(screenManager);

            // Activate the first screens.
            screenManager.AddScreen(new BackgroundScreen(), PlayerIndex.One);
            screenManager.AddScreen(new MainMenuScreen(), PlayerIndex.One);

            Music.Volume = 0.1f;

            Music.Initialize(new AssetService(this));

        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent() {

            foreach (string asset in preloadAssets) {
                Content.Load<object>(asset);
            }

            //Player.Load(this.AssetsService);
            //Skeleton.Load(this.AssetsService);
        }

    }
}
