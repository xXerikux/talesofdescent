﻿using MetavirusGames.Framework.Audio;
using MetavirusGames.Framework.Entities;
using MetavirusGames.Framework.Entities.Effects;
using MetavirusGames.Framework.Entities.Interfaces;
using MetavirusGames.Framework.Services;
using MetavirusGames.TalesOfDescent.Screens;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Services {
    public class CharacterControllerService {

        private Character _character;

        private KeyboardState _previousKeyboard;

        private IGameService _gameService;

        public CharacterControllerService(Character entity, IGameService gameService) {

            this._character = entity;

            this._gameService = gameService;

        }

        public void Update() {
            KeyboardState keyboard = Keyboard.GetState();
            Vector2 moveAmount = new Vector2();

            this._character.ResetMovement();

            if (keyboard.IsKeyDown(Keys.Up)) {
                moveAmount = new Vector2(0, -1);
                this._character.SetHeading(EntityHeading.Up);

                if (this._character.State != EntityState.Attacking)
                    this._character.SetState(EntityState.Walking);
            } if (keyboard.IsKeyDown(Keys.Down)) {
                moveAmount = new Vector2(0, 1);
                this._character.SetHeading(EntityHeading.Down);
                if (this._character.State != EntityState.Attacking)
                    this._character.SetState(EntityState.Walking);
            } if (keyboard.IsKeyDown(Keys.Left)) {
                moveAmount = new Vector2(-1, moveAmount.Y);
                this._character.SetHeading(EntityHeading.Left);
                if (this._character.State != EntityState.Attacking)
                    this._character.SetState(EntityState.Walking);
            } if (keyboard.IsKeyDown(Keys.Right)) {
                moveAmount = new Vector2(1, moveAmount.Y);
                this._character.SetHeading(EntityHeading.Right);
                if (this._character.State != EntityState.Attacking)
                    this._character.SetState(EntityState.Walking);
            }

            if (keyboard.IsKeyDown(Keys.X) && !this._previousKeyboard.IsKeyDown(Keys.X)) {

                if (this._character.Stamina > 0) {

                    this._character.UseStamina();

                    this._character.SetState(EntityState.Attacking);

                    if (this._character.AttackSound != null) {
                        this._character.AttackSound.Play(Music.Volume, 0f, 0f);
                    }
                }
            }

            if (keyboard.IsKeyDown(Keys.S) && !this._previousKeyboard.IsKeyDown(Keys.S)) {

                if (GameplayScreen.PlayerInventory.HealthPotions > 0) {
                    GameplayScreen.PlayerInventory.HealthPotions -= 1;
                    this._character.SetEffect(new HealOverTime(5, this._character, this._gameService.ContentManager.Load<SoundEffect>(@"SFX/heal")));
                }
            }

            if (keyboard.IsKeyDown(Keys.D) && !this._previousKeyboard.IsKeyDown(Keys.D)) {

                if (GameplayScreen.PlayerInventory.StaminaPotions > 0) {
                    GameplayScreen.PlayerInventory.StaminaPotions -= 1;
                    this._character.SetEffect(new StaminaOverTime(5, this._character, this._gameService.ContentManager.Load<SoundEffect>(@"SFX/heal")));
                }
            }

            this._character.Move(moveAmount);

            if (this._character.Velocity == Vector2.Zero) {
                if (this._character.State != EntityState.Attacking) {
                    this._character.SetState(EntityState.Idle);
                }
            }

            this._previousKeyboard = keyboard;
        }
    }
}
