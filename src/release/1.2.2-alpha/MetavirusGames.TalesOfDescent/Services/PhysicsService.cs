﻿using MetavirusGames.Framework.Entities;
using MetavirusGames.Framework.Entities.Interfaces;
using MetavirusGames.Framework.Runtime;
using MetavirusGames.TalesOfDescent.Dungeon;
using MetavirusGames.TalesOfDescent.Entities;
using MetavirusGames.TalesOfDescent.Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Services {
    public sealed class PhysicsService {

        public void ResolveCollision(IMoveableEntity moveableEntity, IMoveableEntity targetEntity, Map map, Tileset tileset, Axis axis) {
            Vector2 roomLocation = map.GetRoomPositionOfEntity(tileset, moveableEntity);
            Room currentRoom = map.GetRoomOfEntity(tileset, moveableEntity);
            Room targetEntityRoom = map.GetRoomOfEntity(tileset, targetEntity);
            Vector2 targetRoomLocation = map.GetRoomPositionOfEntity(tileset, targetEntity);

            if (roomLocation == targetRoomLocation) {
                Rectangle playerBounds = new Rectangle
                    (moveableEntity.Bounds.X - (int)(roomLocation.X * Room.MaxWidth * tileset.TileWidth), 
                    moveableEntity.Bounds.Y - (int)(roomLocation.Y * Room.MaxHeight * tileset.TileHeight), 
                    moveableEntity.Bounds.Width, 
                    moveableEntity.Bounds.Height);

                Rectangle targetBounds = new Rectangle
                    (targetEntity.Bounds.X - (int)(targetRoomLocation.X * Room.MaxWidth * tileset.TileWidth), 
                    targetEntity.Bounds.Y - (int)(targetRoomLocation.Y * Room.MaxHeight * tileset.TileHeight), 
                    targetEntity.Bounds.Width, 
                    targetEntity.Bounds.Height);

                Vector2 depth = Vector2.Zero;

                if (currentRoom.TileIntersectsBounds(playerBounds, targetBounds, axis, out depth)) {

                    //Debug.Print(depth.ToString());

                    if (depth.Y != 0) {
                        moveableEntity.SetVelocity(new Vector2(moveableEntity.Velocity.X, 0));
                    }

                    if (depth.X != 0) {
                        moveableEntity.SetVelocity(new Vector2(0, moveableEntity.Velocity.Y));
                    }

                    moveableEntity.SetPosition(Vector2.Add(moveableEntity.Position, depth));

                    playerBounds = new Rectangle(moveableEntity.Bounds.X - (int)(roomLocation.X * Room.MaxWidth * tileset.TileWidth), moveableEntity.Bounds.Y - (int)(roomLocation.Y * Room.MaxHeight * tileset.TileHeight), moveableEntity.Bounds.Width, moveableEntity.Bounds.Height);

                }
            }
        }

        public void ResolveCollision(IMoveableEntity moveableEntity, Tileset tileset, Map map, Axis axis) {

            Vector2 roomLocation = map.GetRoomPositionOfEntity(tileset, moveableEntity);
            Room currentRoom = map.GetRoomOfEntity(tileset, moveableEntity);
            Rectangle playerBounds = new Rectangle(moveableEntity.Bounds.X - (int)(roomLocation.X * Room.MaxWidth * tileset.TileWidth), moveableEntity.Bounds.Y - (int)(roomLocation.Y * Room.MaxHeight * tileset.TileHeight), moveableEntity.Bounds.Width, moveableEntity.Bounds.Height);

            int leftTile = playerBounds.Left / 16;
            int topTile = playerBounds.Top / 16;
            int rightTile = (int)Math.Ceiling((float)playerBounds.Right / 16) - 1;
            int bottomTile = (int)Math.Ceiling(((float)playerBounds.Bottom / 16)) - 1;

            if (currentRoom != null) {
                for (int y = topTile; y <= bottomTile; ++y) {
                    for (int x = leftTile; x <= rightTile; ++x) {
                        Vector2 depth;

                        bool roomIsObstacle = currentRoom.IsObstacle(x, y);

                        if (moveableEntity is Character) {
                            Character character = (moveableEntity as Character);

                            if (character.StuckInCurrentRoom) {
                                if (x == 0 || x == Room.MaxWidth - 1 || y == 0 || y == Room.MaxHeight - 1) {
                                    roomIsObstacle = true;
                                }
                            }
                        }

                        

                        if (roomIsObstacle && currentRoom.TileIntersectsBounds(playerBounds, currentRoom.GetBounds(tileset, x, y), axis, out depth)) {

                            if (depth.Y != 0) {
                                moveableEntity.SetVelocity(new Vector2(moveableEntity.Velocity.X, 0));
                                if (moveableEntity is BoneAttackEntity) {
                                    (moveableEntity as BoneAttackEntity).Collide(null);
                                }
                            }

                            if (depth.X != 0) {
                                moveableEntity.SetVelocity(new Vector2(0, moveableEntity.Velocity.Y));
                                if (moveableEntity is BoneAttackEntity) {
                                    (moveableEntity as BoneAttackEntity).Collide(null);
                                }
                            }


                            moveableEntity.SetPosition(Vector2.Add(moveableEntity.Position, depth));

                            playerBounds = new Rectangle(moveableEntity.Bounds.X - (int)(roomLocation.X * Room.MaxWidth * tileset.TileWidth), moveableEntity.Bounds.Y - (int)(roomLocation.Y * Room.MaxHeight * tileset.TileHeight), moveableEntity.Bounds.Width, moveableEntity.Bounds.Height);

                        }
                    }
                }
            }
        }

        private static float _friction = 0.85f;

        public void ResolvePosition(IMoveableEntity moveableEntity, Map map, Tileset tileset, GameTime gameTime) {

            Vector2 currentPosition = moveableEntity.Position;

            moveableEntity.SetVelocity(Vector2.Multiply(moveableEntity.MovementDelta, moveableEntity.MovementSpeed));

            //velocity *= GroundDragFactor;

            Vector2 additiveVelocity = Vector2.Multiply(moveableEntity.Velocity, (float)gameTime.ElapsedGameTime.TotalSeconds);

            moveableEntity.SetVelocity(additiveVelocity * _friction);

            moveableEntity.SetPosition(Vector2.Add(moveableEntity.Position, moveableEntity.Velocity));
            //if ( livingEntity is Player ) {

            //    if ( InputManager.IsKeyNewlyPressed( Microsoft.Xna.Framework.Input.Keys.OemPlus ) ) {
            //        MovementSpeed += 0.5f;
            //    }

            //    Task task = new Task( () => {
            //        Debug.WriteLine( "Velocity: " + Velocity.ToString() );
            //    } );

            //    task.Start();
            //}

            //HACK
            if (moveableEntity is BoneAttackEntity) {
                ResolveCollision(moveableEntity, tileset, map, Axis.Horizontal);
                ResolveCollision(moveableEntity, tileset, map, Axis.Vertical);
            }

            if (moveableEntity.Velocity.X != 0) {
                moveableEntity.SetPosition(Vector2.Add(moveableEntity.Position, Vector2.Multiply(moveableEntity.Velocity, Vector2.UnitX)));
                moveableEntity.SetPosition(new Vector2((float)Math.Round(moveableEntity.Position.X), moveableEntity.Position.Y));
                ResolveCollision(moveableEntity, tileset, map, Axis.Horizontal);
            }

            if (moveableEntity.Velocity.Y != 0) {
                moveableEntity.SetPosition(Vector2.Add(moveableEntity.Position, Vector2.Multiply(moveableEntity.Velocity, Vector2.UnitY)));
                moveableEntity.SetPosition(new Vector2(moveableEntity.Position.X, (float)Math.Round(moveableEntity.Position.Y)));
                ResolveCollision(moveableEntity, tileset, map, Axis.Vertical);
            }

            if ((int)currentPosition.X == (int)moveableEntity.Position.X) {
                moveableEntity.SetPosition(new Vector2(currentPosition.X, moveableEntity.Position.Y));
            }
            if ((int)currentPosition.Y == (int)moveableEntity.Position.Y) {
                moveableEntity.SetPosition(new Vector2(moveableEntity.Position.X, currentPosition.Y));
            }
        }

    }
}
