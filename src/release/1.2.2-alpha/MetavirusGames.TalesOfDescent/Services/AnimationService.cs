﻿using MetavirusGames.Framework;
using MetavirusGames.Framework.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetavirusGames.TalesOfDescent.Services {
    public class AnimationService {

        private Dictionary<string, Animation> _animations;

        private static object SyncRoot = new object();

        private static AnimationService _instance;

        public static AnimationService Instance {
            get {
                if (_instance == null) {
                    lock (SyncRoot) {
                        if (_instance == null) {
                            return _instance = new AnimationService();
                        }
                    }
                }

                return _instance;
            }
        }
       
        private AnimationService() 
        {
            this._animations = new Dictionary<string, Animation>();
        }

        public void AddAnimation(string key, Animation animation) {

            if (!this._animations.ContainsKey(key)) {
                this._animations.Add(key, animation);
            }

        }

        public Animation GetAnimation(string key) {

            if (_animations.ContainsKey(key)) {
                return _animations[key];
            } else {
                Error.InvalidOperation("Could not find an animation for the key: {0}", key);
                return null;
            }

        }
    }
}
